<?php

/* WebProfilerBundle:Collector:router.html.twig */
class __TwigTemplate_e921e34bf8ed02db42a195416909e3edabf80b0f44593c2f2b817c8cd17a5dc8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "WebProfilerBundle:Collector:router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d9c59a13f7ad99d299ba87202c79f1a511a9725dc13dccb95686068b7405b801 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d9c59a13f7ad99d299ba87202c79f1a511a9725dc13dccb95686068b7405b801->enter($__internal_d9c59a13f7ad99d299ba87202c79f1a511a9725dc13dccb95686068b7405b801_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:router.html.twig"));

        $__internal_007f9ef4736b6c45adb2bc20536b10364bd9ebfdbdbdcd9ce4f091aea34ade2b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_007f9ef4736b6c45adb2bc20536b10364bd9ebfdbdbdcd9ce4f091aea34ade2b->enter($__internal_007f9ef4736b6c45adb2bc20536b10364bd9ebfdbdbdcd9ce4f091aea34ade2b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_d9c59a13f7ad99d299ba87202c79f1a511a9725dc13dccb95686068b7405b801->leave($__internal_d9c59a13f7ad99d299ba87202c79f1a511a9725dc13dccb95686068b7405b801_prof);

        
        $__internal_007f9ef4736b6c45adb2bc20536b10364bd9ebfdbdbdcd9ce4f091aea34ade2b->leave($__internal_007f9ef4736b6c45adb2bc20536b10364bd9ebfdbdbdcd9ce4f091aea34ade2b_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_163d8dd345733bc54bbb0d7b62c9b3a54fda64bc1ba7430680a7426a812e6b5e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_163d8dd345733bc54bbb0d7b62c9b3a54fda64bc1ba7430680a7426a812e6b5e->enter($__internal_163d8dd345733bc54bbb0d7b62c9b3a54fda64bc1ba7430680a7426a812e6b5e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_9bc328430533fe6b5f465db05435000caf63553b6eabd4443c9926e31628be46 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9bc328430533fe6b5f465db05435000caf63553b6eabd4443c9926e31628be46->enter($__internal_9bc328430533fe6b5f465db05435000caf63553b6eabd4443c9926e31628be46_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_9bc328430533fe6b5f465db05435000caf63553b6eabd4443c9926e31628be46->leave($__internal_9bc328430533fe6b5f465db05435000caf63553b6eabd4443c9926e31628be46_prof);

        
        $__internal_163d8dd345733bc54bbb0d7b62c9b3a54fda64bc1ba7430680a7426a812e6b5e->leave($__internal_163d8dd345733bc54bbb0d7b62c9b3a54fda64bc1ba7430680a7426a812e6b5e_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_5a154731e315370d9d08445e14627cec6cd53652da3521eff68371dd38a9fb82 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5a154731e315370d9d08445e14627cec6cd53652da3521eff68371dd38a9fb82->enter($__internal_5a154731e315370d9d08445e14627cec6cd53652da3521eff68371dd38a9fb82_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_9899fe0b8d6bfe8e4746e6686b02a5ef2cf68ab4a449899e2e81949cba83cd50 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9899fe0b8d6bfe8e4746e6686b02a5ef2cf68ab4a449899e2e81949cba83cd50->enter($__internal_9899fe0b8d6bfe8e4746e6686b02a5ef2cf68ab4a449899e2e81949cba83cd50_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_9899fe0b8d6bfe8e4746e6686b02a5ef2cf68ab4a449899e2e81949cba83cd50->leave($__internal_9899fe0b8d6bfe8e4746e6686b02a5ef2cf68ab4a449899e2e81949cba83cd50_prof);

        
        $__internal_5a154731e315370d9d08445e14627cec6cd53652da3521eff68371dd38a9fb82->leave($__internal_5a154731e315370d9d08445e14627cec6cd53652da3521eff68371dd38a9fb82_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_9a8cd5341600475b1efe55579a60474a472b109c077e338bf5674b198431bc01 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9a8cd5341600475b1efe55579a60474a472b109c077e338bf5674b198431bc01->enter($__internal_9a8cd5341600475b1efe55579a60474a472b109c077e338bf5674b198431bc01_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_627c7261951bd21244de14563be84814ae0ab16fb40361338bb6b41808ecb557 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_627c7261951bd21244de14563be84814ae0ab16fb40361338bb6b41808ecb557->enter($__internal_627c7261951bd21244de14563be84814ae0ab16fb40361338bb6b41808ecb557_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_router", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_627c7261951bd21244de14563be84814ae0ab16fb40361338bb6b41808ecb557->leave($__internal_627c7261951bd21244de14563be84814ae0ab16fb40361338bb6b41808ecb557_prof);

        
        $__internal_9a8cd5341600475b1efe55579a60474a472b109c077e338bf5674b198431bc01->leave($__internal_9a8cd5341600475b1efe55579a60474a472b109c077e338bf5674b198431bc01_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Collector:router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 13,  85 => 12,  71 => 7,  68 => 6,  59 => 5,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}{% endblock %}

{% block menu %}
<span class=\"label\">
    <span class=\"icon\">{{ include('@WebProfiler/Icon/router.svg') }}</span>
    <strong>Routing</strong>
</span>
{% endblock %}

{% block panel %}
    {{ render(path('_profiler_router', { token: token })) }}
{% endblock %}
", "WebProfilerBundle:Collector:router.html.twig", "E:\\xampp_new\\htdocs\\Symfony\\store\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle/Resources/views/Collector/router.html.twig");
    }
}

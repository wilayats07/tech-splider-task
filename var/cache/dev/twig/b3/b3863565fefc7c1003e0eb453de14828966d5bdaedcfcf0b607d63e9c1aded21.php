<?php

/* @WebProfiler/Profiler/toolbar_redirect.html.twig */
class __TwigTemplate_d97ad0b7db000e73054c53f19b845681432a7563cd7b7fb402ca1d07f967724a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@WebProfiler/Profiler/toolbar_redirect.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_81e25c964b2fa3d8b7958e53f849337a3876e0aee4df02d9bd1f43e82545e77f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_81e25c964b2fa3d8b7958e53f849337a3876e0aee4df02d9bd1f43e82545e77f->enter($__internal_81e25c964b2fa3d8b7958e53f849337a3876e0aee4df02d9bd1f43e82545e77f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Profiler/toolbar_redirect.html.twig"));

        $__internal_acabd6bacc96096174de63aee3970e6a41f7af5eee2a4f3fc4746c78144d1d3b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_acabd6bacc96096174de63aee3970e6a41f7af5eee2a4f3fc4746c78144d1d3b->enter($__internal_acabd6bacc96096174de63aee3970e6a41f7af5eee2a4f3fc4746c78144d1d3b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Profiler/toolbar_redirect.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_81e25c964b2fa3d8b7958e53f849337a3876e0aee4df02d9bd1f43e82545e77f->leave($__internal_81e25c964b2fa3d8b7958e53f849337a3876e0aee4df02d9bd1f43e82545e77f_prof);

        
        $__internal_acabd6bacc96096174de63aee3970e6a41f7af5eee2a4f3fc4746c78144d1d3b->leave($__internal_acabd6bacc96096174de63aee3970e6a41f7af5eee2a4f3fc4746c78144d1d3b_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_0ab6f57487c3a660bb2a127721f0a0a755822b30b0ec1c105c3245f3f72f3a62 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0ab6f57487c3a660bb2a127721f0a0a755822b30b0ec1c105c3245f3f72f3a62->enter($__internal_0ab6f57487c3a660bb2a127721f0a0a755822b30b0ec1c105c3245f3f72f3a62_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_f5449897fc262769e42adf8073cf8dc62cb0d2edcdcb9459ca1a2468f82c43b5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f5449897fc262769e42adf8073cf8dc62cb0d2edcdcb9459ca1a2468f82c43b5->enter($__internal_f5449897fc262769e42adf8073cf8dc62cb0d2edcdcb9459ca1a2468f82c43b5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Redirection Intercepted";
        
        $__internal_f5449897fc262769e42adf8073cf8dc62cb0d2edcdcb9459ca1a2468f82c43b5->leave($__internal_f5449897fc262769e42adf8073cf8dc62cb0d2edcdcb9459ca1a2468f82c43b5_prof);

        
        $__internal_0ab6f57487c3a660bb2a127721f0a0a755822b30b0ec1c105c3245f3f72f3a62->leave($__internal_0ab6f57487c3a660bb2a127721f0a0a755822b30b0ec1c105c3245f3f72f3a62_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_190e8952f2a949fc9002879fd1042cd51b9fc060de88424da686bc101e9a3d14 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_190e8952f2a949fc9002879fd1042cd51b9fc060de88424da686bc101e9a3d14->enter($__internal_190e8952f2a949fc9002879fd1042cd51b9fc060de88424da686bc101e9a3d14_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_c486d7aaa7975bf1cd73d9fe1048b3a764692a70d99888b717333c76fda3e417 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c486d7aaa7975bf1cd73d9fe1048b3a764692a70d99888b717333c76fda3e417->enter($__internal_c486d7aaa7975bf1cd73d9fe1048b3a764692a70d99888b717333c76fda3e417_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <div class=\"sf-reset\">
        <div class=\"block-exception\">
            <h1>This request redirects to <a href=\"";
        // line 8
        echo twig_escape_filter($this->env, (isset($context["location"]) ? $context["location"] : $this->getContext($context, "location")), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, (isset($context["location"]) ? $context["location"] : $this->getContext($context, "location")), "html", null, true);
        echo "</a>.</h1>

            <p>
                <small>
                    The redirect was intercepted by the web debug toolbar to help debugging.
                    For more information, see the \"intercept-redirects\" option of the Profiler.
                </small>
            </p>
        </div>
    </div>
";
        
        $__internal_c486d7aaa7975bf1cd73d9fe1048b3a764692a70d99888b717333c76fda3e417->leave($__internal_c486d7aaa7975bf1cd73d9fe1048b3a764692a70d99888b717333c76fda3e417_prof);

        
        $__internal_190e8952f2a949fc9002879fd1042cd51b9fc060de88424da686bc101e9a3d14->leave($__internal_190e8952f2a949fc9002879fd1042cd51b9fc060de88424da686bc101e9a3d14_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Profiler/toolbar_redirect.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  72 => 8,  68 => 6,  59 => 5,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@Twig/layout.html.twig' %}

{% block title 'Redirection Intercepted' %}

{% block body %}
    <div class=\"sf-reset\">
        <div class=\"block-exception\">
            <h1>This request redirects to <a href=\"{{ location }}\">{{ location }}</a>.</h1>

            <p>
                <small>
                    The redirect was intercepted by the web debug toolbar to help debugging.
                    For more information, see the \"intercept-redirects\" option of the Profiler.
                </small>
            </p>
        </div>
    </div>
{% endblock %}
", "@WebProfiler/Profiler/toolbar_redirect.html.twig", "E:\\xampp_new\\htdocs\\Symfony\\store\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle\\Resources\\views\\Profiler\\toolbar_redirect.html.twig");
    }
}

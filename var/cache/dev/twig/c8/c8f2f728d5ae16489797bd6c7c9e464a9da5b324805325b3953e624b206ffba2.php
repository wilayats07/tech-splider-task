<?php

/* ::base.html.twig */
class __TwigTemplate_dfa9721b78528cbeedbe3eb29513387d6f609652fa90b23015e5701593a91d11 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_475ea8fc2ba034764fc61901fabba937b3e178bd23ad5813c79c63625a8761cf = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_475ea8fc2ba034764fc61901fabba937b3e178bd23ad5813c79c63625a8761cf->enter($__internal_475ea8fc2ba034764fc61901fabba937b3e178bd23ad5813c79c63625a8761cf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::base.html.twig"));

        $__internal_27efbb46276c61dd0ce0cbe8255ec68e180cd9158605e2c8e8d4d36ccf32443c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_27efbb46276c61dd0ce0cbe8255ec68e180cd9158605e2c8e8d4d36ccf32443c->enter($__internal_27efbb46276c61dd0ce0cbe8255ec68e180cd9158605e2c8e8d4d36ccf32443c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>
        ";
        // line 10
        $this->displayBlock('body', $context, $blocks);
        // line 11
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 12
        echo "    </body>
</html>
";
        
        $__internal_475ea8fc2ba034764fc61901fabba937b3e178bd23ad5813c79c63625a8761cf->leave($__internal_475ea8fc2ba034764fc61901fabba937b3e178bd23ad5813c79c63625a8761cf_prof);

        
        $__internal_27efbb46276c61dd0ce0cbe8255ec68e180cd9158605e2c8e8d4d36ccf32443c->leave($__internal_27efbb46276c61dd0ce0cbe8255ec68e180cd9158605e2c8e8d4d36ccf32443c_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_21d059f17b0ae9d36b2f81f9998ae4e172ed697f8048b2d9232d56b7b4549bb5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_21d059f17b0ae9d36b2f81f9998ae4e172ed697f8048b2d9232d56b7b4549bb5->enter($__internal_21d059f17b0ae9d36b2f81f9998ae4e172ed697f8048b2d9232d56b7b4549bb5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_16431ebd57345f847603267a1eeea6705feb13b57b84aaf6fd333f9e5a0bb014 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_16431ebd57345f847603267a1eeea6705feb13b57b84aaf6fd333f9e5a0bb014->enter($__internal_16431ebd57345f847603267a1eeea6705feb13b57b84aaf6fd333f9e5a0bb014_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_16431ebd57345f847603267a1eeea6705feb13b57b84aaf6fd333f9e5a0bb014->leave($__internal_16431ebd57345f847603267a1eeea6705feb13b57b84aaf6fd333f9e5a0bb014_prof);

        
        $__internal_21d059f17b0ae9d36b2f81f9998ae4e172ed697f8048b2d9232d56b7b4549bb5->leave($__internal_21d059f17b0ae9d36b2f81f9998ae4e172ed697f8048b2d9232d56b7b4549bb5_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_6c803efaf8e30dfcc8e3de0995c91a0695649c7651016da86ba66541458d18fa = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6c803efaf8e30dfcc8e3de0995c91a0695649c7651016da86ba66541458d18fa->enter($__internal_6c803efaf8e30dfcc8e3de0995c91a0695649c7651016da86ba66541458d18fa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_033ae01bfb6d3c806b412d2773d69fb1745da95c0eccf3883d94b9171f566e20 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_033ae01bfb6d3c806b412d2773d69fb1745da95c0eccf3883d94b9171f566e20->enter($__internal_033ae01bfb6d3c806b412d2773d69fb1745da95c0eccf3883d94b9171f566e20_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_033ae01bfb6d3c806b412d2773d69fb1745da95c0eccf3883d94b9171f566e20->leave($__internal_033ae01bfb6d3c806b412d2773d69fb1745da95c0eccf3883d94b9171f566e20_prof);

        
        $__internal_6c803efaf8e30dfcc8e3de0995c91a0695649c7651016da86ba66541458d18fa->leave($__internal_6c803efaf8e30dfcc8e3de0995c91a0695649c7651016da86ba66541458d18fa_prof);

    }

    // line 10
    public function block_body($context, array $blocks = array())
    {
        $__internal_bbbf6936f150be50fa1e0981f1b98c36189d80ac48dd2be79e6bb4020594db7f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bbbf6936f150be50fa1e0981f1b98c36189d80ac48dd2be79e6bb4020594db7f->enter($__internal_bbbf6936f150be50fa1e0981f1b98c36189d80ac48dd2be79e6bb4020594db7f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_81172ecf8c6d8d9e722883dddd1f0411aa0480b005971adca00c86a175a2f580 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_81172ecf8c6d8d9e722883dddd1f0411aa0480b005971adca00c86a175a2f580->enter($__internal_81172ecf8c6d8d9e722883dddd1f0411aa0480b005971adca00c86a175a2f580_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_81172ecf8c6d8d9e722883dddd1f0411aa0480b005971adca00c86a175a2f580->leave($__internal_81172ecf8c6d8d9e722883dddd1f0411aa0480b005971adca00c86a175a2f580_prof);

        
        $__internal_bbbf6936f150be50fa1e0981f1b98c36189d80ac48dd2be79e6bb4020594db7f->leave($__internal_bbbf6936f150be50fa1e0981f1b98c36189d80ac48dd2be79e6bb4020594db7f_prof);

    }

    // line 11
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_b1a228db754e3b9c7867a0d6d7ae6f726ab2c1cb3172bf4672262589f3d6d451 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b1a228db754e3b9c7867a0d6d7ae6f726ab2c1cb3172bf4672262589f3d6d451->enter($__internal_b1a228db754e3b9c7867a0d6d7ae6f726ab2c1cb3172bf4672262589f3d6d451_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_154ba3eab905cac50a722be6facb19ba2b39d6939dca5dc76ad1fc8d7c20e68c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_154ba3eab905cac50a722be6facb19ba2b39d6939dca5dc76ad1fc8d7c20e68c->enter($__internal_154ba3eab905cac50a722be6facb19ba2b39d6939dca5dc76ad1fc8d7c20e68c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_154ba3eab905cac50a722be6facb19ba2b39d6939dca5dc76ad1fc8d7c20e68c->leave($__internal_154ba3eab905cac50a722be6facb19ba2b39d6939dca5dc76ad1fc8d7c20e68c_prof);

        
        $__internal_b1a228db754e3b9c7867a0d6d7ae6f726ab2c1cb3172bf4672262589f3d6d451->leave($__internal_b1a228db754e3b9c7867a0d6d7ae6f726ab2c1cb3172bf4672262589f3d6d451_prof);

    }

    public function getTemplateName()
    {
        return "::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  117 => 11,  100 => 10,  83 => 6,  65 => 5,  53 => 12,  50 => 11,  48 => 10,  41 => 7,  39 => 6,  35 => 5,  29 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>{% block title %}Welcome!{% endblock %}</title>
        {% block stylesheets %}{% endblock %}
        <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />
    </head>
    <body>
        {% block body %}{% endblock %}
        {% block javascripts %}{% endblock %}
    </body>
</html>
", "::base.html.twig", "E:\\xampp_new\\htdocs\\Symfony\\store\\app/Resources\\views/base.html.twig");
    }
}

<?php

/* @Framework/Form/submit_widget.html.php */
class __TwigTemplate_6fadd97612084bc62e99c1c95fcc3a0e8ff142d467d7e03d2b4dae8da0522881 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_deb984ffc0fd62ecdcddad1e5d5e9b21d86081ab7d9efc4f2e789bb5437e514b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_deb984ffc0fd62ecdcddad1e5d5e9b21d86081ab7d9efc4f2e789bb5437e514b->enter($__internal_deb984ffc0fd62ecdcddad1e5d5e9b21d86081ab7d9efc4f2e789bb5437e514b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/submit_widget.html.php"));

        $__internal_2e5af79977bc8c2eac68615837ee1bd98cded65c0f32fdcaf62ee944d7683fc5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2e5af79977bc8c2eac68615837ee1bd98cded65c0f32fdcaf62ee944d7683fc5->enter($__internal_2e5af79977bc8c2eac68615837ee1bd98cded65c0f32fdcaf62ee944d7683fc5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/submit_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'submit')) ?>
";
        
        $__internal_deb984ffc0fd62ecdcddad1e5d5e9b21d86081ab7d9efc4f2e789bb5437e514b->leave($__internal_deb984ffc0fd62ecdcddad1e5d5e9b21d86081ab7d9efc4f2e789bb5437e514b_prof);

        
        $__internal_2e5af79977bc8c2eac68615837ee1bd98cded65c0f32fdcaf62ee944d7683fc5->leave($__internal_2e5af79977bc8c2eac68615837ee1bd98cded65c0f32fdcaf62ee944d7683fc5_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/submit_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'submit')) ?>
", "@Framework/Form/submit_widget.html.php", "E:\\xampp_new\\htdocs\\Symfony\\store\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\submit_widget.html.php");
    }
}

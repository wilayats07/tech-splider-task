<?php

/* @Framework/Form/hidden_row.html.php */
class __TwigTemplate_0a15eee4d1c1dc59b2ec996683f27fcfd49029369ee2331898152157c1b214db extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0f94156353436fa9dbd265fe36e578e4c5fca9cd61d2ff68013b5432fd1102b9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0f94156353436fa9dbd265fe36e578e4c5fca9cd61d2ff68013b5432fd1102b9->enter($__internal_0f94156353436fa9dbd265fe36e578e4c5fca9cd61d2ff68013b5432fd1102b9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_row.html.php"));

        $__internal_d8d7a3656732b711311e5dce5c1fa37d15f910215f2813b6c2109e97c6ccbe7b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d8d7a3656732b711311e5dce5c1fa37d15f910215f2813b6c2109e97c6ccbe7b->enter($__internal_d8d7a3656732b711311e5dce5c1fa37d15f910215f2813b6c2109e97c6ccbe7b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_row.html.php"));

        // line 1
        echo "<?php echo \$view['form']->widget(\$form) ?>
";
        
        $__internal_0f94156353436fa9dbd265fe36e578e4c5fca9cd61d2ff68013b5432fd1102b9->leave($__internal_0f94156353436fa9dbd265fe36e578e4c5fca9cd61d2ff68013b5432fd1102b9_prof);

        
        $__internal_d8d7a3656732b711311e5dce5c1fa37d15f910215f2813b6c2109e97c6ccbe7b->leave($__internal_d8d7a3656732b711311e5dce5c1fa37d15f910215f2813b6c2109e97c6ccbe7b_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/hidden_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->widget(\$form) ?>
", "@Framework/Form/hidden_row.html.php", "E:\\xampp_new\\htdocs\\Symfony\\store\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\hidden_row.html.php");
    }
}

<?php

/* @Framework/Form/choice_attributes.html.php */
class __TwigTemplate_0c3d3985a058d47d19ab1fc98b4b002179ddafbcfd83c87d48a8411c9de39bd5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c193c8880ac2dfc71bf1e963457db348eee3a4968bbbaff017b2c6b8a41bcf26 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c193c8880ac2dfc71bf1e963457db348eee3a4968bbbaff017b2c6b8a41bcf26->enter($__internal_c193c8880ac2dfc71bf1e963457db348eee3a4968bbbaff017b2c6b8a41bcf26_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_attributes.html.php"));

        $__internal_5e4e26ac28114f5e1ba1a1eaff81a8ad5c31839fd8578d6d58b9c06c6e83eca2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5e4e26ac28114f5e1ba1a1eaff81a8ad5c31839fd8578d6d58b9c06c6e83eca2->enter($__internal_5e4e26ac28114f5e1ba1a1eaff81a8ad5c31839fd8578d6d58b9c06c6e83eca2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_attributes.html.php"));

        // line 1
        echo "<?php if (\$disabled): ?>disabled=\"disabled\" <?php endif ?>
<?php foreach (\$choice_attr as \$k => \$v): ?>
<?php if (\$v === true): ?>
<?php printf('%s=\"%s\" ', \$view->escape(\$k), \$view->escape(\$k)) ?>
<?php elseif (\$v !== false): ?>
<?php printf('%s=\"%s\" ', \$view->escape(\$k), \$view->escape(\$v)) ?>
<?php endif ?>
<?php endforeach ?>
";
        
        $__internal_c193c8880ac2dfc71bf1e963457db348eee3a4968bbbaff017b2c6b8a41bcf26->leave($__internal_c193c8880ac2dfc71bf1e963457db348eee3a4968bbbaff017b2c6b8a41bcf26_prof);

        
        $__internal_5e4e26ac28114f5e1ba1a1eaff81a8ad5c31839fd8578d6d58b9c06c6e83eca2->leave($__internal_5e4e26ac28114f5e1ba1a1eaff81a8ad5c31839fd8578d6d58b9c06c6e83eca2_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$disabled): ?>disabled=\"disabled\" <?php endif ?>
<?php foreach (\$choice_attr as \$k => \$v): ?>
<?php if (\$v === true): ?>
<?php printf('%s=\"%s\" ', \$view->escape(\$k), \$view->escape(\$k)) ?>
<?php elseif (\$v !== false): ?>
<?php printf('%s=\"%s\" ', \$view->escape(\$k), \$view->escape(\$v)) ?>
<?php endif ?>
<?php endforeach ?>
", "@Framework/Form/choice_attributes.html.php", "E:\\xampp_new\\htdocs\\Symfony\\store\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\choice_attributes.html.php");
    }
}

<?php

/* @Framework/Form/search_widget.html.php */
class __TwigTemplate_2d26ba761d87d3657b07a9f29909771d83e2bb674bdc5449f3e16b2e6121900e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9c30ee53d40564501bee4b78c62a95be826640ea45d3708134c05d442e11e44e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9c30ee53d40564501bee4b78c62a95be826640ea45d3708134c05d442e11e44e->enter($__internal_9c30ee53d40564501bee4b78c62a95be826640ea45d3708134c05d442e11e44e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/search_widget.html.php"));

        $__internal_e1e009981b949d432290f20ed1afd99e4741ed0cd46077e8938c89ec39d5d423 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e1e009981b949d432290f20ed1afd99e4741ed0cd46077e8938c89ec39d5d423->enter($__internal_e1e009981b949d432290f20ed1afd99e4741ed0cd46077e8938c89ec39d5d423_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/search_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'search')) ?>
";
        
        $__internal_9c30ee53d40564501bee4b78c62a95be826640ea45d3708134c05d442e11e44e->leave($__internal_9c30ee53d40564501bee4b78c62a95be826640ea45d3708134c05d442e11e44e_prof);

        
        $__internal_e1e009981b949d432290f20ed1afd99e4741ed0cd46077e8938c89ec39d5d423->leave($__internal_e1e009981b949d432290f20ed1afd99e4741ed0cd46077e8938c89ec39d5d423_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/search_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'search')) ?>
", "@Framework/Form/search_widget.html.php", "E:\\xampp_new\\htdocs\\Symfony\\store\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\search_widget.html.php");
    }
}

<?php

/* @Framework/Form/form_row.html.php */
class __TwigTemplate_d68a419cf52df19ffccebe09552c7838ceaa04be286d4f8a91c2f44ea66a0629 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_626be236eb630f9372019348fcc6407ab2f087fedeede486e64711cf49f0f087 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_626be236eb630f9372019348fcc6407ab2f087fedeede486e64711cf49f0f087->enter($__internal_626be236eb630f9372019348fcc6407ab2f087fedeede486e64711cf49f0f087_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_row.html.php"));

        $__internal_d9ed23fa8ffd0f214bce2bde8c54f3f67fa3c9f5b3bc0a30143869e3e14ace05 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d9ed23fa8ffd0f214bce2bde8c54f3f67fa3c9f5b3bc0a30143869e3e14ace05->enter($__internal_d9ed23fa8ffd0f214bce2bde8c54f3f67fa3c9f5b3bc0a30143869e3e14ace05_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_row.html.php"));

        // line 1
        echo "<div>
    <?php echo \$view['form']->label(\$form) ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
";
        
        $__internal_626be236eb630f9372019348fcc6407ab2f087fedeede486e64711cf49f0f087->leave($__internal_626be236eb630f9372019348fcc6407ab2f087fedeede486e64711cf49f0f087_prof);

        
        $__internal_d9ed23fa8ffd0f214bce2bde8c54f3f67fa3c9f5b3bc0a30143869e3e14ace05->leave($__internal_d9ed23fa8ffd0f214bce2bde8c54f3f67fa3c9f5b3bc0a30143869e3e14ace05_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div>
    <?php echo \$view['form']->label(\$form) ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
", "@Framework/Form/form_row.html.php", "E:\\xampp_new\\htdocs\\Symfony\\store\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\form_row.html.php");
    }
}

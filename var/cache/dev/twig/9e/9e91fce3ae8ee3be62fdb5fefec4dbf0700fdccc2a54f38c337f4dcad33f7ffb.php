<?php

/* @Framework/Form/radio_widget.html.php */
class __TwigTemplate_c78088b31c8a70463deefcda8ba44aba122d4d8f1d2a86895468e2f6df27c8eb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_447c9ad8be0d77b1f15f0d17745e2d6c29fdb02173cde67f4fef636c961e40a0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_447c9ad8be0d77b1f15f0d17745e2d6c29fdb02173cde67f4fef636c961e40a0->enter($__internal_447c9ad8be0d77b1f15f0d17745e2d6c29fdb02173cde67f4fef636c961e40a0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/radio_widget.html.php"));

        $__internal_a58c83e9307ff20f630723a47251b9c75b1d19827189b1fe0ebabd6c66056cd0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a58c83e9307ff20f630723a47251b9c75b1d19827189b1fe0ebabd6c66056cd0->enter($__internal_a58c83e9307ff20f630723a47251b9c75b1d19827189b1fe0ebabd6c66056cd0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/radio_widget.html.php"));

        // line 1
        echo "<input type=\"radio\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    value=\"<?php echo \$view->escape(\$value) ?>\"
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
";
        
        $__internal_447c9ad8be0d77b1f15f0d17745e2d6c29fdb02173cde67f4fef636c961e40a0->leave($__internal_447c9ad8be0d77b1f15f0d17745e2d6c29fdb02173cde67f4fef636c961e40a0_prof);

        
        $__internal_a58c83e9307ff20f630723a47251b9c75b1d19827189b1fe0ebabd6c66056cd0->leave($__internal_a58c83e9307ff20f630723a47251b9c75b1d19827189b1fe0ebabd6c66056cd0_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/radio_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<input type=\"radio\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    value=\"<?php echo \$view->escape(\$value) ?>\"
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
", "@Framework/Form/radio_widget.html.php", "E:\\xampp_new\\htdocs\\Symfony\\store\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\radio_widget.html.php");
    }
}

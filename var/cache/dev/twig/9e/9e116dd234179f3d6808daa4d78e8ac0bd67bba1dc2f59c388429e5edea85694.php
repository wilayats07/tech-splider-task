<?php

/* @Framework/Form/form_widget_simple.html.php */
class __TwigTemplate_1a4d59a53ec68f6c0780521dc6ade8449579bcee70e6400343f608cc2c753665 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_04e557e25af2c14f9f75a61c3a6336e7606b3933f6a0bc00a479ad9e2df96a6f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_04e557e25af2c14f9f75a61c3a6336e7606b3933f6a0bc00a479ad9e2df96a6f->enter($__internal_04e557e25af2c14f9f75a61c3a6336e7606b3933f6a0bc00a479ad9e2df96a6f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget_simple.html.php"));

        $__internal_a6dfbe6772e81b42709b7320c5b479c46461ffc5977f712ee78971a6f2a822d7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a6dfbe6772e81b42709b7320c5b479c46461ffc5977f712ee78971a6f2a822d7->enter($__internal_a6dfbe6772e81b42709b7320c5b479c46461ffc5977f712ee78971a6f2a822d7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget_simple.html.php"));

        // line 1
        echo "<input type=\"<?php echo isset(\$type) ? \$view->escape(\$type) : 'text' ?>\" <?php echo \$view['form']->block(\$form, 'widget_attributes') ?><?php if (!empty(\$value) || is_numeric(\$value)): ?> value=\"<?php echo \$view->escape(\$value) ?>\"<?php endif ?> />
";
        
        $__internal_04e557e25af2c14f9f75a61c3a6336e7606b3933f6a0bc00a479ad9e2df96a6f->leave($__internal_04e557e25af2c14f9f75a61c3a6336e7606b3933f6a0bc00a479ad9e2df96a6f_prof);

        
        $__internal_a6dfbe6772e81b42709b7320c5b479c46461ffc5977f712ee78971a6f2a822d7->leave($__internal_a6dfbe6772e81b42709b7320c5b479c46461ffc5977f712ee78971a6f2a822d7_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget_simple.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<input type=\"<?php echo isset(\$type) ? \$view->escape(\$type) : 'text' ?>\" <?php echo \$view['form']->block(\$form, 'widget_attributes') ?><?php if (!empty(\$value) || is_numeric(\$value)): ?> value=\"<?php echo \$view->escape(\$value) ?>\"<?php endif ?> />
", "@Framework/Form/form_widget_simple.html.php", "E:\\xampp_new\\htdocs\\Symfony\\store\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\form_widget_simple.html.php");
    }
}

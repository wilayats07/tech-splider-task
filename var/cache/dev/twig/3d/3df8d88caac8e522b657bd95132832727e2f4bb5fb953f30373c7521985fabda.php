<?php

/* product/show.html.twig */
class __TwigTemplate_42e0b5816f61d2eca0f0bbadddb682968728936c9eec241225d9bbb3e98a52e2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "product/show.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_73c568df0286cd7c48e4350d29a8f7a6926273d262c0a1179bb910de64771310 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_73c568df0286cd7c48e4350d29a8f7a6926273d262c0a1179bb910de64771310->enter($__internal_73c568df0286cd7c48e4350d29a8f7a6926273d262c0a1179bb910de64771310_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "product/show.html.twig"));

        $__internal_a593dfd5f54df63f14ce13c1b865181bf5e4d8913772221caf552fd55abcf496 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a593dfd5f54df63f14ce13c1b865181bf5e4d8913772221caf552fd55abcf496->enter($__internal_a593dfd5f54df63f14ce13c1b865181bf5e4d8913772221caf552fd55abcf496_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "product/show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_73c568df0286cd7c48e4350d29a8f7a6926273d262c0a1179bb910de64771310->leave($__internal_73c568df0286cd7c48e4350d29a8f7a6926273d262c0a1179bb910de64771310_prof);

        
        $__internal_a593dfd5f54df63f14ce13c1b865181bf5e4d8913772221caf552fd55abcf496->leave($__internal_a593dfd5f54df63f14ce13c1b865181bf5e4d8913772221caf552fd55abcf496_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_6a99fb9f1660e8d7a57e63a35cb404246a4326e28fb6eb3fb84538712053f465 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6a99fb9f1660e8d7a57e63a35cb404246a4326e28fb6eb3fb84538712053f465->enter($__internal_6a99fb9f1660e8d7a57e63a35cb404246a4326e28fb6eb3fb84538712053f465_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_34dc158edc7037e07717d4f3be4176ca44c26f7864a3d331165b419ffc9e7b2b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_34dc158edc7037e07717d4f3be4176ca44c26f7864a3d331165b419ffc9e7b2b->enter($__internal_34dc158edc7037e07717d4f3be4176ca44c26f7864a3d331165b419ffc9e7b2b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>Product</h1>

    <table  class=\"table table-bordered \">
        <tbody>
            <tr>
                <th>Id</th>
                <td colspan=\"3\">";
        // line 10
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : $this->getContext($context, "product")), "id", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Title</th>
                <td>";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : $this->getContext($context, "product")), "title", array()), "html", null, true);
        echo "</td>
                <th>Status</th>
                <td>";
        // line 16
        echo (($this->getAttribute((isset($context["product"]) ? $context["product"] : $this->getContext($context, "product")), "isActive", array())) ? ("active") : ("Disabled"));
        echo "</td>
            </tr>
        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"";
        // line 23
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("product_index");
        echo "\">Back to the list</a>
        </li>
        <li>
            <a href=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("product_edit", array("id" => $this->getAttribute((isset($context["product"]) ? $context["product"] : $this->getContext($context, "product")), "id", array()))), "html", null, true);
        echo "\">Edit</a>
        </li>
        <li>
            ";
        // line 29
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["delete_form"]) ? $context["delete_form"] : $this->getContext($context, "delete_form")), 'form_start');
        echo "
                <input type=\"submit\" value=\"Delete\">
            ";
        // line 31
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["delete_form"]) ? $context["delete_form"] : $this->getContext($context, "delete_form")), 'form_end');
        echo "
        </li>
    </ul>
";
        
        $__internal_34dc158edc7037e07717d4f3be4176ca44c26f7864a3d331165b419ffc9e7b2b->leave($__internal_34dc158edc7037e07717d4f3be4176ca44c26f7864a3d331165b419ffc9e7b2b_prof);

        
        $__internal_6a99fb9f1660e8d7a57e63a35cb404246a4326e28fb6eb3fb84538712053f465->leave($__internal_6a99fb9f1660e8d7a57e63a35cb404246a4326e28fb6eb3fb84538712053f465_prof);

    }

    public function getTemplateName()
    {
        return "product/show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  96 => 31,  91 => 29,  85 => 26,  79 => 23,  69 => 16,  64 => 14,  57 => 10,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <h1>Product</h1>

    <table  class=\"table table-bordered \">
        <tbody>
            <tr>
                <th>Id</th>
                <td colspan=\"3\">{{ product.id }}</td>
            </tr>
            <tr>
                <th>Title</th>
                <td>{{ product.title }}</td>
                <th>Status</th>
                <td>{{ (product.isActive)?\"active\" :\"Disabled\" }}</td>
            </tr>
        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"{{ path('product_index') }}\">Back to the list</a>
        </li>
        <li>
            <a href=\"{{ path('product_edit', { 'id': product.id }) }}\">Edit</a>
        </li>
        <li>
            {{ form_start(delete_form) }}
                <input type=\"submit\" value=\"Delete\">
            {{ form_end(delete_form) }}
        </li>
    </ul>
{% endblock %}
", "product/show.html.twig", "E:\\xampp_new\\htdocs\\Symfony\\store\\app\\Resources\\views\\product\\show.html.twig");
    }
}

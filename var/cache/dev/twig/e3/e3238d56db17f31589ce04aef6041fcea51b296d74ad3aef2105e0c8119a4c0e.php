<?php

/* :product:new.html.twig */
class __TwigTemplate_018a31b38e29a9757fd99bd6b905378f6365c2ad5ab7677d1e9afce94dba62f5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":product:new.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_cc18859b2b3d3e41d3430e327155f60721f0776ebdfafe31e30bcbe8680021fa = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_cc18859b2b3d3e41d3430e327155f60721f0776ebdfafe31e30bcbe8680021fa->enter($__internal_cc18859b2b3d3e41d3430e327155f60721f0776ebdfafe31e30bcbe8680021fa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":product:new.html.twig"));

        $__internal_2ccaa95428f788061d6557f47322e0c980c8557dec557196dfc1d0f5caefff2d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2ccaa95428f788061d6557f47322e0c980c8557dec557196dfc1d0f5caefff2d->enter($__internal_2ccaa95428f788061d6557f47322e0c980c8557dec557196dfc1d0f5caefff2d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":product:new.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_cc18859b2b3d3e41d3430e327155f60721f0776ebdfafe31e30bcbe8680021fa->leave($__internal_cc18859b2b3d3e41d3430e327155f60721f0776ebdfafe31e30bcbe8680021fa_prof);

        
        $__internal_2ccaa95428f788061d6557f47322e0c980c8557dec557196dfc1d0f5caefff2d->leave($__internal_2ccaa95428f788061d6557f47322e0c980c8557dec557196dfc1d0f5caefff2d_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_cf7c968e3c6030d9efdb32001dd14627b58236ebb3e22df6894608b00c809903 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_cf7c968e3c6030d9efdb32001dd14627b58236ebb3e22df6894608b00c809903->enter($__internal_cf7c968e3c6030d9efdb32001dd14627b58236ebb3e22df6894608b00c809903_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_b89a4cd9b935e9022e55e52202c96612b9c46d39fb8b4a9d87b48d8bde986e7a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b89a4cd9b935e9022e55e52202c96612b9c46d39fb8b4a9d87b48d8bde986e7a->enter($__internal_b89a4cd9b935e9022e55e52202c96612b9c46d39fb8b4a9d87b48d8bde986e7a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>Product creation</h1>

    ";
        // line 6
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start');
        echo "
        ";
        // line 7
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        echo "
        <input type=\"submit\" value=\"Create\" />
    ";
        // line 9
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "

    <ul>
        <li>
            <a href=\"";
        // line 13
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("product_index");
        echo "\">Back to the list</a>
        </li>
    </ul>
";
        
        $__internal_b89a4cd9b935e9022e55e52202c96612b9c46d39fb8b4a9d87b48d8bde986e7a->leave($__internal_b89a4cd9b935e9022e55e52202c96612b9c46d39fb8b4a9d87b48d8bde986e7a_prof);

        
        $__internal_cf7c968e3c6030d9efdb32001dd14627b58236ebb3e22df6894608b00c809903->leave($__internal_cf7c968e3c6030d9efdb32001dd14627b58236ebb3e22df6894608b00c809903_prof);

    }

    public function getTemplateName()
    {
        return ":product:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  69 => 13,  62 => 9,  57 => 7,  53 => 6,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <h1>Product creation</h1>

    {{ form_start(form) }}
        {{ form_widget(form) }}
        <input type=\"submit\" value=\"Create\" />
    {{ form_end(form) }}

    <ul>
        <li>
            <a href=\"{{ path('product_index') }}\">Back to the list</a>
        </li>
    </ul>
{% endblock %}
", ":product:new.html.twig", "E:\\xampp_new\\htdocs\\Symfony\\store\\app/Resources\\views/product/new.html.twig");
    }
}

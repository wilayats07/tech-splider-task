<?php

/* WebProfilerBundle:Profiler:info.html.twig */
class __TwigTemplate_dc10031aa395a89768e7c4e2f6b22d8ec29ecafeb15e092a145eb2a0989373d7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "WebProfilerBundle:Profiler:info.html.twig", 1);
        $this->blocks = array(
            'summary' => array($this, 'block_summary'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_75e322b19510fd55939e0ccc99712a3b1be5e6d23d9907a3e21d926f730d99a1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_75e322b19510fd55939e0ccc99712a3b1be5e6d23d9907a3e21d926f730d99a1->enter($__internal_75e322b19510fd55939e0ccc99712a3b1be5e6d23d9907a3e21d926f730d99a1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:info.html.twig"));

        $__internal_ae8f1706d375720d338d89458779bc5c999ff0a01770daad7fd1b2be4f06ea5f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ae8f1706d375720d338d89458779bc5c999ff0a01770daad7fd1b2be4f06ea5f->enter($__internal_ae8f1706d375720d338d89458779bc5c999ff0a01770daad7fd1b2be4f06ea5f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:info.html.twig"));

        // line 3
        $context["messages"] = array("purge" => array("status" => "success", "title" => "The profiler database was purged successfully", "message" => "Now you need to browse some pages with the Symfony Profiler enabled to collect data."), "no_token" => array("status" => "error", "title" => (((((        // line 11
array_key_exists("token", $context)) ? (_twig_default_filter((isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")), "")) : ("")) == "latest")) ? ("There are no profiles") : ("Token not found")), "message" => (((((        // line 12
array_key_exists("token", $context)) ? (_twig_default_filter((isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")), "")) : ("")) == "latest")) ? ("No profiles found in the database.") : ((("Token \"" . ((array_key_exists("token", $context)) ? (_twig_default_filter((isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")), "")) : (""))) . "\" was not found in the database.")))), "upload_error" => array("status" => "error", "title" => "A problem occurred when uploading the data", "message" => "No file given or the file was not uploaded successfully."), "already_exists" => array("status" => "error", "title" => "A problem occurred when uploading the data", "message" => "The token already exists in the database."));
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_75e322b19510fd55939e0ccc99712a3b1be5e6d23d9907a3e21d926f730d99a1->leave($__internal_75e322b19510fd55939e0ccc99712a3b1be5e6d23d9907a3e21d926f730d99a1_prof);

        
        $__internal_ae8f1706d375720d338d89458779bc5c999ff0a01770daad7fd1b2be4f06ea5f->leave($__internal_ae8f1706d375720d338d89458779bc5c999ff0a01770daad7fd1b2be4f06ea5f_prof);

    }

    // line 26
    public function block_summary($context, array $blocks = array())
    {
        $__internal_a0134f77aad7fa2a5d6bc3487564d610d78844dfef1d4f66094a5467b8badfa0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a0134f77aad7fa2a5d6bc3487564d610d78844dfef1d4f66094a5467b8badfa0->enter($__internal_a0134f77aad7fa2a5d6bc3487564d610d78844dfef1d4f66094a5467b8badfa0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "summary"));

        $__internal_a3bd4bf2f99515355a7a6a8e68b86ba0562b1bc26dd43fe6c680b14ccad4dfec = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a3bd4bf2f99515355a7a6a8e68b86ba0562b1bc26dd43fe6c680b14ccad4dfec->enter($__internal_a3bd4bf2f99515355a7a6a8e68b86ba0562b1bc26dd43fe6c680b14ccad4dfec_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "summary"));

        // line 27
        echo "    <div class=\"status status-";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["messages"]) ? $context["messages"] : $this->getContext($context, "messages")), (isset($context["about"]) ? $context["about"] : $this->getContext($context, "about")), array(), "array"), "status", array()), "html", null, true);
        echo "\">
        <div class=\"container\">
            <h2>";
        // line 29
        echo twig_escape_filter($this->env, twig_title_string_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["messages"]) ? $context["messages"] : $this->getContext($context, "messages")), (isset($context["about"]) ? $context["about"] : $this->getContext($context, "about")), array(), "array"), "status", array())), "html", null, true);
        echo "</h2>
        </div>
    </div>
";
        
        $__internal_a3bd4bf2f99515355a7a6a8e68b86ba0562b1bc26dd43fe6c680b14ccad4dfec->leave($__internal_a3bd4bf2f99515355a7a6a8e68b86ba0562b1bc26dd43fe6c680b14ccad4dfec_prof);

        
        $__internal_a0134f77aad7fa2a5d6bc3487564d610d78844dfef1d4f66094a5467b8badfa0->leave($__internal_a0134f77aad7fa2a5d6bc3487564d610d78844dfef1d4f66094a5467b8badfa0_prof);

    }

    // line 34
    public function block_panel($context, array $blocks = array())
    {
        $__internal_3ca28c01a447edfafbd8c97877fddbd97400c8abb5ef7bf926ced7169c66450b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3ca28c01a447edfafbd8c97877fddbd97400c8abb5ef7bf926ced7169c66450b->enter($__internal_3ca28c01a447edfafbd8c97877fddbd97400c8abb5ef7bf926ced7169c66450b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_41a97dea2ca0061d036799a836c14a5ae4b497503f418f2072ce79122901aca1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_41a97dea2ca0061d036799a836c14a5ae4b497503f418f2072ce79122901aca1->enter($__internal_41a97dea2ca0061d036799a836c14a5ae4b497503f418f2072ce79122901aca1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 35
        echo "    <h2>";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["messages"]) ? $context["messages"] : $this->getContext($context, "messages")), (isset($context["about"]) ? $context["about"] : $this->getContext($context, "about")), array(), "array"), "title", array()), "html", null, true);
        echo "</h2>
    <p>";
        // line 36
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["messages"]) ? $context["messages"] : $this->getContext($context, "messages")), (isset($context["about"]) ? $context["about"] : $this->getContext($context, "about")), array(), "array"), "message", array()), "html", null, true);
        echo "</p>
";
        
        $__internal_41a97dea2ca0061d036799a836c14a5ae4b497503f418f2072ce79122901aca1->leave($__internal_41a97dea2ca0061d036799a836c14a5ae4b497503f418f2072ce79122901aca1_prof);

        
        $__internal_3ca28c01a447edfafbd8c97877fddbd97400c8abb5ef7bf926ced7169c66450b->leave($__internal_3ca28c01a447edfafbd8c97877fddbd97400c8abb5ef7bf926ced7169c66450b_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:info.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  89 => 36,  84 => 35,  75 => 34,  61 => 29,  55 => 27,  46 => 26,  36 => 1,  34 => 12,  33 => 11,  32 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% set messages = {
    'purge' : {
        status:  'success',
        title:   'The profiler database was purged successfully',
        message: 'Now you need to browse some pages with the Symfony Profiler enabled to collect data.'
    },
    'no_token' : {
        status:  'error',
        title:   (token|default('') == 'latest') ? 'There are no profiles' : 'Token not found',
        message: (token|default('') == 'latest') ? 'No profiles found in the database.' : 'Token \"' ~ token|default('') ~ '\" was not found in the database.'
    },
    'upload_error' : {
        status:  'error',
        title:   'A problem occurred when uploading the data',
        message: 'No file given or the file was not uploaded successfully.'
    },
    'already_exists' : {
        status:  'error',
        title:   'A problem occurred when uploading the data',
        message: 'The token already exists in the database.'
    }
} %}

{% block summary %}
    <div class=\"status status-{{ messages[about].status }}\">
        <div class=\"container\">
            <h2>{{ messages[about].status|title }}</h2>
        </div>
    </div>
{% endblock %}

{% block panel %}
    <h2>{{ messages[about].title }}</h2>
    <p>{{ messages[about].message }}</p>
{% endblock %}
", "WebProfilerBundle:Profiler:info.html.twig", "E:\\xampp_new\\htdocs\\Symfony\\store\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle/Resources/views/Profiler/info.html.twig");
    }
}

<?php

/* @WebProfiler/Profiler/open.html.twig */
class __TwigTemplate_22c639aca54f3a4e259d4f54b394460df527237147be94af9c8393f211a20749 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/base.html.twig", "@WebProfiler/Profiler/open.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8186458df42a6522075773619fb9f56286998c74f88b04d11113e60b29d9f315 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8186458df42a6522075773619fb9f56286998c74f88b04d11113e60b29d9f315->enter($__internal_8186458df42a6522075773619fb9f56286998c74f88b04d11113e60b29d9f315_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Profiler/open.html.twig"));

        $__internal_a2f9d7702684c530bca418bd397b2167372aacb700789fd4b45d97cc838b4788 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a2f9d7702684c530bca418bd397b2167372aacb700789fd4b45d97cc838b4788->enter($__internal_a2f9d7702684c530bca418bd397b2167372aacb700789fd4b45d97cc838b4788_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Profiler/open.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_8186458df42a6522075773619fb9f56286998c74f88b04d11113e60b29d9f315->leave($__internal_8186458df42a6522075773619fb9f56286998c74f88b04d11113e60b29d9f315_prof);

        
        $__internal_a2f9d7702684c530bca418bd397b2167372aacb700789fd4b45d97cc838b4788->leave($__internal_a2f9d7702684c530bca418bd397b2167372aacb700789fd4b45d97cc838b4788_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_183845dd555f576b4a1acc0e9fcbd17ee8f2b58921deefe2ca5caebef80ec965 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_183845dd555f576b4a1acc0e9fcbd17ee8f2b58921deefe2ca5caebef80ec965->enter($__internal_183845dd555f576b4a1acc0e9fcbd17ee8f2b58921deefe2ca5caebef80ec965_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_12ad40d73f2bdb78116666139e68dd1d05005bf552059a2589778176ff3ade9c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_12ad40d73f2bdb78116666139e68dd1d05005bf552059a2589778176ff3ade9c->enter($__internal_12ad40d73f2bdb78116666139e68dd1d05005bf552059a2589778176ff3ade9c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <style>
        ";
        // line 5
        echo twig_include($this->env, $context, "@WebProfiler/Profiler/open.css.twig");
        echo "
    </style>
";
        
        $__internal_12ad40d73f2bdb78116666139e68dd1d05005bf552059a2589778176ff3ade9c->leave($__internal_12ad40d73f2bdb78116666139e68dd1d05005bf552059a2589778176ff3ade9c_prof);

        
        $__internal_183845dd555f576b4a1acc0e9fcbd17ee8f2b58921deefe2ca5caebef80ec965->leave($__internal_183845dd555f576b4a1acc0e9fcbd17ee8f2b58921deefe2ca5caebef80ec965_prof);

    }

    // line 9
    public function block_body($context, array $blocks = array())
    {
        $__internal_acf7fdc6523796da911a30d4df0c03e9e41d6e89d58efc1ec7f2028faa0dfc5d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_acf7fdc6523796da911a30d4df0c03e9e41d6e89d58efc1ec7f2028faa0dfc5d->enter($__internal_acf7fdc6523796da911a30d4df0c03e9e41d6e89d58efc1ec7f2028faa0dfc5d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_70d6aea746bde6a6ace1ea05a38352a8de0f7e993c6fba6cf8cbb795c50b9846 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_70d6aea746bde6a6ace1ea05a38352a8de0f7e993c6fba6cf8cbb795c50b9846->enter($__internal_70d6aea746bde6a6ace1ea05a38352a8de0f7e993c6fba6cf8cbb795c50b9846_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 10
        echo "<div class=\"header\">
    <h1>";
        // line 11
        echo twig_escape_filter($this->env, (isset($context["file"]) ? $context["file"] : $this->getContext($context, "file")), "html", null, true);
        echo " <small>line ";
        echo twig_escape_filter($this->env, (isset($context["line"]) ? $context["line"] : $this->getContext($context, "line")), "html", null, true);
        echo "</small></h1>
    <a class=\"doc\" href=\"https://symfony.com/doc/";
        // line 12
        echo twig_escape_filter($this->env, twig_constant("Symfony\\Component\\HttpKernel\\Kernel::VERSION"), "html", null, true);
        echo "/reference/configuration/framework.html#ide\" rel=\"help\">Open in your IDE?</a>
</div>
<div class=\"source\">
    ";
        // line 15
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\CodeExtension')->fileExcerpt((isset($context["filename"]) ? $context["filename"] : $this->getContext($context, "filename")), (isset($context["line"]) ? $context["line"] : $this->getContext($context, "line")),  -1);
        echo "
</div>
";
        
        $__internal_70d6aea746bde6a6ace1ea05a38352a8de0f7e993c6fba6cf8cbb795c50b9846->leave($__internal_70d6aea746bde6a6ace1ea05a38352a8de0f7e993c6fba6cf8cbb795c50b9846_prof);

        
        $__internal_acf7fdc6523796da911a30d4df0c03e9e41d6e89d58efc1ec7f2028faa0dfc5d->leave($__internal_acf7fdc6523796da911a30d4df0c03e9e41d6e89d58efc1ec7f2028faa0dfc5d_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Profiler/open.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  90 => 15,  84 => 12,  78 => 11,  75 => 10,  66 => 9,  53 => 5,  50 => 4,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/base.html.twig' %}

{% block head %}
    <style>
        {{ include('@WebProfiler/Profiler/open.css.twig') }}
    </style>
{% endblock %}

{% block body %}
<div class=\"header\">
    <h1>{{ file }} <small>line {{ line }}</small></h1>
    <a class=\"doc\" href=\"https://symfony.com/doc/{{ constant('Symfony\\\\Component\\\\HttpKernel\\\\Kernel::VERSION') }}/reference/configuration/framework.html#ide\" rel=\"help\">Open in your IDE?</a>
</div>
<div class=\"source\">
    {{ filename|file_excerpt(line, -1) }}
</div>
{% endblock %}
", "@WebProfiler/Profiler/open.html.twig", "E:\\xampp_new\\htdocs\\Symfony\\store\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle\\Resources\\views\\Profiler\\open.html.twig");
    }
}

<?php

/* @Framework/Form/form_widget.html.php */
class __TwigTemplate_641373ae80d67ae83f4b6a0facaa44980cb2e0b6f176d37c497fdd5588420939 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e7ef70b039912cd636e2878f0e343cdec463ea9c0dec631bce3dfaeefd21d42d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e7ef70b039912cd636e2878f0e343cdec463ea9c0dec631bce3dfaeefd21d42d->enter($__internal_e7ef70b039912cd636e2878f0e343cdec463ea9c0dec631bce3dfaeefd21d42d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget.html.php"));

        $__internal_3f87c1e6d916f9c0313ba9bced0d222a6fc1b50a31154debd93ba10b3c679d7c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3f87c1e6d916f9c0313ba9bced0d222a6fc1b50a31154debd93ba10b3c679d7c->enter($__internal_3f87c1e6d916f9c0313ba9bced0d222a6fc1b50a31154debd93ba10b3c679d7c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget.html.php"));

        // line 1
        echo "<?php if (\$compound): ?>
<?php echo \$view['form']->block(\$form, 'form_widget_compound')?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'form_widget_simple')?>
<?php endif ?>
";
        
        $__internal_e7ef70b039912cd636e2878f0e343cdec463ea9c0dec631bce3dfaeefd21d42d->leave($__internal_e7ef70b039912cd636e2878f0e343cdec463ea9c0dec631bce3dfaeefd21d42d_prof);

        
        $__internal_3f87c1e6d916f9c0313ba9bced0d222a6fc1b50a31154debd93ba10b3c679d7c->leave($__internal_3f87c1e6d916f9c0313ba9bced0d222a6fc1b50a31154debd93ba10b3c679d7c_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$compound): ?>
<?php echo \$view['form']->block(\$form, 'form_widget_compound')?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'form_widget_simple')?>
<?php endif ?>
", "@Framework/Form/form_widget.html.php", "E:\\xampp_new\\htdocs\\Symfony\\store\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\form_widget.html.php");
    }
}

<?php

/* @Framework/Form/password_widget.html.php */
class __TwigTemplate_923212f6404a9cfd0b0558295603682969779985ac302d02b3842016f9a4c642 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_210219d049cb713619678944a9ccb172288f6f3dcccf7f0afb205281f0d31202 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_210219d049cb713619678944a9ccb172288f6f3dcccf7f0afb205281f0d31202->enter($__internal_210219d049cb713619678944a9ccb172288f6f3dcccf7f0afb205281f0d31202_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/password_widget.html.php"));

        $__internal_f5376bf8e926312dc01c3150015dfea91fcc1e5c3461140b3ec42bc5a1c37a1c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f5376bf8e926312dc01c3150015dfea91fcc1e5c3461140b3ec42bc5a1c37a1c->enter($__internal_f5376bf8e926312dc01c3150015dfea91fcc1e5c3461140b3ec42bc5a1c37a1c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/password_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'password')) ?>
";
        
        $__internal_210219d049cb713619678944a9ccb172288f6f3dcccf7f0afb205281f0d31202->leave($__internal_210219d049cb713619678944a9ccb172288f6f3dcccf7f0afb205281f0d31202_prof);

        
        $__internal_f5376bf8e926312dc01c3150015dfea91fcc1e5c3461140b3ec42bc5a1c37a1c->leave($__internal_f5376bf8e926312dc01c3150015dfea91fcc1e5c3461140b3ec42bc5a1c37a1c_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/password_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'password')) ?>
", "@Framework/Form/password_widget.html.php", "E:\\xampp_new\\htdocs\\Symfony\\store\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\password_widget.html.php");
    }
}

<?php

/* @Framework/Form/button_row.html.php */
class __TwigTemplate_3370b3dcd81a99e02d11ac4d8629f3e21bebfa88f5fc14415e279f042a8c2f0d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1525f2a23692cd5f9e93d422d804b2aa7d45f533639037f29cbda3d2b78497a5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1525f2a23692cd5f9e93d422d804b2aa7d45f533639037f29cbda3d2b78497a5->enter($__internal_1525f2a23692cd5f9e93d422d804b2aa7d45f533639037f29cbda3d2b78497a5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_row.html.php"));

        $__internal_65ec9bbaf17ae27fced5c2f560f380ed2be1f16a2fab61075a31e301e7b02c2a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_65ec9bbaf17ae27fced5c2f560f380ed2be1f16a2fab61075a31e301e7b02c2a->enter($__internal_65ec9bbaf17ae27fced5c2f560f380ed2be1f16a2fab61075a31e301e7b02c2a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_row.html.php"));

        // line 1
        echo "<div>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
";
        
        $__internal_1525f2a23692cd5f9e93d422d804b2aa7d45f533639037f29cbda3d2b78497a5->leave($__internal_1525f2a23692cd5f9e93d422d804b2aa7d45f533639037f29cbda3d2b78497a5_prof);

        
        $__internal_65ec9bbaf17ae27fced5c2f560f380ed2be1f16a2fab61075a31e301e7b02c2a->leave($__internal_65ec9bbaf17ae27fced5c2f560f380ed2be1f16a2fab61075a31e301e7b02c2a_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/button_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
", "@Framework/Form/button_row.html.php", "E:\\xampp_new\\htdocs\\Symfony\\store\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\button_row.html.php");
    }
}

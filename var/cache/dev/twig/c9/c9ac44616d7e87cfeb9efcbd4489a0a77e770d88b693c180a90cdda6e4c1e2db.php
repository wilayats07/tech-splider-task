<?php

/* @Framework/Form/form_end.html.php */
class __TwigTemplate_393da864547072fbae849e24b1fdad62bc7ded7547c947c91f96ce70c64d0369 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_421d5fee2bbffc1e12b2b3073a1a9124e6c367b0d25c37644e4683cd486317af = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_421d5fee2bbffc1e12b2b3073a1a9124e6c367b0d25c37644e4683cd486317af->enter($__internal_421d5fee2bbffc1e12b2b3073a1a9124e6c367b0d25c37644e4683cd486317af_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_end.html.php"));

        $__internal_249517c2a82546d8f3c15f4be8718813764fb806123fad4ea81ed19db6d6155b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_249517c2a82546d8f3c15f4be8718813764fb806123fad4ea81ed19db6d6155b->enter($__internal_249517c2a82546d8f3c15f4be8718813764fb806123fad4ea81ed19db6d6155b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_end.html.php"));

        // line 1
        echo "<?php if (!isset(\$render_rest) || \$render_rest): ?>
<?php echo \$view['form']->rest(\$form) ?>
<?php endif ?>
</form>
";
        
        $__internal_421d5fee2bbffc1e12b2b3073a1a9124e6c367b0d25c37644e4683cd486317af->leave($__internal_421d5fee2bbffc1e12b2b3073a1a9124e6c367b0d25c37644e4683cd486317af_prof);

        
        $__internal_249517c2a82546d8f3c15f4be8718813764fb806123fad4ea81ed19db6d6155b->leave($__internal_249517c2a82546d8f3c15f4be8718813764fb806123fad4ea81ed19db6d6155b_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_end.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (!isset(\$render_rest) || \$render_rest): ?>
<?php echo \$view['form']->rest(\$form) ?>
<?php endif ?>
</form>
", "@Framework/Form/form_end.html.php", "E:\\xampp_new\\htdocs\\Symfony\\store\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\form_end.html.php");
    }
}

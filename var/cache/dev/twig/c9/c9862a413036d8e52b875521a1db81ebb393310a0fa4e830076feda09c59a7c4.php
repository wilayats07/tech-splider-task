<?php

/* WebProfilerBundle:Collector:ajax.html.twig */
class __TwigTemplate_f307fce9dceb2f37576b3a6f8ec15111cf802fdbbb435bb1bfcd2b1225c58baa extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "WebProfilerBundle:Collector:ajax.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_bd3e2847bcdf06f321fe48f036c4f415906380370d38a2f9e8d9d325e6a4067a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bd3e2847bcdf06f321fe48f036c4f415906380370d38a2f9e8d9d325e6a4067a->enter($__internal_bd3e2847bcdf06f321fe48f036c4f415906380370d38a2f9e8d9d325e6a4067a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:ajax.html.twig"));

        $__internal_85ab85db6c3876b5fe2c0dabc717bfd5433a2c1e62e911f52270865579818f01 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_85ab85db6c3876b5fe2c0dabc717bfd5433a2c1e62e911f52270865579818f01->enter($__internal_85ab85db6c3876b5fe2c0dabc717bfd5433a2c1e62e911f52270865579818f01_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:ajax.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_bd3e2847bcdf06f321fe48f036c4f415906380370d38a2f9e8d9d325e6a4067a->leave($__internal_bd3e2847bcdf06f321fe48f036c4f415906380370d38a2f9e8d9d325e6a4067a_prof);

        
        $__internal_85ab85db6c3876b5fe2c0dabc717bfd5433a2c1e62e911f52270865579818f01->leave($__internal_85ab85db6c3876b5fe2c0dabc717bfd5433a2c1e62e911f52270865579818f01_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_e522820f20a20c2d0d35fd0aa8fb9d2da5f68d1dc2b3b7f795c6d0559fb5d14e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e522820f20a20c2d0d35fd0aa8fb9d2da5f68d1dc2b3b7f795c6d0559fb5d14e->enter($__internal_e522820f20a20c2d0d35fd0aa8fb9d2da5f68d1dc2b3b7f795c6d0559fb5d14e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_c2c4116873bdf4f02925ac0d58f21391b03ebda3c920d4c5ddc04231dd9918d9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c2c4116873bdf4f02925ac0d58f21391b03ebda3c920d4c5ddc04231dd9918d9->enter($__internal_c2c4116873bdf4f02925ac0d58f21391b03ebda3c920d4c5ddc04231dd9918d9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        // line 4
        echo "    ";
        ob_start();
        // line 5
        echo "        ";
        echo twig_include($this->env, $context, "@WebProfiler/Icon/ajax.svg");
        echo "
        <span class=\"sf-toolbar-value sf-toolbar-ajax-requests\">0</span>
    ";
        $context["icon"] = ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        // line 8
        echo "
    ";
        // line 9
        $context["text"] = ('' === $tmp = "        <div class=\"sf-toolbar-info-piece\">
            <b class=\"sf-toolbar-ajax-info\"></b>
        </div>
        <div class=\"sf-toolbar-info-piece\">
            <table class=\"sf-toolbar-ajax-requests\">
                <thead>
                    <tr>
                        <th>Method</th>
                        <th>Type</th>
                        <th>Status</th>
                        <th>URL</th>
                        <th>Time</th>
                        <th>Profile</th>
                    </tr>
                </thead>
                <tbody class=\"sf-toolbar-ajax-request-list\"></tbody>
            </table>
        </div>
    ") ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        // line 29
        echo "
    ";
        // line 30
        echo twig_include($this->env, $context, "@WebProfiler/Profiler/toolbar_item.html.twig", array("link" => false));
        echo "
";
        
        $__internal_c2c4116873bdf4f02925ac0d58f21391b03ebda3c920d4c5ddc04231dd9918d9->leave($__internal_c2c4116873bdf4f02925ac0d58f21391b03ebda3c920d4c5ddc04231dd9918d9_prof);

        
        $__internal_e522820f20a20c2d0d35fd0aa8fb9d2da5f68d1dc2b3b7f795c6d0559fb5d14e->leave($__internal_e522820f20a20c2d0d35fd0aa8fb9d2da5f68d1dc2b3b7f795c6d0559fb5d14e_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Collector:ajax.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  85 => 30,  82 => 29,  62 => 9,  59 => 8,  52 => 5,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}
    {% set icon %}
        {{ include('@WebProfiler/Icon/ajax.svg') }}
        <span class=\"sf-toolbar-value sf-toolbar-ajax-requests\">0</span>
    {% endset %}

    {% set text %}
        <div class=\"sf-toolbar-info-piece\">
            <b class=\"sf-toolbar-ajax-info\"></b>
        </div>
        <div class=\"sf-toolbar-info-piece\">
            <table class=\"sf-toolbar-ajax-requests\">
                <thead>
                    <tr>
                        <th>Method</th>
                        <th>Type</th>
                        <th>Status</th>
                        <th>URL</th>
                        <th>Time</th>
                        <th>Profile</th>
                    </tr>
                </thead>
                <tbody class=\"sf-toolbar-ajax-request-list\"></tbody>
            </table>
        </div>
    {% endset %}

    {{ include('@WebProfiler/Profiler/toolbar_item.html.twig', { link: false }) }}
{% endblock %}
", "WebProfilerBundle:Collector:ajax.html.twig", "E:\\xampp_new\\htdocs\\Symfony\\store\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle/Resources/views/Collector/ajax.html.twig");
    }
}

<?php

/* @Framework/Form/collection_widget.html.php */
class __TwigTemplate_4b2db95a6920be8c5af03321c5fc5e1aea039c16756ddebe0048d02db064adcd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d769d44a8d2b35e93d44dfb45a62f9a8e5695437002711e46586dd5c55f48069 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d769d44a8d2b35e93d44dfb45a62f9a8e5695437002711e46586dd5c55f48069->enter($__internal_d769d44a8d2b35e93d44dfb45a62f9a8e5695437002711e46586dd5c55f48069_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/collection_widget.html.php"));

        $__internal_2d1013259b01f023ccb842c4cf9cd77d519bd7a27e1ae7e6cf0301092591f0e4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2d1013259b01f023ccb842c4cf9cd77d519bd7a27e1ae7e6cf0301092591f0e4->enter($__internal_2d1013259b01f023ccb842c4cf9cd77d519bd7a27e1ae7e6cf0301092591f0e4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/collection_widget.html.php"));

        // line 1
        echo "<?php if (isset(\$prototype)): ?>
    <?php \$attr['data-prototype'] = \$view->escape(\$view['form']->row(\$prototype)) ?>
<?php endif ?>
<?php echo \$view['form']->widget(\$form, array('attr' => \$attr)) ?>
";
        
        $__internal_d769d44a8d2b35e93d44dfb45a62f9a8e5695437002711e46586dd5c55f48069->leave($__internal_d769d44a8d2b35e93d44dfb45a62f9a8e5695437002711e46586dd5c55f48069_prof);

        
        $__internal_2d1013259b01f023ccb842c4cf9cd77d519bd7a27e1ae7e6cf0301092591f0e4->leave($__internal_2d1013259b01f023ccb842c4cf9cd77d519bd7a27e1ae7e6cf0301092591f0e4_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/collection_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (isset(\$prototype)): ?>
    <?php \$attr['data-prototype'] = \$view->escape(\$view['form']->row(\$prototype)) ?>
<?php endif ?>
<?php echo \$view['form']->widget(\$form, array('attr' => \$attr)) ?>
", "@Framework/Form/collection_widget.html.php", "E:\\xampp_new\\htdocs\\Symfony\\store\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\collection_widget.html.php");
    }
}

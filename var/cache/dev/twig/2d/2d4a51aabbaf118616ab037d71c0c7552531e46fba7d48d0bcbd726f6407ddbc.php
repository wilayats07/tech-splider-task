<?php

/* @Framework/Form/form_rest.html.php */
class __TwigTemplate_a01f470a581e7c48a497142856e5518036b77523014133a0f5d8ac3ae504b66d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_63b4eff54058ab86251fcb22db4ca85445283d51cf0051762ee3a2aa3a27227e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_63b4eff54058ab86251fcb22db4ca85445283d51cf0051762ee3a2aa3a27227e->enter($__internal_63b4eff54058ab86251fcb22db4ca85445283d51cf0051762ee3a2aa3a27227e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rest.html.php"));

        $__internal_2bad1784e8e468c45bb2b884f882f04959939fe0f1b09560d416db488f8e7f8c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2bad1784e8e468c45bb2b884f882f04959939fe0f1b09560d416db488f8e7f8c->enter($__internal_2bad1784e8e468c45bb2b884f882f04959939fe0f1b09560d416db488f8e7f8c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rest.html.php"));

        // line 1
        echo "<?php foreach (\$form as \$child): ?>
    <?php if (!\$child->isRendered()): ?>
        <?php echo \$view['form']->row(\$child) ?>
    <?php endif; ?>
<?php endforeach; ?>
";
        
        $__internal_63b4eff54058ab86251fcb22db4ca85445283d51cf0051762ee3a2aa3a27227e->leave($__internal_63b4eff54058ab86251fcb22db4ca85445283d51cf0051762ee3a2aa3a27227e_prof);

        
        $__internal_2bad1784e8e468c45bb2b884f882f04959939fe0f1b09560d416db488f8e7f8c->leave($__internal_2bad1784e8e468c45bb2b884f882f04959939fe0f1b09560d416db488f8e7f8c_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_rest.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php foreach (\$form as \$child): ?>
    <?php if (!\$child->isRendered()): ?>
        <?php echo \$view['form']->row(\$child) ?>
    <?php endif; ?>
<?php endforeach; ?>
", "@Framework/Form/form_rest.html.php", "E:\\xampp_new\\htdocs\\Symfony\\store\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\form_rest.html.php");
    }
}

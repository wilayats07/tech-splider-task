<?php

/* @WebProfiler/Profiler/ajax_layout.html.twig */
class __TwigTemplate_a9f738c332b9605873b8ff5939ed971ac3086e0f429ae3f519c374f093393833 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_22823cf5f82fa74f7542082d8c75d98180f4b252bb7bd7878d9b0fd6533fc4fb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_22823cf5f82fa74f7542082d8c75d98180f4b252bb7bd7878d9b0fd6533fc4fb->enter($__internal_22823cf5f82fa74f7542082d8c75d98180f4b252bb7bd7878d9b0fd6533fc4fb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Profiler/ajax_layout.html.twig"));

        $__internal_2df30fcb11ef15450fc9307e481f551819190cb3c2006ba4ada264c71ffdffa9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2df30fcb11ef15450fc9307e481f551819190cb3c2006ba4ada264c71ffdffa9->enter($__internal_2df30fcb11ef15450fc9307e481f551819190cb3c2006ba4ada264c71ffdffa9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Profiler/ajax_layout.html.twig"));

        // line 1
        $this->displayBlock('panel', $context, $blocks);
        
        $__internal_22823cf5f82fa74f7542082d8c75d98180f4b252bb7bd7878d9b0fd6533fc4fb->leave($__internal_22823cf5f82fa74f7542082d8c75d98180f4b252bb7bd7878d9b0fd6533fc4fb_prof);

        
        $__internal_2df30fcb11ef15450fc9307e481f551819190cb3c2006ba4ada264c71ffdffa9->leave($__internal_2df30fcb11ef15450fc9307e481f551819190cb3c2006ba4ada264c71ffdffa9_prof);

    }

    public function block_panel($context, array $blocks = array())
    {
        $__internal_a03620ea60f3c3a062354541f0674a6a967fd0a46b14c815c2bc204c04d138a0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a03620ea60f3c3a062354541f0674a6a967fd0a46b14c815c2bc204c04d138a0->enter($__internal_a03620ea60f3c3a062354541f0674a6a967fd0a46b14c815c2bc204c04d138a0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_6c0b6a3c402ff6596b362ce174337f7a24c1a46772bf2b119b489b5a636262a6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6c0b6a3c402ff6596b362ce174337f7a24c1a46772bf2b119b489b5a636262a6->enter($__internal_6c0b6a3c402ff6596b362ce174337f7a24c1a46772bf2b119b489b5a636262a6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        echo "";
        
        $__internal_6c0b6a3c402ff6596b362ce174337f7a24c1a46772bf2b119b489b5a636262a6->leave($__internal_6c0b6a3c402ff6596b362ce174337f7a24c1a46772bf2b119b489b5a636262a6_prof);

        
        $__internal_a03620ea60f3c3a062354541f0674a6a967fd0a46b14c815c2bc204c04d138a0->leave($__internal_a03620ea60f3c3a062354541f0674a6a967fd0a46b14c815c2bc204c04d138a0_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Profiler/ajax_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  26 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% block panel '' %}
", "@WebProfiler/Profiler/ajax_layout.html.twig", "E:\\xampp_new\\htdocs\\Symfony\\store\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle\\Resources\\views\\Profiler\\ajax_layout.html.twig");
    }
}

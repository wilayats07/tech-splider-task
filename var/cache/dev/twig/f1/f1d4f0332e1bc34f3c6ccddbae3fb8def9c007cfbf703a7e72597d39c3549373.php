<?php

/* @Framework/Form/reset_widget.html.php */
class __TwigTemplate_0fb6986762dfaf434e267d5d63295529de494a723c4d4779c246d4344877f065 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a969c02bd02b0364fbcf5deb7b2dbaf9585bc40d51655e02b02d115ed9faefc0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a969c02bd02b0364fbcf5deb7b2dbaf9585bc40d51655e02b02d115ed9faefc0->enter($__internal_a969c02bd02b0364fbcf5deb7b2dbaf9585bc40d51655e02b02d115ed9faefc0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/reset_widget.html.php"));

        $__internal_6650ac7d9259e7c2a35fc5caa3714b0492d2f8967e704ce4f42d6d15043103b8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6650ac7d9259e7c2a35fc5caa3714b0492d2f8967e704ce4f42d6d15043103b8->enter($__internal_6650ac7d9259e7c2a35fc5caa3714b0492d2f8967e704ce4f42d6d15043103b8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/reset_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'reset')) ?>
";
        
        $__internal_a969c02bd02b0364fbcf5deb7b2dbaf9585bc40d51655e02b02d115ed9faefc0->leave($__internal_a969c02bd02b0364fbcf5deb7b2dbaf9585bc40d51655e02b02d115ed9faefc0_prof);

        
        $__internal_6650ac7d9259e7c2a35fc5caa3714b0492d2f8967e704ce4f42d6d15043103b8->leave($__internal_6650ac7d9259e7c2a35fc5caa3714b0492d2f8967e704ce4f42d6d15043103b8_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/reset_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'reset')) ?>
", "@Framework/Form/reset_widget.html.php", "E:\\xampp_new\\htdocs\\Symfony\\store\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\reset_widget.html.php");
    }
}

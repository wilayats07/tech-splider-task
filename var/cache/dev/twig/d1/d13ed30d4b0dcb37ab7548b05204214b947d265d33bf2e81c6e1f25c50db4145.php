<?php

/* @Framework/Form/money_widget.html.php */
class __TwigTemplate_ef74fcace844ccdc0827040464c45d29988129509eb0bb5733b16ac64caddafe extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4a9f01055df13c05fb697452d208c517f2d04a0f043587fbb764cc404dcdf84d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4a9f01055df13c05fb697452d208c517f2d04a0f043587fbb764cc404dcdf84d->enter($__internal_4a9f01055df13c05fb697452d208c517f2d04a0f043587fbb764cc404dcdf84d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/money_widget.html.php"));

        $__internal_fa9e1adb2eb558b089293c7e955369b143ae8361c0dc44391ab8f51330fe4991 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fa9e1adb2eb558b089293c7e955369b143ae8361c0dc44391ab8f51330fe4991->enter($__internal_fa9e1adb2eb558b089293c7e955369b143ae8361c0dc44391ab8f51330fe4991_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/money_widget.html.php"));

        // line 1
        echo "<?php echo str_replace('";
        echo twig_escape_filter($this->env, (isset($context["widget"]) ? $context["widget"] : $this->getContext($context, "widget")), "html", null, true);
        echo "', \$view['form']->block(\$form, 'form_widget_simple'), \$money_pattern) ?>
";
        
        $__internal_4a9f01055df13c05fb697452d208c517f2d04a0f043587fbb764cc404dcdf84d->leave($__internal_4a9f01055df13c05fb697452d208c517f2d04a0f043587fbb764cc404dcdf84d_prof);

        
        $__internal_fa9e1adb2eb558b089293c7e955369b143ae8361c0dc44391ab8f51330fe4991->leave($__internal_fa9e1adb2eb558b089293c7e955369b143ae8361c0dc44391ab8f51330fe4991_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/money_widget.html.php";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo str_replace('{{ widget }}', \$view['form']->block(\$form, 'form_widget_simple'), \$money_pattern) ?>
", "@Framework/Form/money_widget.html.php", "E:\\xampp_new\\htdocs\\Symfony\\store\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\money_widget.html.php");
    }
}

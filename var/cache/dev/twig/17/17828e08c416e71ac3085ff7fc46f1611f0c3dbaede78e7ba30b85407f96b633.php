<?php

/* TwigBundle:Exception:exception.css.twig */
class __TwigTemplate_2085c1b3268776e260a077bea7a89c53df6d829ab60b1b2008a1136d22e2e42e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_444eda223263ce690d47cd5e07694cf62d6d6a6c3657e159798890ddac12d9b5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_444eda223263ce690d47cd5e07694cf62d6d6a6c3657e159798890ddac12d9b5->enter($__internal_444eda223263ce690d47cd5e07694cf62d6d6a6c3657e159798890ddac12d9b5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.css.twig"));

        $__internal_cb96f382317a4d6e82bc504d78ca46dc85464137b3f0f3ffbc46f6bc1ccbebef = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cb96f382317a4d6e82bc504d78ca46dc85464137b3f0f3ffbc46f6bc1ccbebef->enter($__internal_cb96f382317a4d6e82bc504d78ca46dc85464137b3f0f3ffbc46f6bc1ccbebef_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.css.twig"));

        // line 1
        echo "/*
";
        // line 2
        $this->loadTemplate("@Twig/Exception/exception.txt.twig", "TwigBundle:Exception:exception.css.twig", 2)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        // line 3
        echo "*/
";
        
        $__internal_444eda223263ce690d47cd5e07694cf62d6d6a6c3657e159798890ddac12d9b5->leave($__internal_444eda223263ce690d47cd5e07694cf62d6d6a6c3657e159798890ddac12d9b5_prof);

        
        $__internal_cb96f382317a4d6e82bc504d78ca46dc85464137b3f0f3ffbc46f6bc1ccbebef->leave($__internal_cb96f382317a4d6e82bc504d78ca46dc85464137b3f0f3ffbc46f6bc1ccbebef_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.css.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  30 => 3,  28 => 2,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("/*
{% include '@Twig/Exception/exception.txt.twig' with { 'exception': exception } %}
*/
", "TwigBundle:Exception:exception.css.twig", "E:\\xampp_new\\htdocs\\Symfony\\store\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle/Resources/views/Exception/exception.css.twig");
    }
}

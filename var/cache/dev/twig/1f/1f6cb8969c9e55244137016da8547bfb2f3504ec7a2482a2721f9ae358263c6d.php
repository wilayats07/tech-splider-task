<?php

/* :product:show.html.twig */
class __TwigTemplate_94952158f597c281a9042b392bc726a82dc990a6a755c0d01e658474431f1891 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":product:show.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a9240d5b7e726b519ca717daea824211214751a03376c2f7e2ce5cfce269c16a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a9240d5b7e726b519ca717daea824211214751a03376c2f7e2ce5cfce269c16a->enter($__internal_a9240d5b7e726b519ca717daea824211214751a03376c2f7e2ce5cfce269c16a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":product:show.html.twig"));

        $__internal_a7155d19fb576aef94e81090f338ccfed03a05d060589b5312bb4ddf3eb9b254 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a7155d19fb576aef94e81090f338ccfed03a05d060589b5312bb4ddf3eb9b254->enter($__internal_a7155d19fb576aef94e81090f338ccfed03a05d060589b5312bb4ddf3eb9b254_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":product:show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_a9240d5b7e726b519ca717daea824211214751a03376c2f7e2ce5cfce269c16a->leave($__internal_a9240d5b7e726b519ca717daea824211214751a03376c2f7e2ce5cfce269c16a_prof);

        
        $__internal_a7155d19fb576aef94e81090f338ccfed03a05d060589b5312bb4ddf3eb9b254->leave($__internal_a7155d19fb576aef94e81090f338ccfed03a05d060589b5312bb4ddf3eb9b254_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_cc8fb33da9009954e2ee699bf45a45539da84a4c473ecd302beeb53bee9d16f8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_cc8fb33da9009954e2ee699bf45a45539da84a4c473ecd302beeb53bee9d16f8->enter($__internal_cc8fb33da9009954e2ee699bf45a45539da84a4c473ecd302beeb53bee9d16f8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_c647cf27c20d806b0ef18343eb049a8211e0da8fbff99bea0ace6fdb80c88ba6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c647cf27c20d806b0ef18343eb049a8211e0da8fbff99bea0ace6fdb80c88ba6->enter($__internal_c647cf27c20d806b0ef18343eb049a8211e0da8fbff99bea0ace6fdb80c88ba6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>Product</h1>

    <table>
        <tbody>
            <tr>
                <th>Id</th>
                <td>";
        // line 10
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : $this->getContext($context, "product")), "id", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Title</th>
                <td>";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : $this->getContext($context, "product")), "title", array()), "html", null, true);
        echo "</td>
            </tr>
        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"";
        // line 21
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("product_index");
        echo "\">Back to the list</a>
        </li>
        <li>
            <a href=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("product_edit", array("id" => $this->getAttribute((isset($context["product"]) ? $context["product"] : $this->getContext($context, "product")), "id", array()))), "html", null, true);
        echo "\">Edit</a>
        </li>
        <li>
            ";
        // line 27
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["delete_form"]) ? $context["delete_form"] : $this->getContext($context, "delete_form")), 'form_start');
        echo "
                <input type=\"submit\" value=\"Delete\">
            ";
        // line 29
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["delete_form"]) ? $context["delete_form"] : $this->getContext($context, "delete_form")), 'form_end');
        echo "
        </li>
    </ul>
";
        
        $__internal_c647cf27c20d806b0ef18343eb049a8211e0da8fbff99bea0ace6fdb80c88ba6->leave($__internal_c647cf27c20d806b0ef18343eb049a8211e0da8fbff99bea0ace6fdb80c88ba6_prof);

        
        $__internal_cc8fb33da9009954e2ee699bf45a45539da84a4c473ecd302beeb53bee9d16f8->leave($__internal_cc8fb33da9009954e2ee699bf45a45539da84a4c473ecd302beeb53bee9d16f8_prof);

    }

    public function getTemplateName()
    {
        return ":product:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  91 => 29,  86 => 27,  80 => 24,  74 => 21,  64 => 14,  57 => 10,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <h1>Product</h1>

    <table>
        <tbody>
            <tr>
                <th>Id</th>
                <td>{{ product.id }}</td>
            </tr>
            <tr>
                <th>Title</th>
                <td>{{ product.title }}</td>
            </tr>
        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"{{ path('product_index') }}\">Back to the list</a>
        </li>
        <li>
            <a href=\"{{ path('product_edit', { 'id': product.id }) }}\">Edit</a>
        </li>
        <li>
            {{ form_start(delete_form) }}
                <input type=\"submit\" value=\"Delete\">
            {{ form_end(delete_form) }}
        </li>
    </ul>
{% endblock %}
", ":product:show.html.twig", "E:\\xampp_new\\htdocs\\Symfony\\store\\app/Resources\\views/product/show.html.twig");
    }
}

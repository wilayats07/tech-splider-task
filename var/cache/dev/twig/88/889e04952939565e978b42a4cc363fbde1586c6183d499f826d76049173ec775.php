<?php

/* @Framework/Form/form.html.php */
class __TwigTemplate_d0ae23abd49f9c60de7f820ae1ecc72214b731bc4feb9ef7340450b2b2033136 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c9aca7ba6b78fc22cae9e56842911e36f9c7fa2c315df4eed934d1e825386576 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c9aca7ba6b78fc22cae9e56842911e36f9c7fa2c315df4eed934d1e825386576->enter($__internal_c9aca7ba6b78fc22cae9e56842911e36f9c7fa2c315df4eed934d1e825386576_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form.html.php"));

        $__internal_4c12487774f8e618d1d46db6881751bd0509802a57bf7df4eac621fedbe295a4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4c12487774f8e618d1d46db6881751bd0509802a57bf7df4eac621fedbe295a4->enter($__internal_4c12487774f8e618d1d46db6881751bd0509802a57bf7df4eac621fedbe295a4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form.html.php"));

        // line 1
        echo "<?php echo \$view['form']->start(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
<?php echo \$view['form']->end(\$form) ?>
";
        
        $__internal_c9aca7ba6b78fc22cae9e56842911e36f9c7fa2c315df4eed934d1e825386576->leave($__internal_c9aca7ba6b78fc22cae9e56842911e36f9c7fa2c315df4eed934d1e825386576_prof);

        
        $__internal_4c12487774f8e618d1d46db6881751bd0509802a57bf7df4eac621fedbe295a4->leave($__internal_4c12487774f8e618d1d46db6881751bd0509802a57bf7df4eac621fedbe295a4_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->start(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
<?php echo \$view['form']->end(\$form) ?>
", "@Framework/Form/form.html.php", "E:\\xampp_new\\htdocs\\Symfony\\store\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\form.html.php");
    }
}

<?php

/* TwigBundle:Exception:exception.json.twig */
class __TwigTemplate_9432ae1103d3494e8bb638f8c7707357a10e3d19892aff84790d78508103549c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_fc4409a6a331a16914046ff71899770f82ffa6516f9dc912847e308ce97ad19c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fc4409a6a331a16914046ff71899770f82ffa6516f9dc912847e308ce97ad19c->enter($__internal_fc4409a6a331a16914046ff71899770f82ffa6516f9dc912847e308ce97ad19c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.json.twig"));

        $__internal_08f594c3ff08f66ba57a428a064db3b826f4fb22470933e02a3edb6c70be140f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_08f594c3ff08f66ba57a428a064db3b826f4fb22470933e02a3edb6c70be140f->enter($__internal_08f594c3ff08f66ba57a428a064db3b826f4fb22470933e02a3edb6c70be140f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.json.twig"));

        // line 1
        echo twig_jsonencode_filter(array("error" => array("code" => (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "message" => (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "exception" => $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "toarray", array()))));
        echo "
";
        
        $__internal_fc4409a6a331a16914046ff71899770f82ffa6516f9dc912847e308ce97ad19c->leave($__internal_fc4409a6a331a16914046ff71899770f82ffa6516f9dc912847e308ce97ad19c_prof);

        
        $__internal_08f594c3ff08f66ba57a428a064db3b826f4fb22470933e02a3edb6c70be140f->leave($__internal_08f594c3ff08f66ba57a428a064db3b826f4fb22470933e02a3edb6c70be140f_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.json.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ { 'error': { 'code': status_code, 'message': status_text, 'exception': exception.toarray } }|json_encode|raw }}
", "TwigBundle:Exception:exception.json.twig", "E:\\xampp_new\\htdocs\\Symfony\\store\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle/Resources/views/Exception/exception.json.twig");
    }
}

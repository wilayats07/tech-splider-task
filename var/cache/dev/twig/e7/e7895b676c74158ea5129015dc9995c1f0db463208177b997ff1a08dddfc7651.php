<?php

/* @Framework/Form/integer_widget.html.php */
class __TwigTemplate_95a9b09ebf68b6acc9ce119c7d7cabfed3a0aa09267026ba03cbd0ff3b63b7e2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ccebec03afd6c3cc180efbd59a1f0562160088e78145b134d5a32381e18ca662 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ccebec03afd6c3cc180efbd59a1f0562160088e78145b134d5a32381e18ca662->enter($__internal_ccebec03afd6c3cc180efbd59a1f0562160088e78145b134d5a32381e18ca662_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/integer_widget.html.php"));

        $__internal_1f15471621014bbb88a13b4eb6306b7faf70c634d781ad1229eab5fdcb258a0b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1f15471621014bbb88a13b4eb6306b7faf70c634d781ad1229eab5fdcb258a0b->enter($__internal_1f15471621014bbb88a13b4eb6306b7faf70c634d781ad1229eab5fdcb258a0b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/integer_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'number')) ?>
";
        
        $__internal_ccebec03afd6c3cc180efbd59a1f0562160088e78145b134d5a32381e18ca662->leave($__internal_ccebec03afd6c3cc180efbd59a1f0562160088e78145b134d5a32381e18ca662_prof);

        
        $__internal_1f15471621014bbb88a13b4eb6306b7faf70c634d781ad1229eab5fdcb258a0b->leave($__internal_1f15471621014bbb88a13b4eb6306b7faf70c634d781ad1229eab5fdcb258a0b_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/integer_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'number')) ?>
", "@Framework/Form/integer_widget.html.php", "E:\\xampp_new\\htdocs\\Symfony\\store\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\integer_widget.html.php");
    }
}

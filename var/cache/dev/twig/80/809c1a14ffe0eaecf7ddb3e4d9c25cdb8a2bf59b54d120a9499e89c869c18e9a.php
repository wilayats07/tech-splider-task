<?php

/* :product:index.html.twig */
class __TwigTemplate_830f3012f22c4f23fd0ea730d8c68edf7090fdc5f6bae5a181a783d6eb5d5d2e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":product:index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0ec2432d6bab69f0be3acf1d4bed2cdfbf2b2b8130f0026cb742a0b613e78b02 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0ec2432d6bab69f0be3acf1d4bed2cdfbf2b2b8130f0026cb742a0b613e78b02->enter($__internal_0ec2432d6bab69f0be3acf1d4bed2cdfbf2b2b8130f0026cb742a0b613e78b02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":product:index.html.twig"));

        $__internal_af314a18505236055740b8a39d8b21c66256a1e56a371f592fc013ab18d2c6a3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_af314a18505236055740b8a39d8b21c66256a1e56a371f592fc013ab18d2c6a3->enter($__internal_af314a18505236055740b8a39d8b21c66256a1e56a371f592fc013ab18d2c6a3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":product:index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_0ec2432d6bab69f0be3acf1d4bed2cdfbf2b2b8130f0026cb742a0b613e78b02->leave($__internal_0ec2432d6bab69f0be3acf1d4bed2cdfbf2b2b8130f0026cb742a0b613e78b02_prof);

        
        $__internal_af314a18505236055740b8a39d8b21c66256a1e56a371f592fc013ab18d2c6a3->leave($__internal_af314a18505236055740b8a39d8b21c66256a1e56a371f592fc013ab18d2c6a3_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_2a459f5723d9ba2b61c40e714e96fd60c3f2b178576999c3f5dd545cde7fccd8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2a459f5723d9ba2b61c40e714e96fd60c3f2b178576999c3f5dd545cde7fccd8->enter($__internal_2a459f5723d9ba2b61c40e714e96fd60c3f2b178576999c3f5dd545cde7fccd8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_4476300c3ebf554518a092264b82ae6bd4bac078447b2569080ed537afe4cdf4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4476300c3ebf554518a092264b82ae6bd4bac078447b2569080ed537afe4cdf4->enter($__internal_4476300c3ebf554518a092264b82ae6bd4bac078447b2569080ed537afe4cdf4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>Products list</h1>

    <table>
        <thead>
            <tr>
                <th>Id</th>
                <th>Title</th>
                <th>Active</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        ";
        // line 16
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["products"]) ? $context["products"] : $this->getContext($context, "products")));
        foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
            // line 17
            echo "            <tr>
                <td><a href=\"";
            // line 18
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("product_show", array("id" => $this->getAttribute($context["product"], "id", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "id", array()), "html", null, true);
            echo "</a></td>
                <td>";
            // line 19
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "title", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 20
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "isActive", array()), "html", null, true);
            echo "</td>
                <td>
                    <ul>
                        <li>
                            <a href=\"";
            // line 24
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("product_show", array("id" => $this->getAttribute($context["product"], "id", array()))), "html", null, true);
            echo "\">show</a>
                        </li>
                        <li>
                            <a href=\"";
            // line 27
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("product_edit", array("id" => $this->getAttribute($context["product"], "id", array()))), "html", null, true);
            echo "\">edit</a>
                        </li>
                    </ul>
                </td>
            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 33
        echo "        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"";
        // line 38
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("product_new");
        echo "\">Create a new product</a>
        </li>
    </ul>
";
        
        $__internal_4476300c3ebf554518a092264b82ae6bd4bac078447b2569080ed537afe4cdf4->leave($__internal_4476300c3ebf554518a092264b82ae6bd4bac078447b2569080ed537afe4cdf4_prof);

        
        $__internal_2a459f5723d9ba2b61c40e714e96fd60c3f2b178576999c3f5dd545cde7fccd8->leave($__internal_2a459f5723d9ba2b61c40e714e96fd60c3f2b178576999c3f5dd545cde7fccd8_prof);

    }

    public function getTemplateName()
    {
        return ":product:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  112 => 38,  105 => 33,  93 => 27,  87 => 24,  80 => 20,  76 => 19,  70 => 18,  67 => 17,  63 => 16,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <h1>Products list</h1>

    <table>
        <thead>
            <tr>
                <th>Id</th>
                <th>Title</th>
                <th>Active</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        {% for product in products %}
            <tr>
                <td><a href=\"{{ path('product_show', { 'id': product.id }) }}\">{{ product.id }}</a></td>
                <td>{{ product.title }}</td>
                <td>{{ product.isActive }}</td>
                <td>
                    <ul>
                        <li>
                            <a href=\"{{ path('product_show', { 'id': product.id }) }}\">show</a>
                        </li>
                        <li>
                            <a href=\"{{ path('product_edit', { 'id': product.id }) }}\">edit</a>
                        </li>
                    </ul>
                </td>
            </tr>
        {% endfor %}
        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"{{ path('product_new') }}\">Create a new product</a>
        </li>
    </ul>
{% endblock %}
", ":product:index.html.twig", "E:\\xampp_new\\htdocs\\Symfony\\store\\app/Resources\\views/product/index.html.twig");
    }
}

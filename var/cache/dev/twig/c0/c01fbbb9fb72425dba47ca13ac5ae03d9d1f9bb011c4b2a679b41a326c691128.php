<?php

/* @Framework/Form/choice_widget.html.php */
class __TwigTemplate_5b0c7723737a3bf1a18033efceba1b153622eb0acbc11f9d06ef8f4c21198852 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e5c610b8a74f39e152629594faf84efc4240d887d4b01f348cdbf70d2d09103e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e5c610b8a74f39e152629594faf84efc4240d887d4b01f348cdbf70d2d09103e->enter($__internal_e5c610b8a74f39e152629594faf84efc4240d887d4b01f348cdbf70d2d09103e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget.html.php"));

        $__internal_62f58b9a8c329310aceed339ce2c5c00e319bc651725dc0dd2cd147a28e03780 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_62f58b9a8c329310aceed339ce2c5c00e319bc651725dc0dd2cd147a28e03780->enter($__internal_62f58b9a8c329310aceed339ce2c5c00e319bc651725dc0dd2cd147a28e03780_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget.html.php"));

        // line 1
        echo "<?php if (\$expanded): ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_expanded') ?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_collapsed') ?>
<?php endif ?>
";
        
        $__internal_e5c610b8a74f39e152629594faf84efc4240d887d4b01f348cdbf70d2d09103e->leave($__internal_e5c610b8a74f39e152629594faf84efc4240d887d4b01f348cdbf70d2d09103e_prof);

        
        $__internal_62f58b9a8c329310aceed339ce2c5c00e319bc651725dc0dd2cd147a28e03780->leave($__internal_62f58b9a8c329310aceed339ce2c5c00e319bc651725dc0dd2cd147a28e03780_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$expanded): ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_expanded') ?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_collapsed') ?>
<?php endif ?>
", "@Framework/Form/choice_widget.html.php", "E:\\xampp_new\\htdocs\\Symfony\\store\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\choice_widget.html.php");
    }
}

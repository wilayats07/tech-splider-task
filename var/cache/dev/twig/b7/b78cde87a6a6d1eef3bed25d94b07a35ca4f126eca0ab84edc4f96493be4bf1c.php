<?php

/* product/index.html.twig */
class __TwigTemplate_fbd0f6a2797770e06e0a1e1f27c4e9ecb6dad8daf4fad1c960b464d3a894f173 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "product/index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_64d42ba40004c672cc583df451d452a058040827ca8a9e4743b09cf87e1a96b8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_64d42ba40004c672cc583df451d452a058040827ca8a9e4743b09cf87e1a96b8->enter($__internal_64d42ba40004c672cc583df451d452a058040827ca8a9e4743b09cf87e1a96b8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "product/index.html.twig"));

        $__internal_8dac4a94927f98129a98142c91039ccbe57b77b7ebb2485b4c7e3d2582a00ef2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8dac4a94927f98129a98142c91039ccbe57b77b7ebb2485b4c7e3d2582a00ef2->enter($__internal_8dac4a94927f98129a98142c91039ccbe57b77b7ebb2485b4c7e3d2582a00ef2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "product/index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_64d42ba40004c672cc583df451d452a058040827ca8a9e4743b09cf87e1a96b8->leave($__internal_64d42ba40004c672cc583df451d452a058040827ca8a9e4743b09cf87e1a96b8_prof);

        
        $__internal_8dac4a94927f98129a98142c91039ccbe57b77b7ebb2485b4c7e3d2582a00ef2->leave($__internal_8dac4a94927f98129a98142c91039ccbe57b77b7ebb2485b4c7e3d2582a00ef2_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_5756faca6afcad1b2f7ad85f7e1cd2a26b732de3b7bd7d18f0eb44078087fd57 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5756faca6afcad1b2f7ad85f7e1cd2a26b732de3b7bd7d18f0eb44078087fd57->enter($__internal_5756faca6afcad1b2f7ad85f7e1cd2a26b732de3b7bd7d18f0eb44078087fd57_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_188685438f9363d2e09fbce4230633c300e5c36a7ca78bc5b0e1f1bc409c9ccb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_188685438f9363d2e09fbce4230633c300e5c36a7ca78bc5b0e1f1bc409c9ccb->enter($__internal_188685438f9363d2e09fbce4230633c300e5c36a7ca78bc5b0e1f1bc409c9ccb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>Products list</h1>

    <table class=\"table table-bordered \">
        <thead>
            <tr>
                <th>Id</th>
                <th>Title</th>
                <th>Status</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        ";
        // line 16
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["products"]) ? $context["products"] : $this->getContext($context, "products")));
        foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
            // line 17
            echo "            <tr>
                <td><a href=\"";
            // line 18
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("product_show", array("id" => $this->getAttribute($context["product"], "id", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "id", array()), "html", null, true);
            echo "</a></td>
                <td>";
            // line 19
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "title", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 20
            echo (($this->getAttribute($context["product"], "isActive", array())) ? ("Active") : ("Disabled"));
            echo "</td>
                <td>
                    <ul>
                        <li>
                            <a href=\"";
            // line 24
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("product_show", array("id" => $this->getAttribute($context["product"], "id", array()))), "html", null, true);
            echo "\">show</a>
                        </li>
                        <li>
                            <a href=\"";
            // line 27
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("product_edit", array("id" => $this->getAttribute($context["product"], "id", array()))), "html", null, true);
            echo "\">edit</a>
                        </li>
                    </ul>
                </td>
            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 33
        echo "        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"";
        // line 38
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("product_new");
        echo "\">Create a new product</a>
        </li>
    </ul>
";
        
        $__internal_188685438f9363d2e09fbce4230633c300e5c36a7ca78bc5b0e1f1bc409c9ccb->leave($__internal_188685438f9363d2e09fbce4230633c300e5c36a7ca78bc5b0e1f1bc409c9ccb_prof);

        
        $__internal_5756faca6afcad1b2f7ad85f7e1cd2a26b732de3b7bd7d18f0eb44078087fd57->leave($__internal_5756faca6afcad1b2f7ad85f7e1cd2a26b732de3b7bd7d18f0eb44078087fd57_prof);

    }

    public function getTemplateName()
    {
        return "product/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  112 => 38,  105 => 33,  93 => 27,  87 => 24,  80 => 20,  76 => 19,  70 => 18,  67 => 17,  63 => 16,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <h1>Products list</h1>

    <table class=\"table table-bordered \">
        <thead>
            <tr>
                <th>Id</th>
                <th>Title</th>
                <th>Status</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        {% for product in products %}
            <tr>
                <td><a href=\"{{ path('product_show', { 'id': product.id }) }}\">{{ product.id }}</a></td>
                <td>{{ product.title }}</td>
                <td>{{ (product.isActive) ?\"Active\":\"Disabled\" }}</td>
                <td>
                    <ul>
                        <li>
                            <a href=\"{{ path('product_show', { 'id': product.id }) }}\">show</a>
                        </li>
                        <li>
                            <a href=\"{{ path('product_edit', { 'id': product.id }) }}\">edit</a>
                        </li>
                    </ul>
                </td>
            </tr>
        {% endfor %}
        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"{{ path('product_new') }}\">Create a new product</a>
        </li>
    </ul>
{% endblock %}
", "product/index.html.twig", "E:\\xampp_new\\htdocs\\Symfony\\store\\app\\Resources\\views\\product\\index.html.twig");
    }
}

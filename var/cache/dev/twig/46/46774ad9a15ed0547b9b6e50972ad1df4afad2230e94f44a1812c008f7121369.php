<?php

/* @Framework/Form/hidden_widget.html.php */
class __TwigTemplate_b4ef0e07f9ec8ab69c75c71703d163c2fbebf7609a73488fe116ce644847f117 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9d1ed385beb284bbc3fee07f535c568be328bf5c9990cc10b7e0d50366eee602 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9d1ed385beb284bbc3fee07f535c568be328bf5c9990cc10b7e0d50366eee602->enter($__internal_9d1ed385beb284bbc3fee07f535c568be328bf5c9990cc10b7e0d50366eee602_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_widget.html.php"));

        $__internal_fcc22a4951e27994c2b65909223c718a2cc2b2ba06064104544e91fdb929c157 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fcc22a4951e27994c2b65909223c718a2cc2b2ba06064104544e91fdb929c157->enter($__internal_fcc22a4951e27994c2b65909223c718a2cc2b2ba06064104544e91fdb929c157_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'hidden')) ?>
";
        
        $__internal_9d1ed385beb284bbc3fee07f535c568be328bf5c9990cc10b7e0d50366eee602->leave($__internal_9d1ed385beb284bbc3fee07f535c568be328bf5c9990cc10b7e0d50366eee602_prof);

        
        $__internal_fcc22a4951e27994c2b65909223c718a2cc2b2ba06064104544e91fdb929c157->leave($__internal_fcc22a4951e27994c2b65909223c718a2cc2b2ba06064104544e91fdb929c157_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/hidden_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'hidden')) ?>
", "@Framework/Form/hidden_widget.html.php", "E:\\xampp_new\\htdocs\\Symfony\\store\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\hidden_widget.html.php");
    }
}

<?php

/* WebProfilerBundle:Profiler:open.html.twig */
class __TwigTemplate_e9d9ca4af59ba34e29753c08cfdbfce7dfe17edd15cfd7965bd74bc488ed6c22 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/base.html.twig", "WebProfilerBundle:Profiler:open.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ed1a62f410656de357a9610abf8d16dc1ca4131fe55cdcbc8c67dd2cbcf94995 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ed1a62f410656de357a9610abf8d16dc1ca4131fe55cdcbc8c67dd2cbcf94995->enter($__internal_ed1a62f410656de357a9610abf8d16dc1ca4131fe55cdcbc8c67dd2cbcf94995_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:open.html.twig"));

        $__internal_367c11f123f634e70b397eda8d8bc5f1ec4221ea6d20cc31ad698598c52eb6c5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_367c11f123f634e70b397eda8d8bc5f1ec4221ea6d20cc31ad698598c52eb6c5->enter($__internal_367c11f123f634e70b397eda8d8bc5f1ec4221ea6d20cc31ad698598c52eb6c5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:open.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ed1a62f410656de357a9610abf8d16dc1ca4131fe55cdcbc8c67dd2cbcf94995->leave($__internal_ed1a62f410656de357a9610abf8d16dc1ca4131fe55cdcbc8c67dd2cbcf94995_prof);

        
        $__internal_367c11f123f634e70b397eda8d8bc5f1ec4221ea6d20cc31ad698598c52eb6c5->leave($__internal_367c11f123f634e70b397eda8d8bc5f1ec4221ea6d20cc31ad698598c52eb6c5_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_79e6d3c7908a7175067b053668b6e756c781513cbda77a2212e2f093e19e9d69 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_79e6d3c7908a7175067b053668b6e756c781513cbda77a2212e2f093e19e9d69->enter($__internal_79e6d3c7908a7175067b053668b6e756c781513cbda77a2212e2f093e19e9d69_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_fdf9721af6e32e023bf3f11a3e31086d6d80cf0642d2e3908af8d276d9cdd18e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fdf9721af6e32e023bf3f11a3e31086d6d80cf0642d2e3908af8d276d9cdd18e->enter($__internal_fdf9721af6e32e023bf3f11a3e31086d6d80cf0642d2e3908af8d276d9cdd18e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <style>
        ";
        // line 5
        echo twig_include($this->env, $context, "@WebProfiler/Profiler/open.css.twig");
        echo "
    </style>
";
        
        $__internal_fdf9721af6e32e023bf3f11a3e31086d6d80cf0642d2e3908af8d276d9cdd18e->leave($__internal_fdf9721af6e32e023bf3f11a3e31086d6d80cf0642d2e3908af8d276d9cdd18e_prof);

        
        $__internal_79e6d3c7908a7175067b053668b6e756c781513cbda77a2212e2f093e19e9d69->leave($__internal_79e6d3c7908a7175067b053668b6e756c781513cbda77a2212e2f093e19e9d69_prof);

    }

    // line 9
    public function block_body($context, array $blocks = array())
    {
        $__internal_d3927194715127337280b22c376f340ff7e0e8b32121ff48a4811b23f6eb0d62 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d3927194715127337280b22c376f340ff7e0e8b32121ff48a4811b23f6eb0d62->enter($__internal_d3927194715127337280b22c376f340ff7e0e8b32121ff48a4811b23f6eb0d62_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_531bca88f8cf812bbfee3a95d7f0202a862b6184f39c73858eb60332f5575229 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_531bca88f8cf812bbfee3a95d7f0202a862b6184f39c73858eb60332f5575229->enter($__internal_531bca88f8cf812bbfee3a95d7f0202a862b6184f39c73858eb60332f5575229_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 10
        echo "<div class=\"header\">
    <h1>";
        // line 11
        echo twig_escape_filter($this->env, (isset($context["file"]) ? $context["file"] : $this->getContext($context, "file")), "html", null, true);
        echo " <small>line ";
        echo twig_escape_filter($this->env, (isset($context["line"]) ? $context["line"] : $this->getContext($context, "line")), "html", null, true);
        echo "</small></h1>
    <a class=\"doc\" href=\"https://symfony.com/doc/";
        // line 12
        echo twig_escape_filter($this->env, twig_constant("Symfony\\Component\\HttpKernel\\Kernel::VERSION"), "html", null, true);
        echo "/reference/configuration/framework.html#ide\" rel=\"help\">Open in your IDE?</a>
</div>
<div class=\"source\">
    ";
        // line 15
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\CodeExtension')->fileExcerpt((isset($context["filename"]) ? $context["filename"] : $this->getContext($context, "filename")), (isset($context["line"]) ? $context["line"] : $this->getContext($context, "line")),  -1);
        echo "
</div>
";
        
        $__internal_531bca88f8cf812bbfee3a95d7f0202a862b6184f39c73858eb60332f5575229->leave($__internal_531bca88f8cf812bbfee3a95d7f0202a862b6184f39c73858eb60332f5575229_prof);

        
        $__internal_d3927194715127337280b22c376f340ff7e0e8b32121ff48a4811b23f6eb0d62->leave($__internal_d3927194715127337280b22c376f340ff7e0e8b32121ff48a4811b23f6eb0d62_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:open.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  90 => 15,  84 => 12,  78 => 11,  75 => 10,  66 => 9,  53 => 5,  50 => 4,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/base.html.twig' %}

{% block head %}
    <style>
        {{ include('@WebProfiler/Profiler/open.css.twig') }}
    </style>
{% endblock %}

{% block body %}
<div class=\"header\">
    <h1>{{ file }} <small>line {{ line }}</small></h1>
    <a class=\"doc\" href=\"https://symfony.com/doc/{{ constant('Symfony\\\\Component\\\\HttpKernel\\\\Kernel::VERSION') }}/reference/configuration/framework.html#ide\" rel=\"help\">Open in your IDE?</a>
</div>
<div class=\"source\">
    {{ filename|file_excerpt(line, -1) }}
</div>
{% endblock %}
", "WebProfilerBundle:Profiler:open.html.twig", "E:\\xampp_new\\htdocs\\Symfony\\store\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle/Resources/views/Profiler/open.html.twig");
    }
}

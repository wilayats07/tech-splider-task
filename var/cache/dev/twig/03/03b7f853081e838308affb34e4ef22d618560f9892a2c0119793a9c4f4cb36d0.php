<?php

/* @Twig/Exception/exception.css.twig */
class __TwigTemplate_b7bdd2070b73591cbbb72102022881a93ce767f4583eb851b890635331ba3877 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_89f1ffcc3eb9b8d9885f5a0af0648f3c3e92b8d36eb1c7f7e7cc210cdbd3f8c7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_89f1ffcc3eb9b8d9885f5a0af0648f3c3e92b8d36eb1c7f7e7cc210cdbd3f8c7->enter($__internal_89f1ffcc3eb9b8d9885f5a0af0648f3c3e92b8d36eb1c7f7e7cc210cdbd3f8c7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception.css.twig"));

        $__internal_096e060b99e5e5bdf98b08e057eb3cc900b9183c1f2695dbc239a37403c70e36 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_096e060b99e5e5bdf98b08e057eb3cc900b9183c1f2695dbc239a37403c70e36->enter($__internal_096e060b99e5e5bdf98b08e057eb3cc900b9183c1f2695dbc239a37403c70e36_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception.css.twig"));

        // line 1
        echo "/*
";
        // line 2
        $this->loadTemplate("@Twig/Exception/exception.txt.twig", "@Twig/Exception/exception.css.twig", 2)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        // line 3
        echo "*/
";
        
        $__internal_89f1ffcc3eb9b8d9885f5a0af0648f3c3e92b8d36eb1c7f7e7cc210cdbd3f8c7->leave($__internal_89f1ffcc3eb9b8d9885f5a0af0648f3c3e92b8d36eb1c7f7e7cc210cdbd3f8c7_prof);

        
        $__internal_096e060b99e5e5bdf98b08e057eb3cc900b9183c1f2695dbc239a37403c70e36->leave($__internal_096e060b99e5e5bdf98b08e057eb3cc900b9183c1f2695dbc239a37403c70e36_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception.css.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  30 => 3,  28 => 2,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("/*
{% include '@Twig/Exception/exception.txt.twig' with { 'exception': exception } %}
*/
", "@Twig/Exception/exception.css.twig", "E:\\xampp_new\\htdocs\\Symfony\\store\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle\\Resources\\views\\Exception\\exception.css.twig");
    }
}

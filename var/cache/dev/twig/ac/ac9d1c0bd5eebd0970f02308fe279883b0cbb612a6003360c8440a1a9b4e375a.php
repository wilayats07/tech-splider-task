<?php

/* TwigBundle:Exception:exception.js.twig */
class __TwigTemplate_e7da76f07441292d28aa4d4a4d7349aa799a05b0fe197fdb0282353d41cfe9f3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3b086812b903aec7896101416cca718b8fe12853f20637ed98407e77d087592e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3b086812b903aec7896101416cca718b8fe12853f20637ed98407e77d087592e->enter($__internal_3b086812b903aec7896101416cca718b8fe12853f20637ed98407e77d087592e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.js.twig"));

        $__internal_a4b9992d142e53a8173bd0e1ff2cc75e65fafcc93ee51607fad4b19816bc08a5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a4b9992d142e53a8173bd0e1ff2cc75e65fafcc93ee51607fad4b19816bc08a5->enter($__internal_a4b9992d142e53a8173bd0e1ff2cc75e65fafcc93ee51607fad4b19816bc08a5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.js.twig"));

        // line 1
        echo "/*
";
        // line 2
        $this->loadTemplate("@Twig/Exception/exception.txt.twig", "TwigBundle:Exception:exception.js.twig", 2)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        // line 3
        echo "*/
";
        
        $__internal_3b086812b903aec7896101416cca718b8fe12853f20637ed98407e77d087592e->leave($__internal_3b086812b903aec7896101416cca718b8fe12853f20637ed98407e77d087592e_prof);

        
        $__internal_a4b9992d142e53a8173bd0e1ff2cc75e65fafcc93ee51607fad4b19816bc08a5->leave($__internal_a4b9992d142e53a8173bd0e1ff2cc75e65fafcc93ee51607fad4b19816bc08a5_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.js.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  30 => 3,  28 => 2,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("/*
{% include '@Twig/Exception/exception.txt.twig' with { 'exception': exception } %}
*/
", "TwigBundle:Exception:exception.js.twig", "E:\\xampp_new\\htdocs\\Symfony\\store\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle/Resources/views/Exception/exception.js.twig");
    }
}

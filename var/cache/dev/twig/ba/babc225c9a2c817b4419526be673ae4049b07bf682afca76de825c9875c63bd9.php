<?php

/* @Framework/Form/checkbox_widget.html.php */
class __TwigTemplate_1340be69d0a6a5e116497c35152a2599ed662e07faafbd1f21cd7dfe63d8cec3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1b3a6875f8e8adeacbcb51a7508c31153ba57c7d13a8af777c84bfb34b0d5dd1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1b3a6875f8e8adeacbcb51a7508c31153ba57c7d13a8af777c84bfb34b0d5dd1->enter($__internal_1b3a6875f8e8adeacbcb51a7508c31153ba57c7d13a8af777c84bfb34b0d5dd1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/checkbox_widget.html.php"));

        $__internal_43ddad0a83bd663ee1c1d953cd4fd652c32182452fdd2490f3b24472856c23a3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_43ddad0a83bd663ee1c1d953cd4fd652c32182452fdd2490f3b24472856c23a3->enter($__internal_43ddad0a83bd663ee1c1d953cd4fd652c32182452fdd2490f3b24472856c23a3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/checkbox_widget.html.php"));

        // line 1
        echo "<input type=\"checkbox\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    <?php if (strlen(\$value) > 0): ?> value=\"<?php echo \$view->escape(\$value) ?>\"<?php endif ?>
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
";
        
        $__internal_1b3a6875f8e8adeacbcb51a7508c31153ba57c7d13a8af777c84bfb34b0d5dd1->leave($__internal_1b3a6875f8e8adeacbcb51a7508c31153ba57c7d13a8af777c84bfb34b0d5dd1_prof);

        
        $__internal_43ddad0a83bd663ee1c1d953cd4fd652c32182452fdd2490f3b24472856c23a3->leave($__internal_43ddad0a83bd663ee1c1d953cd4fd652c32182452fdd2490f3b24472856c23a3_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/checkbox_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<input type=\"checkbox\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    <?php if (strlen(\$value) > 0): ?> value=\"<?php echo \$view->escape(\$value) ?>\"<?php endif ?>
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
", "@Framework/Form/checkbox_widget.html.php", "E:\\xampp_new\\htdocs\\Symfony\\store\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\checkbox_widget.html.php");
    }
}

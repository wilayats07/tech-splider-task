<?php

/* :product:edit.html.twig */
class __TwigTemplate_46ae1114c22bb25eca71df73b3f2ed9a8869e71db5371e442de32c2b11fa658c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":product:edit.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_19b789bd137f56ed731d5d24f257fb0d8cf8a7924e4d8128320180c5b0e7f977 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_19b789bd137f56ed731d5d24f257fb0d8cf8a7924e4d8128320180c5b0e7f977->enter($__internal_19b789bd137f56ed731d5d24f257fb0d8cf8a7924e4d8128320180c5b0e7f977_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":product:edit.html.twig"));

        $__internal_6b989399c022400c7addbbea591b7fa7aa9136b870a35a7db6d61905445666c7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6b989399c022400c7addbbea591b7fa7aa9136b870a35a7db6d61905445666c7->enter($__internal_6b989399c022400c7addbbea591b7fa7aa9136b870a35a7db6d61905445666c7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":product:edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_19b789bd137f56ed731d5d24f257fb0d8cf8a7924e4d8128320180c5b0e7f977->leave($__internal_19b789bd137f56ed731d5d24f257fb0d8cf8a7924e4d8128320180c5b0e7f977_prof);

        
        $__internal_6b989399c022400c7addbbea591b7fa7aa9136b870a35a7db6d61905445666c7->leave($__internal_6b989399c022400c7addbbea591b7fa7aa9136b870a35a7db6d61905445666c7_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_178963646511058492e76e6f0ec5a5ec6a65006798cc162c5c534ae3b366b33c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_178963646511058492e76e6f0ec5a5ec6a65006798cc162c5c534ae3b366b33c->enter($__internal_178963646511058492e76e6f0ec5a5ec6a65006798cc162c5c534ae3b366b33c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_aa938fe6fa3211ade2c993e73a31f0fa7d5c29348cca070a23b8cfed3f4c7e56 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_aa938fe6fa3211ade2c993e73a31f0fa7d5c29348cca070a23b8cfed3f4c7e56->enter($__internal_aa938fe6fa3211ade2c993e73a31f0fa7d5c29348cca070a23b8cfed3f4c7e56_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>Product edit</h1>

    ";
        // line 6
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), 'form_start');
        echo "
        ";
        // line 7
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), 'widget');
        echo "
        <input type=\"submit\" value=\"Edit\" />
    ";
        // line 9
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), 'form_end');
        echo "

    <ul>
        <li>
            <a href=\"";
        // line 13
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("product_index");
        echo "\">Back to the list</a>
        </li>
        <li>
            ";
        // line 16
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["delete_form"]) ? $context["delete_form"] : $this->getContext($context, "delete_form")), 'form_start');
        echo "
                <input type=\"submit\" value=\"Delete\">
            ";
        // line 18
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["delete_form"]) ? $context["delete_form"] : $this->getContext($context, "delete_form")), 'form_end');
        echo "
        </li>
    </ul>
";
        
        $__internal_aa938fe6fa3211ade2c993e73a31f0fa7d5c29348cca070a23b8cfed3f4c7e56->leave($__internal_aa938fe6fa3211ade2c993e73a31f0fa7d5c29348cca070a23b8cfed3f4c7e56_prof);

        
        $__internal_178963646511058492e76e6f0ec5a5ec6a65006798cc162c5c534ae3b366b33c->leave($__internal_178963646511058492e76e6f0ec5a5ec6a65006798cc162c5c534ae3b366b33c_prof);

    }

    public function getTemplateName()
    {
        return ":product:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  80 => 18,  75 => 16,  69 => 13,  62 => 9,  57 => 7,  53 => 6,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <h1>Product edit</h1>

    {{ form_start(edit_form) }}
        {{ form_widget(edit_form) }}
        <input type=\"submit\" value=\"Edit\" />
    {{ form_end(edit_form) }}

    <ul>
        <li>
            <a href=\"{{ path('product_index') }}\">Back to the list</a>
        </li>
        <li>
            {{ form_start(delete_form) }}
                <input type=\"submit\" value=\"Delete\">
            {{ form_end(delete_form) }}
        </li>
    </ul>
{% endblock %}
", ":product:edit.html.twig", "E:\\xampp_new\\htdocs\\Symfony\\store\\app/Resources\\views/product/edit.html.twig");
    }
}

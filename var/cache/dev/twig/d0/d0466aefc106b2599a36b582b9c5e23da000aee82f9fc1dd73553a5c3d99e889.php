<?php

/* @Framework/Form/datetime_widget.html.php */
class __TwigTemplate_57123e58bf93206ab01f4fed7e7c46f8ddcdc489f0204084f0faadcd1f9d7d55 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_587a1461c2cca0d7b5e92baf58908241a28c51e06fe3cd8f271760bdb9e8dd2b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_587a1461c2cca0d7b5e92baf58908241a28c51e06fe3cd8f271760bdb9e8dd2b->enter($__internal_587a1461c2cca0d7b5e92baf58908241a28c51e06fe3cd8f271760bdb9e8dd2b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/datetime_widget.html.php"));

        $__internal_2099455f6ab595b0e2436524cdacc1db9ccdc494e8c5b3048734b68077c39488 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2099455f6ab595b0e2436524cdacc1db9ccdc494e8c5b3048734b68077c39488->enter($__internal_2099455f6ab595b0e2436524cdacc1db9ccdc494e8c5b3048734b68077c39488_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/datetime_widget.html.php"));

        // line 1
        echo "<?php if (\$widget == 'single_text'): ?>
    <?php echo \$view['form']->block(\$form, 'form_widget_simple'); ?>
<?php else: ?>
    <div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
        <?php echo \$view['form']->widget(\$form['date']).' '.\$view['form']->widget(\$form['time']) ?>
    </div>
<?php endif ?>
";
        
        $__internal_587a1461c2cca0d7b5e92baf58908241a28c51e06fe3cd8f271760bdb9e8dd2b->leave($__internal_587a1461c2cca0d7b5e92baf58908241a28c51e06fe3cd8f271760bdb9e8dd2b_prof);

        
        $__internal_2099455f6ab595b0e2436524cdacc1db9ccdc494e8c5b3048734b68077c39488->leave($__internal_2099455f6ab595b0e2436524cdacc1db9ccdc494e8c5b3048734b68077c39488_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/datetime_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$widget == 'single_text'): ?>
    <?php echo \$view['form']->block(\$form, 'form_widget_simple'); ?>
<?php else: ?>
    <div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
        <?php echo \$view['form']->widget(\$form['date']).' '.\$view['form']->widget(\$form['time']) ?>
    </div>
<?php endif ?>
", "@Framework/Form/datetime_widget.html.php", "E:\\xampp_new\\htdocs\\Symfony\\store\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\datetime_widget.html.php");
    }
}

<?php

/* @Framework/Form/attributes.html.php */
class __TwigTemplate_0777722e2033d767e7110f3fceb5d3d9ada997afa58d87df59b9c17c9dd55c18 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f088c1737539fdfc5be1701319caabf35da5605184a78a2e9c6d0d078650fa48 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f088c1737539fdfc5be1701319caabf35da5605184a78a2e9c6d0d078650fa48->enter($__internal_f088c1737539fdfc5be1701319caabf35da5605184a78a2e9c6d0d078650fa48_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/attributes.html.php"));

        $__internal_2e686100e6a08c9adbcfb58b52d56cfb2f5ab0545e30e896eb848ddda0f8db5a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2e686100e6a08c9adbcfb58b52d56cfb2f5ab0545e30e896eb848ddda0f8db5a->enter($__internal_2e686100e6a08c9adbcfb58b52d56cfb2f5ab0545e30e896eb848ddda0f8db5a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/attributes.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
";
        
        $__internal_f088c1737539fdfc5be1701319caabf35da5605184a78a2e9c6d0d078650fa48->leave($__internal_f088c1737539fdfc5be1701319caabf35da5605184a78a2e9c6d0d078650fa48_prof);

        
        $__internal_2e686100e6a08c9adbcfb58b52d56cfb2f5ab0545e30e896eb848ddda0f8db5a->leave($__internal_2e686100e6a08c9adbcfb58b52d56cfb2f5ab0545e30e896eb848ddda0f8db5a_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
", "@Framework/Form/attributes.html.php", "E:\\xampp_new\\htdocs\\Symfony\\store\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\attributes.html.php");
    }
}

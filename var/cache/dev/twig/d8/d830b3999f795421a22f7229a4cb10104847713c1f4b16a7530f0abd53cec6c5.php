<?php

/* @Twig/Exception/error.css.twig */
class __TwigTemplate_577890cd89a58c02877e96b96009a6d2eb5a6b0994d624a7ff1d25d2e678e0df extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d7af36569cc78d99677a0642791978a61c3af0c1337fe83d3bae4e058239688d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d7af36569cc78d99677a0642791978a61c3af0c1337fe83d3bae4e058239688d->enter($__internal_d7af36569cc78d99677a0642791978a61c3af0c1337fe83d3bae4e058239688d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/error.css.twig"));

        $__internal_52260b8d3995cf4b14893c13e8626d0ecc02b0d98fc2f7dd53ccf11a067e1f50 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_52260b8d3995cf4b14893c13e8626d0ecc02b0d98fc2f7dd53ccf11a067e1f50->enter($__internal_52260b8d3995cf4b14893c13e8626d0ecc02b0d98fc2f7dd53ccf11a067e1f50_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/error.css.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "css", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "css", null, true);
        echo "

*/
";
        
        $__internal_d7af36569cc78d99677a0642791978a61c3af0c1337fe83d3bae4e058239688d->leave($__internal_d7af36569cc78d99677a0642791978a61c3af0c1337fe83d3bae4e058239688d_prof);

        
        $__internal_52260b8d3995cf4b14893c13e8626d0ecc02b0d98fc2f7dd53ccf11a067e1f50->leave($__internal_52260b8d3995cf4b14893c13e8626d0ecc02b0d98fc2f7dd53ccf11a067e1f50_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/error.css.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 2,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("/*
{{ status_code }} {{ status_text }}

*/
", "@Twig/Exception/error.css.twig", "E:\\xampp_new\\htdocs\\Symfony\\store\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle\\Resources\\views\\Exception\\error.css.twig");
    }
}

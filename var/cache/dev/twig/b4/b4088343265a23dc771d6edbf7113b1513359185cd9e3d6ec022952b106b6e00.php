<?php

/* base.html.twig */
class __TwigTemplate_8b9af363f79570f54baa8c6ff66ed3fd291fa90ae054c39ca27af4c2c64f332b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_80441b35aa278b40cee54c3d795921aeefe81b6c00ea7abb8adb8cbb69acdb59 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_80441b35aa278b40cee54c3d795921aeefe81b6c00ea7abb8adb8cbb69acdb59->enter($__internal_80441b35aa278b40cee54c3d795921aeefe81b6c00ea7abb8adb8cbb69acdb59_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        $__internal_3e4a2979b15704e4624c4391d53091629d31c2ca196d16658fc301e7efc47bfb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3e4a2979b15704e4624c4391d53091629d31c2ca196d16658fc301e7efc47bfb->enter($__internal_3e4a2979b15704e4624c4391d53091629d31c2ca196d16658fc301e7efc47bfb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
        <!-- Latest compiled and minified CSS -->
        <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\" integrity=\"sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u\" crossorigin=\"anonymous\">
    </head>
    <body>
        ";
        // line 12
        $this->displayBlock('body', $context, $blocks);
        // line 13
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 14
        echo "    </body>
</html>
";
        
        $__internal_80441b35aa278b40cee54c3d795921aeefe81b6c00ea7abb8adb8cbb69acdb59->leave($__internal_80441b35aa278b40cee54c3d795921aeefe81b6c00ea7abb8adb8cbb69acdb59_prof);

        
        $__internal_3e4a2979b15704e4624c4391d53091629d31c2ca196d16658fc301e7efc47bfb->leave($__internal_3e4a2979b15704e4624c4391d53091629d31c2ca196d16658fc301e7efc47bfb_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_cf26dba72bac773c26bc54d071364aa3a685f82f2135e7993288bf29df2b0f06 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_cf26dba72bac773c26bc54d071364aa3a685f82f2135e7993288bf29df2b0f06->enter($__internal_cf26dba72bac773c26bc54d071364aa3a685f82f2135e7993288bf29df2b0f06_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_131d2202706da4cf99b24761180fca80cd56e3545d1893e23dceabcbc6479b50 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_131d2202706da4cf99b24761180fca80cd56e3545d1893e23dceabcbc6479b50->enter($__internal_131d2202706da4cf99b24761180fca80cd56e3545d1893e23dceabcbc6479b50_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_131d2202706da4cf99b24761180fca80cd56e3545d1893e23dceabcbc6479b50->leave($__internal_131d2202706da4cf99b24761180fca80cd56e3545d1893e23dceabcbc6479b50_prof);

        
        $__internal_cf26dba72bac773c26bc54d071364aa3a685f82f2135e7993288bf29df2b0f06->leave($__internal_cf26dba72bac773c26bc54d071364aa3a685f82f2135e7993288bf29df2b0f06_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_f8931ce34e7eb045b8321538f87ae8bc45b318cf62401f14235929a8786b2255 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f8931ce34e7eb045b8321538f87ae8bc45b318cf62401f14235929a8786b2255->enter($__internal_f8931ce34e7eb045b8321538f87ae8bc45b318cf62401f14235929a8786b2255_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_3d5fee42fd89e5283a8c830c5f4ddd2452e190c1f048eaa5bd166e1e00894f9d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3d5fee42fd89e5283a8c830c5f4ddd2452e190c1f048eaa5bd166e1e00894f9d->enter($__internal_3d5fee42fd89e5283a8c830c5f4ddd2452e190c1f048eaa5bd166e1e00894f9d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_3d5fee42fd89e5283a8c830c5f4ddd2452e190c1f048eaa5bd166e1e00894f9d->leave($__internal_3d5fee42fd89e5283a8c830c5f4ddd2452e190c1f048eaa5bd166e1e00894f9d_prof);

        
        $__internal_f8931ce34e7eb045b8321538f87ae8bc45b318cf62401f14235929a8786b2255->leave($__internal_f8931ce34e7eb045b8321538f87ae8bc45b318cf62401f14235929a8786b2255_prof);

    }

    // line 12
    public function block_body($context, array $blocks = array())
    {
        $__internal_e76ff6000c5d342df71424968284e1ee93c819b86cd5ed283fa930b8265c2b00 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e76ff6000c5d342df71424968284e1ee93c819b86cd5ed283fa930b8265c2b00->enter($__internal_e76ff6000c5d342df71424968284e1ee93c819b86cd5ed283fa930b8265c2b00_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_bc64ac9947d23a397dc8cf99fe00212cbfae2a76634fb3dea370af4fb37fa701 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bc64ac9947d23a397dc8cf99fe00212cbfae2a76634fb3dea370af4fb37fa701->enter($__internal_bc64ac9947d23a397dc8cf99fe00212cbfae2a76634fb3dea370af4fb37fa701_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_bc64ac9947d23a397dc8cf99fe00212cbfae2a76634fb3dea370af4fb37fa701->leave($__internal_bc64ac9947d23a397dc8cf99fe00212cbfae2a76634fb3dea370af4fb37fa701_prof);

        
        $__internal_e76ff6000c5d342df71424968284e1ee93c819b86cd5ed283fa930b8265c2b00->leave($__internal_e76ff6000c5d342df71424968284e1ee93c819b86cd5ed283fa930b8265c2b00_prof);

    }

    // line 13
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_3947655cd57dddd184f7300d2f9d95d2d04cb858dfb24c88fa0ea23143a13211 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3947655cd57dddd184f7300d2f9d95d2d04cb858dfb24c88fa0ea23143a13211->enter($__internal_3947655cd57dddd184f7300d2f9d95d2d04cb858dfb24c88fa0ea23143a13211_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_258d953a2ec79008c7c07fb8e865cd2a9a59860355196796f8b3cf607d96671e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_258d953a2ec79008c7c07fb8e865cd2a9a59860355196796f8b3cf607d96671e->enter($__internal_258d953a2ec79008c7c07fb8e865cd2a9a59860355196796f8b3cf607d96671e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_258d953a2ec79008c7c07fb8e865cd2a9a59860355196796f8b3cf607d96671e->leave($__internal_258d953a2ec79008c7c07fb8e865cd2a9a59860355196796f8b3cf607d96671e_prof);

        
        $__internal_3947655cd57dddd184f7300d2f9d95d2d04cb858dfb24c88fa0ea23143a13211->leave($__internal_3947655cd57dddd184f7300d2f9d95d2d04cb858dfb24c88fa0ea23143a13211_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  119 => 13,  102 => 12,  85 => 6,  67 => 5,  55 => 14,  52 => 13,  50 => 12,  41 => 7,  39 => 6,  35 => 5,  29 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>{% block title %}Welcome!{% endblock %}</title>
        {% block stylesheets %}{% endblock %}
        <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />
        <!-- Latest compiled and minified CSS -->
        <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\" integrity=\"sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u\" crossorigin=\"anonymous\">
    </head>
    <body>
        {% block body %}{% endblock %}
        {% block javascripts %}{% endblock %}
    </body>
</html>
", "base.html.twig", "E:\\xampp_new\\htdocs\\Symfony\\store\\app\\Resources\\views\\base.html.twig");
    }
}

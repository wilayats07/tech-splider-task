<?php

/* @Twig/Exception/exception.atom.twig */
class __TwigTemplate_ecccba5efc0f5ce1136c0faa19b8808db3d21c868cb69e7cf93bb6186a2a9d56 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7e505f3ff358bf67ed73c38a58cd8adf11f34636b6764c6d8d2977abc4d9c2cf = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7e505f3ff358bf67ed73c38a58cd8adf11f34636b6764c6d8d2977abc4d9c2cf->enter($__internal_7e505f3ff358bf67ed73c38a58cd8adf11f34636b6764c6d8d2977abc4d9c2cf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception.atom.twig"));

        $__internal_6d961746377c82f3970feb0a735e5b3c75324a8b280b9ab0f9f8eb66678e28af = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6d961746377c82f3970feb0a735e5b3c75324a8b280b9ab0f9f8eb66678e28af->enter($__internal_6d961746377c82f3970feb0a735e5b3c75324a8b280b9ab0f9f8eb66678e28af_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception.atom.twig"));

        // line 1
        $this->loadTemplate("@Twig/Exception/exception.xml.twig", "@Twig/Exception/exception.atom.twig", 1)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        
        $__internal_7e505f3ff358bf67ed73c38a58cd8adf11f34636b6764c6d8d2977abc4d9c2cf->leave($__internal_7e505f3ff358bf67ed73c38a58cd8adf11f34636b6764c6d8d2977abc4d9c2cf_prof);

        
        $__internal_6d961746377c82f3970feb0a735e5b3c75324a8b280b9ab0f9f8eb66678e28af->leave($__internal_6d961746377c82f3970feb0a735e5b3c75324a8b280b9ab0f9f8eb66678e28af_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception.atom.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% include '@Twig/Exception/exception.xml.twig' with { 'exception': exception } %}
", "@Twig/Exception/exception.atom.twig", "E:\\xampp_new\\htdocs\\Symfony\\store\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle\\Resources\\views\\Exception\\exception.atom.twig");
    }
}

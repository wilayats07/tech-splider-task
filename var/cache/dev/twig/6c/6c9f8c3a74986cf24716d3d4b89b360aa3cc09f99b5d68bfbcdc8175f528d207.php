<?php

/* @Framework/Form/form_enctype.html.php */
class __TwigTemplate_eb43fa682f0debfdc43812a171140529219ef624edb1c3c8a017b88dfb973c30 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_00ca5a5c652f29ef31191bac1a6272b204b7d1ec816d99435d1d274a71e8017f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_00ca5a5c652f29ef31191bac1a6272b204b7d1ec816d99435d1d274a71e8017f->enter($__internal_00ca5a5c652f29ef31191bac1a6272b204b7d1ec816d99435d1d274a71e8017f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_enctype.html.php"));

        $__internal_b47d3043c87404b887b2bd1a870eaef8718ac30ea3da2799bd9496e809c70379 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b47d3043c87404b887b2bd1a870eaef8718ac30ea3da2799bd9496e809c70379->enter($__internal_b47d3043c87404b887b2bd1a870eaef8718ac30ea3da2799bd9496e809c70379_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_enctype.html.php"));

        // line 1
        echo "<?php if (\$form->vars['multipart']): ?>enctype=\"multipart/form-data\"<?php endif ?>
";
        
        $__internal_00ca5a5c652f29ef31191bac1a6272b204b7d1ec816d99435d1d274a71e8017f->leave($__internal_00ca5a5c652f29ef31191bac1a6272b204b7d1ec816d99435d1d274a71e8017f_prof);

        
        $__internal_b47d3043c87404b887b2bd1a870eaef8718ac30ea3da2799bd9496e809c70379->leave($__internal_b47d3043c87404b887b2bd1a870eaef8718ac30ea3da2799bd9496e809c70379_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_enctype.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$form->vars['multipart']): ?>enctype=\"multipart/form-data\"<?php endif ?>
", "@Framework/Form/form_enctype.html.php", "E:\\xampp_new\\htdocs\\Symfony\\store\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\form_enctype.html.php");
    }
}

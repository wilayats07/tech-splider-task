<?php

/* @WebProfiler/Collector/exception.html.twig */
class __TwigTemplate_d0ea04b0681df5be4f4372891dbf57cd265f736c45c771d50e480f083206011b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/exception.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_516ec4c1759026971cd77bfbac54fd838817b54b6af2c72ef74b19e45b178133 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_516ec4c1759026971cd77bfbac54fd838817b54b6af2c72ef74b19e45b178133->enter($__internal_516ec4c1759026971cd77bfbac54fd838817b54b6af2c72ef74b19e45b178133_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $__internal_837b0ad1a66df2ca6779e35fb95561e85b7d630acbde4da2cfd7fafaaebea5af = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_837b0ad1a66df2ca6779e35fb95561e85b7d630acbde4da2cfd7fafaaebea5af->enter($__internal_837b0ad1a66df2ca6779e35fb95561e85b7d630acbde4da2cfd7fafaaebea5af_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_516ec4c1759026971cd77bfbac54fd838817b54b6af2c72ef74b19e45b178133->leave($__internal_516ec4c1759026971cd77bfbac54fd838817b54b6af2c72ef74b19e45b178133_prof);

        
        $__internal_837b0ad1a66df2ca6779e35fb95561e85b7d630acbde4da2cfd7fafaaebea5af->leave($__internal_837b0ad1a66df2ca6779e35fb95561e85b7d630acbde4da2cfd7fafaaebea5af_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_591684756f2f3f4c36f1b2c53d9bed79503edfa691fe411784bdd0bebef4da9a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_591684756f2f3f4c36f1b2c53d9bed79503edfa691fe411784bdd0bebef4da9a->enter($__internal_591684756f2f3f4c36f1b2c53d9bed79503edfa691fe411784bdd0bebef4da9a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_5ec75076a8b1755071fca66519b0631eeeb1b4d62f171ce1b164589484e91e8e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5ec75076a8b1755071fca66519b0631eeeb1b4d62f171ce1b164589484e91e8e->enter($__internal_5ec75076a8b1755071fca66519b0631eeeb1b4d62f171ce1b164589484e91e8e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    ";
        if ($this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "hasexception", array())) {
            // line 5
            echo "        <style>
            ";
            // line 6
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception_css", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
            echo "
        </style>
    ";
        }
        // line 9
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
";
        
        $__internal_5ec75076a8b1755071fca66519b0631eeeb1b4d62f171ce1b164589484e91e8e->leave($__internal_5ec75076a8b1755071fca66519b0631eeeb1b4d62f171ce1b164589484e91e8e_prof);

        
        $__internal_591684756f2f3f4c36f1b2c53d9bed79503edfa691fe411784bdd0bebef4da9a->leave($__internal_591684756f2f3f4c36f1b2c53d9bed79503edfa691fe411784bdd0bebef4da9a_prof);

    }

    // line 12
    public function block_menu($context, array $blocks = array())
    {
        $__internal_e25a47e650014acbb64f90001de7861e6915634f991cf7bf9a64ba65a9a4f0f7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e25a47e650014acbb64f90001de7861e6915634f991cf7bf9a64ba65a9a4f0f7->enter($__internal_e25a47e650014acbb64f90001de7861e6915634f991cf7bf9a64ba65a9a4f0f7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_331a757a412ddc9aa452465f048b1d3aab1aeecac29bcdba86d8dfc3b7df094c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_331a757a412ddc9aa452465f048b1d3aab1aeecac29bcdba86d8dfc3b7df094c->enter($__internal_331a757a412ddc9aa452465f048b1d3aab1aeecac29bcdba86d8dfc3b7df094c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 13
        echo "    <span class=\"label ";
        echo (($this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "hasexception", array())) ? ("label-status-error") : ("disabled"));
        echo "\">
        <span class=\"icon\">";
        // line 14
        echo twig_include($this->env, $context, "@WebProfiler/Icon/exception.svg");
        echo "</span>
        <strong>Exception</strong>
        ";
        // line 16
        if ($this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "hasexception", array())) {
            // line 17
            echo "            <span class=\"count\">
                <span>1</span>
            </span>
        ";
        }
        // line 21
        echo "    </span>
";
        
        $__internal_331a757a412ddc9aa452465f048b1d3aab1aeecac29bcdba86d8dfc3b7df094c->leave($__internal_331a757a412ddc9aa452465f048b1d3aab1aeecac29bcdba86d8dfc3b7df094c_prof);

        
        $__internal_e25a47e650014acbb64f90001de7861e6915634f991cf7bf9a64ba65a9a4f0f7->leave($__internal_e25a47e650014acbb64f90001de7861e6915634f991cf7bf9a64ba65a9a4f0f7_prof);

    }

    // line 24
    public function block_panel($context, array $blocks = array())
    {
        $__internal_9929aa87059abdbefbf5574a2c7f81800e53a82bbc70d6bf5052cd6519f5dd7e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9929aa87059abdbefbf5574a2c7f81800e53a82bbc70d6bf5052cd6519f5dd7e->enter($__internal_9929aa87059abdbefbf5574a2c7f81800e53a82bbc70d6bf5052cd6519f5dd7e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_c7a59cd46f873c83c95ae3992ef049278be3120e404f8218d199a69fcbea7199 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c7a59cd46f873c83c95ae3992ef049278be3120e404f8218d199a69fcbea7199->enter($__internal_c7a59cd46f873c83c95ae3992ef049278be3120e404f8218d199a69fcbea7199_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 25
        echo "    <h2>Exceptions</h2>

    ";
        // line 27
        if ( !$this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "hasexception", array())) {
            // line 28
            echo "        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    ";
        } else {
            // line 32
            echo "        <div class=\"sf-reset\">
            ";
            // line 33
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
            echo "
        </div>
    ";
        }
        
        $__internal_c7a59cd46f873c83c95ae3992ef049278be3120e404f8218d199a69fcbea7199->leave($__internal_c7a59cd46f873c83c95ae3992ef049278be3120e404f8218d199a69fcbea7199_prof);

        
        $__internal_9929aa87059abdbefbf5574a2c7f81800e53a82bbc70d6bf5052cd6519f5dd7e->leave($__internal_9929aa87059abdbefbf5574a2c7f81800e53a82bbc70d6bf5052cd6519f5dd7e_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/exception.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  138 => 33,  135 => 32,  129 => 28,  127 => 27,  123 => 25,  114 => 24,  103 => 21,  97 => 17,  95 => 16,  90 => 14,  85 => 13,  76 => 12,  63 => 9,  57 => 6,  54 => 5,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block head %}
    {% if collector.hasexception %}
        <style>
            {{ render(path('_profiler_exception_css', { token: token })) }}
        </style>
    {% endif %}
    {{ parent() }}
{% endblock %}

{% block menu %}
    <span class=\"label {{ collector.hasexception ? 'label-status-error' : 'disabled' }}\">
        <span class=\"icon\">{{ include('@WebProfiler/Icon/exception.svg') }}</span>
        <strong>Exception</strong>
        {% if collector.hasexception %}
            <span class=\"count\">
                <span>1</span>
            </span>
        {% endif %}
    </span>
{% endblock %}

{% block panel %}
    <h2>Exceptions</h2>

    {% if not collector.hasexception %}
        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    {% else %}
        <div class=\"sf-reset\">
            {{ render(path('_profiler_exception', { token: token })) }}
        </div>
    {% endif %}
{% endblock %}
", "@WebProfiler/Collector/exception.html.twig", "E:\\xampp_new\\htdocs\\Symfony\\store\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle\\Resources\\views\\Collector\\exception.html.twig");
    }
}

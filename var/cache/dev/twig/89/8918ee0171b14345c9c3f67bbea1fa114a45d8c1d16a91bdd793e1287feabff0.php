<?php

/* @Framework/Form/number_widget.html.php */
class __TwigTemplate_e1b6daaab3e531b382c95b76f6a2057edf6327da5799597e2393dfeb375998ee extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3908b8f8e51e45f2915eff95683b25c1db71cf31a2c524321c1ff6c38aea47cd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3908b8f8e51e45f2915eff95683b25c1db71cf31a2c524321c1ff6c38aea47cd->enter($__internal_3908b8f8e51e45f2915eff95683b25c1db71cf31a2c524321c1ff6c38aea47cd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/number_widget.html.php"));

        $__internal_b3aad85aad06c633bbe4702e4a972206d9b351dd94ff31761259b17f1e194ae0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b3aad85aad06c633bbe4702e4a972206d9b351dd94ff31761259b17f1e194ae0->enter($__internal_b3aad85aad06c633bbe4702e4a972206d9b351dd94ff31761259b17f1e194ae0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/number_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'text')) ?>
";
        
        $__internal_3908b8f8e51e45f2915eff95683b25c1db71cf31a2c524321c1ff6c38aea47cd->leave($__internal_3908b8f8e51e45f2915eff95683b25c1db71cf31a2c524321c1ff6c38aea47cd_prof);

        
        $__internal_b3aad85aad06c633bbe4702e4a972206d9b351dd94ff31761259b17f1e194ae0->leave($__internal_b3aad85aad06c633bbe4702e4a972206d9b351dd94ff31761259b17f1e194ae0_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/number_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'text')) ?>
", "@Framework/Form/number_widget.html.php", "E:\\xampp_new\\htdocs\\Symfony\\store\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\number_widget.html.php");
    }
}

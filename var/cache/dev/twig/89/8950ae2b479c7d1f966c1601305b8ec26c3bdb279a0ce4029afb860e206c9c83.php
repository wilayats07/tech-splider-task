<?php

/* product/new.html.twig */
class __TwigTemplate_66415fab9304b8c836b7685448b9e19f7c5bc868a961f00efad0ec47e3e9ac49 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "product/new.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_48f1745d6065188cfc20869cbf0edc60186dd8b11c3a95d324812c37ae4f7f50 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_48f1745d6065188cfc20869cbf0edc60186dd8b11c3a95d324812c37ae4f7f50->enter($__internal_48f1745d6065188cfc20869cbf0edc60186dd8b11c3a95d324812c37ae4f7f50_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "product/new.html.twig"));

        $__internal_549f1e06e9453f2cbf2fa35a193ab9c1280aee07b830d9924b6ad0dbd5f57a0c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_549f1e06e9453f2cbf2fa35a193ab9c1280aee07b830d9924b6ad0dbd5f57a0c->enter($__internal_549f1e06e9453f2cbf2fa35a193ab9c1280aee07b830d9924b6ad0dbd5f57a0c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "product/new.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_48f1745d6065188cfc20869cbf0edc60186dd8b11c3a95d324812c37ae4f7f50->leave($__internal_48f1745d6065188cfc20869cbf0edc60186dd8b11c3a95d324812c37ae4f7f50_prof);

        
        $__internal_549f1e06e9453f2cbf2fa35a193ab9c1280aee07b830d9924b6ad0dbd5f57a0c->leave($__internal_549f1e06e9453f2cbf2fa35a193ab9c1280aee07b830d9924b6ad0dbd5f57a0c_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_27ebbd9c79b23b5dfd8abad27a9567443cff0d080dd35fff248738a522e8baeb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_27ebbd9c79b23b5dfd8abad27a9567443cff0d080dd35fff248738a522e8baeb->enter($__internal_27ebbd9c79b23b5dfd8abad27a9567443cff0d080dd35fff248738a522e8baeb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_d20bad3680ee87f71f22836e99ceab45460c65b43858df101eb3336441935a63 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d20bad3680ee87f71f22836e99ceab45460c65b43858df101eb3336441935a63->enter($__internal_d20bad3680ee87f71f22836e99ceab45460c65b43858df101eb3336441935a63_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>Product creation</h1>

    ";
        // line 6
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start');
        echo "
        ";
        // line 7
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        echo "
        <input type=\"submit\" value=\"Create\" />
    ";
        // line 9
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "

    <ul>
        <li>
            <a href=\"";
        // line 13
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("product_index");
        echo "\">Back to the list</a>
        </li>
    </ul>
";
        
        $__internal_d20bad3680ee87f71f22836e99ceab45460c65b43858df101eb3336441935a63->leave($__internal_d20bad3680ee87f71f22836e99ceab45460c65b43858df101eb3336441935a63_prof);

        
        $__internal_27ebbd9c79b23b5dfd8abad27a9567443cff0d080dd35fff248738a522e8baeb->leave($__internal_27ebbd9c79b23b5dfd8abad27a9567443cff0d080dd35fff248738a522e8baeb_prof);

    }

    public function getTemplateName()
    {
        return "product/new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  69 => 13,  62 => 9,  57 => 7,  53 => 6,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <h1>Product creation</h1>

    {{ form_start(form) }}
        {{ form_widget(form) }}
        <input type=\"submit\" value=\"Create\" />
    {{ form_end(form) }}

    <ul>
        <li>
            <a href=\"{{ path('product_index') }}\">Back to the list</a>
        </li>
    </ul>
{% endblock %}
", "product/new.html.twig", "E:\\xampp_new\\htdocs\\Symfony\\store\\app\\Resources\\views\\product\\new.html.twig");
    }
}

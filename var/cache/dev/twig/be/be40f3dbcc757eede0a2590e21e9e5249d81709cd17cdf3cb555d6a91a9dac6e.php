<?php

/* product/edit.html.twig */
class __TwigTemplate_4981f8aa4a55608f279c0a3c2fbb96a3b9c1d5f71df7a4edcaa3e7529ee36479 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "product/edit.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f4819732204e6976f563835d4b6728c7fad0f5c64b92dec2bc90ea5e7c5469cd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f4819732204e6976f563835d4b6728c7fad0f5c64b92dec2bc90ea5e7c5469cd->enter($__internal_f4819732204e6976f563835d4b6728c7fad0f5c64b92dec2bc90ea5e7c5469cd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "product/edit.html.twig"));

        $__internal_fc342ef1ffe934fe646c0ad194e495cb441236620bd5f80a23a0cdc47f526651 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fc342ef1ffe934fe646c0ad194e495cb441236620bd5f80a23a0cdc47f526651->enter($__internal_fc342ef1ffe934fe646c0ad194e495cb441236620bd5f80a23a0cdc47f526651_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "product/edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_f4819732204e6976f563835d4b6728c7fad0f5c64b92dec2bc90ea5e7c5469cd->leave($__internal_f4819732204e6976f563835d4b6728c7fad0f5c64b92dec2bc90ea5e7c5469cd_prof);

        
        $__internal_fc342ef1ffe934fe646c0ad194e495cb441236620bd5f80a23a0cdc47f526651->leave($__internal_fc342ef1ffe934fe646c0ad194e495cb441236620bd5f80a23a0cdc47f526651_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_6f94c057cedb4feaaeff490e66301273d58a3620c68cb73e269206ffada8417f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6f94c057cedb4feaaeff490e66301273d58a3620c68cb73e269206ffada8417f->enter($__internal_6f94c057cedb4feaaeff490e66301273d58a3620c68cb73e269206ffada8417f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_bf18250858b43c9bea39704ca21554bfcbaabbab7c64143c7ee56736d3350bbd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bf18250858b43c9bea39704ca21554bfcbaabbab7c64143c7ee56736d3350bbd->enter($__internal_bf18250858b43c9bea39704ca21554bfcbaabbab7c64143c7ee56736d3350bbd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>Product edit</h1>

    ";
        // line 6
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), 'form_start');
        echo "
        ";
        // line 7
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), 'widget');
        echo "
        <input type=\"submit\" value=\"Edit\" />
    ";
        // line 9
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), 'form_end');
        echo "

    <ul>
        <li>
            <a href=\"";
        // line 13
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("product_index");
        echo "\">Back to the list</a>
        </li>
        <li>
            ";
        // line 16
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["delete_form"]) ? $context["delete_form"] : $this->getContext($context, "delete_form")), 'form_start');
        echo "
                <input type=\"submit\" value=\"Delete\">
            ";
        // line 18
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["delete_form"]) ? $context["delete_form"] : $this->getContext($context, "delete_form")), 'form_end');
        echo "
        </li>
    </ul>
";
        
        $__internal_bf18250858b43c9bea39704ca21554bfcbaabbab7c64143c7ee56736d3350bbd->leave($__internal_bf18250858b43c9bea39704ca21554bfcbaabbab7c64143c7ee56736d3350bbd_prof);

        
        $__internal_6f94c057cedb4feaaeff490e66301273d58a3620c68cb73e269206ffada8417f->leave($__internal_6f94c057cedb4feaaeff490e66301273d58a3620c68cb73e269206ffada8417f_prof);

    }

    public function getTemplateName()
    {
        return "product/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  80 => 18,  75 => 16,  69 => 13,  62 => 9,  57 => 7,  53 => 6,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <h1>Product edit</h1>

    {{ form_start(edit_form) }}
        {{ form_widget(edit_form) }}
        <input type=\"submit\" value=\"Edit\" />
    {{ form_end(edit_form) }}

    <ul>
        <li>
            <a href=\"{{ path('product_index') }}\">Back to the list</a>
        </li>
        <li>
            {{ form_start(delete_form) }}
                <input type=\"submit\" value=\"Delete\">
            {{ form_end(delete_form) }}
        </li>
    </ul>
{% endblock %}
", "product/edit.html.twig", "E:\\xampp_new\\htdocs\\Symfony\\store\\app\\Resources\\views\\product\\edit.html.twig");
    }
}

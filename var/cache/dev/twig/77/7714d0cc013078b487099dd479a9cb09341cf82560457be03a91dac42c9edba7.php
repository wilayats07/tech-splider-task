<?php

/* @Framework/Form/choice_options.html.php */
class __TwigTemplate_91cd3b4ba3857eed529c4af2c966bc9b24432d1f46c443d90b030406e8c6b856 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8ef86d145bedd38de0b1c6ab5feaf09f3eef1fecb0e15414143086bcf07bedbe = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8ef86d145bedd38de0b1c6ab5feaf09f3eef1fecb0e15414143086bcf07bedbe->enter($__internal_8ef86d145bedd38de0b1c6ab5feaf09f3eef1fecb0e15414143086bcf07bedbe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_options.html.php"));

        $__internal_ccbafdee5eaac91359796e414c0a33037621471a6e0add9b6f254c27780f6ba3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ccbafdee5eaac91359796e414c0a33037621471a6e0add9b6f254c27780f6ba3->enter($__internal_ccbafdee5eaac91359796e414c0a33037621471a6e0add9b6f254c27780f6ba3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_options.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'choice_widget_options') ?>
";
        
        $__internal_8ef86d145bedd38de0b1c6ab5feaf09f3eef1fecb0e15414143086bcf07bedbe->leave($__internal_8ef86d145bedd38de0b1c6ab5feaf09f3eef1fecb0e15414143086bcf07bedbe_prof);

        
        $__internal_ccbafdee5eaac91359796e414c0a33037621471a6e0add9b6f254c27780f6ba3->leave($__internal_ccbafdee5eaac91359796e414c0a33037621471a6e0add9b6f254c27780f6ba3_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_options.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'choice_widget_options') ?>
", "@Framework/Form/choice_options.html.php", "E:\\xampp_new\\htdocs\\Symfony\\store\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\choice_options.html.php");
    }
}

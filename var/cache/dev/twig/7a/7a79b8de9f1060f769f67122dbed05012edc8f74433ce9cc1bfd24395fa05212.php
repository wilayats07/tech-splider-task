<?php

/* @Twig/Exception/exception.rdf.twig */
class __TwigTemplate_9a37ad8ddc3020dafa241d65b7f92c049acf411addc3fdb58c1f927a77adf5a8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7f476ae85d19a033de717eb10cc30ef503e4fc69b03f476ec1eb9964007e0c0f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7f476ae85d19a033de717eb10cc30ef503e4fc69b03f476ec1eb9964007e0c0f->enter($__internal_7f476ae85d19a033de717eb10cc30ef503e4fc69b03f476ec1eb9964007e0c0f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception.rdf.twig"));

        $__internal_0e9ebcdef9fbeb52fa1ae2545ba8929e50b4858949174551a9c568017f0f0b3b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0e9ebcdef9fbeb52fa1ae2545ba8929e50b4858949174551a9c568017f0f0b3b->enter($__internal_0e9ebcdef9fbeb52fa1ae2545ba8929e50b4858949174551a9c568017f0f0b3b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception.rdf.twig"));

        // line 1
        $this->loadTemplate("@Twig/Exception/exception.xml.twig", "@Twig/Exception/exception.rdf.twig", 1)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        
        $__internal_7f476ae85d19a033de717eb10cc30ef503e4fc69b03f476ec1eb9964007e0c0f->leave($__internal_7f476ae85d19a033de717eb10cc30ef503e4fc69b03f476ec1eb9964007e0c0f_prof);

        
        $__internal_0e9ebcdef9fbeb52fa1ae2545ba8929e50b4858949174551a9c568017f0f0b3b->leave($__internal_0e9ebcdef9fbeb52fa1ae2545ba8929e50b4858949174551a9c568017f0f0b3b_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception.rdf.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% include '@Twig/Exception/exception.xml.twig' with { 'exception': exception } %}
", "@Twig/Exception/exception.rdf.twig", "E:\\xampp_new\\htdocs\\Symfony\\store\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle\\Resources\\views\\Exception\\exception.rdf.twig");
    }
}

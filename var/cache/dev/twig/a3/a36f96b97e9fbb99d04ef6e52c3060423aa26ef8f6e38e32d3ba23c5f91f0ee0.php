<?php

/* WebProfilerBundle:Collector:exception.html.twig */
class __TwigTemplate_d181e90151c7f4f75e82a6c017a43c025490c0eb156a84345f2b1d5f879fef92 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "WebProfilerBundle:Collector:exception.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a2b2ade7341be1d762219c39b14f7687a9bda34ede8a32cd737ee4d3fe475c57 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a2b2ade7341be1d762219c39b14f7687a9bda34ede8a32cd737ee4d3fe475c57->enter($__internal_a2b2ade7341be1d762219c39b14f7687a9bda34ede8a32cd737ee4d3fe475c57_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:exception.html.twig"));

        $__internal_eab5cb5c0c6a319d633b378a2647c8c0d279835abd5e7d2d2353557867b0bfe6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_eab5cb5c0c6a319d633b378a2647c8c0d279835abd5e7d2d2353557867b0bfe6->enter($__internal_eab5cb5c0c6a319d633b378a2647c8c0d279835abd5e7d2d2353557867b0bfe6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:exception.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_a2b2ade7341be1d762219c39b14f7687a9bda34ede8a32cd737ee4d3fe475c57->leave($__internal_a2b2ade7341be1d762219c39b14f7687a9bda34ede8a32cd737ee4d3fe475c57_prof);

        
        $__internal_eab5cb5c0c6a319d633b378a2647c8c0d279835abd5e7d2d2353557867b0bfe6->leave($__internal_eab5cb5c0c6a319d633b378a2647c8c0d279835abd5e7d2d2353557867b0bfe6_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_cfda9d4670b04051dece72abde14c284b10abd267682da8e48571df15f9482b7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_cfda9d4670b04051dece72abde14c284b10abd267682da8e48571df15f9482b7->enter($__internal_cfda9d4670b04051dece72abde14c284b10abd267682da8e48571df15f9482b7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_e20f11daa9cf01d2ec16efbeb717eacc65b6c4b1de6abb362a1c186ace933b24 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e20f11daa9cf01d2ec16efbeb717eacc65b6c4b1de6abb362a1c186ace933b24->enter($__internal_e20f11daa9cf01d2ec16efbeb717eacc65b6c4b1de6abb362a1c186ace933b24_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    ";
        if ($this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "hasexception", array())) {
            // line 5
            echo "        <style>
            ";
            // line 6
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception_css", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
            echo "
        </style>
    ";
        }
        // line 9
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
";
        
        $__internal_e20f11daa9cf01d2ec16efbeb717eacc65b6c4b1de6abb362a1c186ace933b24->leave($__internal_e20f11daa9cf01d2ec16efbeb717eacc65b6c4b1de6abb362a1c186ace933b24_prof);

        
        $__internal_cfda9d4670b04051dece72abde14c284b10abd267682da8e48571df15f9482b7->leave($__internal_cfda9d4670b04051dece72abde14c284b10abd267682da8e48571df15f9482b7_prof);

    }

    // line 12
    public function block_menu($context, array $blocks = array())
    {
        $__internal_122cef29695e9237010e0cb2af44143828d49bf7ff87bd494a6842a703640b04 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_122cef29695e9237010e0cb2af44143828d49bf7ff87bd494a6842a703640b04->enter($__internal_122cef29695e9237010e0cb2af44143828d49bf7ff87bd494a6842a703640b04_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_9ac68b9f1e03bb3470681205b2510bd5e7ecfe5c3181107540fe7b97e8d406b2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9ac68b9f1e03bb3470681205b2510bd5e7ecfe5c3181107540fe7b97e8d406b2->enter($__internal_9ac68b9f1e03bb3470681205b2510bd5e7ecfe5c3181107540fe7b97e8d406b2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 13
        echo "    <span class=\"label ";
        echo (($this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "hasexception", array())) ? ("label-status-error") : ("disabled"));
        echo "\">
        <span class=\"icon\">";
        // line 14
        echo twig_include($this->env, $context, "@WebProfiler/Icon/exception.svg");
        echo "</span>
        <strong>Exception</strong>
        ";
        // line 16
        if ($this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "hasexception", array())) {
            // line 17
            echo "            <span class=\"count\">
                <span>1</span>
            </span>
        ";
        }
        // line 21
        echo "    </span>
";
        
        $__internal_9ac68b9f1e03bb3470681205b2510bd5e7ecfe5c3181107540fe7b97e8d406b2->leave($__internal_9ac68b9f1e03bb3470681205b2510bd5e7ecfe5c3181107540fe7b97e8d406b2_prof);

        
        $__internal_122cef29695e9237010e0cb2af44143828d49bf7ff87bd494a6842a703640b04->leave($__internal_122cef29695e9237010e0cb2af44143828d49bf7ff87bd494a6842a703640b04_prof);

    }

    // line 24
    public function block_panel($context, array $blocks = array())
    {
        $__internal_5365c0212d3357867a14d53fe3a9c37e156fa0878b95a49eb761bb9511e7c8ef = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5365c0212d3357867a14d53fe3a9c37e156fa0878b95a49eb761bb9511e7c8ef->enter($__internal_5365c0212d3357867a14d53fe3a9c37e156fa0878b95a49eb761bb9511e7c8ef_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_deec5bf0d6f9f291147f496ed071400c1407d6723a0f829ef0b112bc062a83a3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_deec5bf0d6f9f291147f496ed071400c1407d6723a0f829ef0b112bc062a83a3->enter($__internal_deec5bf0d6f9f291147f496ed071400c1407d6723a0f829ef0b112bc062a83a3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 25
        echo "    <h2>Exceptions</h2>

    ";
        // line 27
        if ( !$this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "hasexception", array())) {
            // line 28
            echo "        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    ";
        } else {
            // line 32
            echo "        <div class=\"sf-reset\">
            ";
            // line 33
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
            echo "
        </div>
    ";
        }
        
        $__internal_deec5bf0d6f9f291147f496ed071400c1407d6723a0f829ef0b112bc062a83a3->leave($__internal_deec5bf0d6f9f291147f496ed071400c1407d6723a0f829ef0b112bc062a83a3_prof);

        
        $__internal_5365c0212d3357867a14d53fe3a9c37e156fa0878b95a49eb761bb9511e7c8ef->leave($__internal_5365c0212d3357867a14d53fe3a9c37e156fa0878b95a49eb761bb9511e7c8ef_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Collector:exception.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  138 => 33,  135 => 32,  129 => 28,  127 => 27,  123 => 25,  114 => 24,  103 => 21,  97 => 17,  95 => 16,  90 => 14,  85 => 13,  76 => 12,  63 => 9,  57 => 6,  54 => 5,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block head %}
    {% if collector.hasexception %}
        <style>
            {{ render(path('_profiler_exception_css', { token: token })) }}
        </style>
    {% endif %}
    {{ parent() }}
{% endblock %}

{% block menu %}
    <span class=\"label {{ collector.hasexception ? 'label-status-error' : 'disabled' }}\">
        <span class=\"icon\">{{ include('@WebProfiler/Icon/exception.svg') }}</span>
        <strong>Exception</strong>
        {% if collector.hasexception %}
            <span class=\"count\">
                <span>1</span>
            </span>
        {% endif %}
    </span>
{% endblock %}

{% block panel %}
    <h2>Exceptions</h2>

    {% if not collector.hasexception %}
        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    {% else %}
        <div class=\"sf-reset\">
            {{ render(path('_profiler_exception', { token: token })) }}
        </div>
    {% endif %}
{% endblock %}
", "WebProfilerBundle:Collector:exception.html.twig", "E:\\xampp_new\\htdocs\\Symfony\\store\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle/Resources/views/Collector/exception.html.twig");
    }
}

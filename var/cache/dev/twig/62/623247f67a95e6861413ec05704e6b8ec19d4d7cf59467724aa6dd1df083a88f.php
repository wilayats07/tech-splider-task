<?php

/* @Twig/Exception/error.json.twig */
class __TwigTemplate_94e782c3b132e4e8193df6909b76fdba0be27a16f85afefcdbb50ab45fc8292b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_758fbd9e07197f13929c582708847ca4ec843e12ad3da01935b6949d6bdf2cbc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_758fbd9e07197f13929c582708847ca4ec843e12ad3da01935b6949d6bdf2cbc->enter($__internal_758fbd9e07197f13929c582708847ca4ec843e12ad3da01935b6949d6bdf2cbc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/error.json.twig"));

        $__internal_1db1f8e450ca4cc235e7e5e5dfd0bd6252d41037b35913496069a9f6fadd6f99 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1db1f8e450ca4cc235e7e5e5dfd0bd6252d41037b35913496069a9f6fadd6f99->enter($__internal_1db1f8e450ca4cc235e7e5e5dfd0bd6252d41037b35913496069a9f6fadd6f99_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/error.json.twig"));

        // line 1
        echo twig_jsonencode_filter(array("error" => array("code" => (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "message" => (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")))));
        echo "
";
        
        $__internal_758fbd9e07197f13929c582708847ca4ec843e12ad3da01935b6949d6bdf2cbc->leave($__internal_758fbd9e07197f13929c582708847ca4ec843e12ad3da01935b6949d6bdf2cbc_prof);

        
        $__internal_1db1f8e450ca4cc235e7e5e5dfd0bd6252d41037b35913496069a9f6fadd6f99->leave($__internal_1db1f8e450ca4cc235e7e5e5dfd0bd6252d41037b35913496069a9f6fadd6f99_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/error.json.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ { 'error': { 'code': status_code, 'message': status_text } }|json_encode|raw }}
", "@Twig/Exception/error.json.twig", "E:\\xampp_new\\htdocs\\Symfony\\store\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle\\Resources\\views\\Exception\\error.json.twig");
    }
}

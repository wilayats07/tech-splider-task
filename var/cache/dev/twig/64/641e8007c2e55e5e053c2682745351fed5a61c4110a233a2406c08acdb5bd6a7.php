<?php

/* WebProfilerBundle:Profiler:toolbar_redirect.html.twig */
class __TwigTemplate_067f4e2b1ed9b8fc8579ebafbadba23a6828395efefd8dffe93e2372d6164a9d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5fcb1e88db0b768a58ff8f7b3ebf2a8567d0d942d62a6f7b9cee05c8a427efe4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5fcb1e88db0b768a58ff8f7b3ebf2a8567d0d942d62a6f7b9cee05c8a427efe4->enter($__internal_5fcb1e88db0b768a58ff8f7b3ebf2a8567d0d942d62a6f7b9cee05c8a427efe4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig"));

        $__internal_80f4eec3e5ebeb7dbceb7c7cd9461c9acacd953427cb5154b66a64dc70f8b1eb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_80f4eec3e5ebeb7dbceb7c7cd9461c9acacd953427cb5154b66a64dc70f8b1eb->enter($__internal_80f4eec3e5ebeb7dbceb7c7cd9461c9acacd953427cb5154b66a64dc70f8b1eb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5fcb1e88db0b768a58ff8f7b3ebf2a8567d0d942d62a6f7b9cee05c8a427efe4->leave($__internal_5fcb1e88db0b768a58ff8f7b3ebf2a8567d0d942d62a6f7b9cee05c8a427efe4_prof);

        
        $__internal_80f4eec3e5ebeb7dbceb7c7cd9461c9acacd953427cb5154b66a64dc70f8b1eb->leave($__internal_80f4eec3e5ebeb7dbceb7c7cd9461c9acacd953427cb5154b66a64dc70f8b1eb_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_c6f1f9f08425daccd48c15f2da07298bb840e54a40240cd0ff0220e56c368977 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c6f1f9f08425daccd48c15f2da07298bb840e54a40240cd0ff0220e56c368977->enter($__internal_c6f1f9f08425daccd48c15f2da07298bb840e54a40240cd0ff0220e56c368977_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_bf4f4a1b346e8f70d54c67c92c34ca1abb8ef3a7bbc2567f1c0eec6d74a89010 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bf4f4a1b346e8f70d54c67c92c34ca1abb8ef3a7bbc2567f1c0eec6d74a89010->enter($__internal_bf4f4a1b346e8f70d54c67c92c34ca1abb8ef3a7bbc2567f1c0eec6d74a89010_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Redirection Intercepted";
        
        $__internal_bf4f4a1b346e8f70d54c67c92c34ca1abb8ef3a7bbc2567f1c0eec6d74a89010->leave($__internal_bf4f4a1b346e8f70d54c67c92c34ca1abb8ef3a7bbc2567f1c0eec6d74a89010_prof);

        
        $__internal_c6f1f9f08425daccd48c15f2da07298bb840e54a40240cd0ff0220e56c368977->leave($__internal_c6f1f9f08425daccd48c15f2da07298bb840e54a40240cd0ff0220e56c368977_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_71f73ef0e3c7be08450bc41332fafaa3477cc49389694034a4004071774b0374 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_71f73ef0e3c7be08450bc41332fafaa3477cc49389694034a4004071774b0374->enter($__internal_71f73ef0e3c7be08450bc41332fafaa3477cc49389694034a4004071774b0374_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_9eb0346bc1b683f0f9310f2504f04044cc5c30d9216f612f899d80d7e1db9d85 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9eb0346bc1b683f0f9310f2504f04044cc5c30d9216f612f899d80d7e1db9d85->enter($__internal_9eb0346bc1b683f0f9310f2504f04044cc5c30d9216f612f899d80d7e1db9d85_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <div class=\"sf-reset\">
        <div class=\"block-exception\">
            <h1>This request redirects to <a href=\"";
        // line 8
        echo twig_escape_filter($this->env, (isset($context["location"]) ? $context["location"] : $this->getContext($context, "location")), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, (isset($context["location"]) ? $context["location"] : $this->getContext($context, "location")), "html", null, true);
        echo "</a>.</h1>

            <p>
                <small>
                    The redirect was intercepted by the web debug toolbar to help debugging.
                    For more information, see the \"intercept-redirects\" option of the Profiler.
                </small>
            </p>
        </div>
    </div>
";
        
        $__internal_9eb0346bc1b683f0f9310f2504f04044cc5c30d9216f612f899d80d7e1db9d85->leave($__internal_9eb0346bc1b683f0f9310f2504f04044cc5c30d9216f612f899d80d7e1db9d85_prof);

        
        $__internal_71f73ef0e3c7be08450bc41332fafaa3477cc49389694034a4004071774b0374->leave($__internal_71f73ef0e3c7be08450bc41332fafaa3477cc49389694034a4004071774b0374_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:toolbar_redirect.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  72 => 8,  68 => 6,  59 => 5,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@Twig/layout.html.twig' %}

{% block title 'Redirection Intercepted' %}

{% block body %}
    <div class=\"sf-reset\">
        <div class=\"block-exception\">
            <h1>This request redirects to <a href=\"{{ location }}\">{{ location }}</a>.</h1>

            <p>
                <small>
                    The redirect was intercepted by the web debug toolbar to help debugging.
                    For more information, see the \"intercept-redirects\" option of the Profiler.
                </small>
            </p>
        </div>
    </div>
{% endblock %}
", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig", "E:\\xampp_new\\htdocs\\Symfony\\store\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle/Resources/views/Profiler/toolbar_redirect.html.twig");
    }
}

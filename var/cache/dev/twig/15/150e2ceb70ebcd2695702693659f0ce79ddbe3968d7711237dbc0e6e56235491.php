<?php

/* @Framework/Form/form_errors.html.php */
class __TwigTemplate_73d6f091166d6733c12434c6dc665f76e846f4759b1033ad0ad52b66027ade1d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e1ab608ac5a1d438ebac7063a4d450b00d4ce701190b4f3a6244885925fb605f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e1ab608ac5a1d438ebac7063a4d450b00d4ce701190b4f3a6244885925fb605f->enter($__internal_e1ab608ac5a1d438ebac7063a4d450b00d4ce701190b4f3a6244885925fb605f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_errors.html.php"));

        $__internal_e5ac127631d572a27e4c9e28cb148d7d66e78a092c14136ad0e5cd05c1016aab = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e5ac127631d572a27e4c9e28cb148d7d66e78a092c14136ad0e5cd05c1016aab->enter($__internal_e5ac127631d572a27e4c9e28cb148d7d66e78a092c14136ad0e5cd05c1016aab_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_errors.html.php"));

        // line 1
        echo "<?php if (count(\$errors) > 0): ?>
    <ul>
        <?php foreach (\$errors as \$error): ?>
            <li><?php echo \$error->getMessage() ?></li>
        <?php endforeach; ?>
    </ul>
<?php endif ?>
";
        
        $__internal_e1ab608ac5a1d438ebac7063a4d450b00d4ce701190b4f3a6244885925fb605f->leave($__internal_e1ab608ac5a1d438ebac7063a4d450b00d4ce701190b4f3a6244885925fb605f_prof);

        
        $__internal_e5ac127631d572a27e4c9e28cb148d7d66e78a092c14136ad0e5cd05c1016aab->leave($__internal_e5ac127631d572a27e4c9e28cb148d7d66e78a092c14136ad0e5cd05c1016aab_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_errors.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (count(\$errors) > 0): ?>
    <ul>
        <?php foreach (\$errors as \$error): ?>
            <li><?php echo \$error->getMessage() ?></li>
        <?php endforeach; ?>
    </ul>
<?php endif ?>
", "@Framework/Form/form_errors.html.php", "E:\\xampp_new\\htdocs\\Symfony\\store\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\form_errors.html.php");
    }
}

<?php

/* @Framework/FormTable/hidden_row.html.php */
class __TwigTemplate_88a9ecd71feaa4e9d92c9d0613c6618e8ec5a7816c15fb85150a30e0b9ce6318 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_25b9f4cebe38ce7e08878f9d09787add6bf4746a4c15cc54279790b92b84a628 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_25b9f4cebe38ce7e08878f9d09787add6bf4746a4c15cc54279790b92b84a628->enter($__internal_25b9f4cebe38ce7e08878f9d09787add6bf4746a4c15cc54279790b92b84a628_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/hidden_row.html.php"));

        $__internal_0db35348525a200346f1b527826c9c769bb1b8e163df34fad7d928beaf376203 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0db35348525a200346f1b527826c9c769bb1b8e163df34fad7d928beaf376203->enter($__internal_0db35348525a200346f1b527826c9c769bb1b8e163df34fad7d928beaf376203_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/hidden_row.html.php"));

        // line 1
        echo "<tr style=\"display: none\">
    <td colspan=\"2\">
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
        
        $__internal_25b9f4cebe38ce7e08878f9d09787add6bf4746a4c15cc54279790b92b84a628->leave($__internal_25b9f4cebe38ce7e08878f9d09787add6bf4746a4c15cc54279790b92b84a628_prof);

        
        $__internal_0db35348525a200346f1b527826c9c769bb1b8e163df34fad7d928beaf376203->leave($__internal_0db35348525a200346f1b527826c9c769bb1b8e163df34fad7d928beaf376203_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/hidden_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<tr style=\"display: none\">
    <td colspan=\"2\">
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
", "@Framework/FormTable/hidden_row.html.php", "E:\\xampp_new\\htdocs\\Symfony\\store\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\FormTable\\hidden_row.html.php");
    }
}

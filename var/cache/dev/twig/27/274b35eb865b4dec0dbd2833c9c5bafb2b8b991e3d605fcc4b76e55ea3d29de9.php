<?php

/* TwigBundle:Exception:exception.atom.twig */
class __TwigTemplate_4a0837c52d6733fc0060ed5757ccfb28ace0aef65c6a16b6b14ca321a51748cd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e401520ebb36852e9b2a44cd4c89c41240cec1b9f0fd4950960c591346fcba9f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e401520ebb36852e9b2a44cd4c89c41240cec1b9f0fd4950960c591346fcba9f->enter($__internal_e401520ebb36852e9b2a44cd4c89c41240cec1b9f0fd4950960c591346fcba9f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.atom.twig"));

        $__internal_9bc450c51892ffcafb7c6a722385ce4e40456a595f7833d95cc6ff39026e682d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9bc450c51892ffcafb7c6a722385ce4e40456a595f7833d95cc6ff39026e682d->enter($__internal_9bc450c51892ffcafb7c6a722385ce4e40456a595f7833d95cc6ff39026e682d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.atom.twig"));

        // line 1
        $this->loadTemplate("@Twig/Exception/exception.xml.twig", "TwigBundle:Exception:exception.atom.twig", 1)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        
        $__internal_e401520ebb36852e9b2a44cd4c89c41240cec1b9f0fd4950960c591346fcba9f->leave($__internal_e401520ebb36852e9b2a44cd4c89c41240cec1b9f0fd4950960c591346fcba9f_prof);

        
        $__internal_9bc450c51892ffcafb7c6a722385ce4e40456a595f7833d95cc6ff39026e682d->leave($__internal_9bc450c51892ffcafb7c6a722385ce4e40456a595f7833d95cc6ff39026e682d_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.atom.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% include '@Twig/Exception/exception.xml.twig' with { 'exception': exception } %}
", "TwigBundle:Exception:exception.atom.twig", "E:\\xampp_new\\htdocs\\Symfony\\store\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle/Resources/views/Exception/exception.atom.twig");
    }
}

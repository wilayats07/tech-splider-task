<?php

/* @Twig/Exception/error.xml.twig */
class __TwigTemplate_ecab9bd0f7df503e832ea6f0c24cc58fd52f06d2bd6d90490777e8bce83b9074 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8583562cd601546fd2774e09e76da54cd999aa94003706c47985e4ea3ab93cf6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8583562cd601546fd2774e09e76da54cd999aa94003706c47985e4ea3ab93cf6->enter($__internal_8583562cd601546fd2774e09e76da54cd999aa94003706c47985e4ea3ab93cf6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/error.xml.twig"));

        $__internal_e930604d00c7d359e012a75df61555993ad4aded86162521f18a8205ee499b14 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e930604d00c7d359e012a75df61555993ad4aded86162521f18a8205ee499b14->enter($__internal_e930604d00c7d359e012a75df61555993ad4aded86162521f18a8205ee499b14_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/error.xml.twig"));

        // line 1
        echo "<?xml version=\"1.0\" encoding=\"";
        echo twig_escape_filter($this->env, $this->env->getCharset(), "html", null, true);
        echo "\" ?>

<error code=\"";
        // line 3
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo "\" message=\"";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo "\" />
";
        
        $__internal_8583562cd601546fd2774e09e76da54cd999aa94003706c47985e4ea3ab93cf6->leave($__internal_8583562cd601546fd2774e09e76da54cd999aa94003706c47985e4ea3ab93cf6_prof);

        
        $__internal_e930604d00c7d359e012a75df61555993ad4aded86162521f18a8205ee499b14->leave($__internal_e930604d00c7d359e012a75df61555993ad4aded86162521f18a8205ee499b14_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/error.xml.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  31 => 3,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?xml version=\"1.0\" encoding=\"{{ _charset }}\" ?>

<error code=\"{{ status_code }}\" message=\"{{ status_text }}\" />
", "@Twig/Exception/error.xml.twig", "E:\\xampp_new\\htdocs\\Symfony\\store\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle\\Resources\\views\\Exception\\error.xml.twig");
    }
}

<?php

/* WebProfilerBundle:Profiler:header.html.twig */
class __TwigTemplate_7fa301137c70abe5968c877eb4d3b2dc2980a891691ac47cae0c0cb63083ffee extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_82812f86c6b3fa2e1c2eacf8b0904d81697d017fddeba92694dc46dec0a32e63 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_82812f86c6b3fa2e1c2eacf8b0904d81697d017fddeba92694dc46dec0a32e63->enter($__internal_82812f86c6b3fa2e1c2eacf8b0904d81697d017fddeba92694dc46dec0a32e63_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:header.html.twig"));

        $__internal_b0a562b4d3bf5a81a3dd6f32de06de118a212f03c89275cd47bd3fa3d40c158d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b0a562b4d3bf5a81a3dd6f32de06de118a212f03c89275cd47bd3fa3d40c158d->enter($__internal_b0a562b4d3bf5a81a3dd6f32de06de118a212f03c89275cd47bd3fa3d40c158d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:header.html.twig"));

        // line 1
        echo "<div id=\"header\">
    <div class=\"container\">
        <h1>";
        // line 3
        echo twig_include($this->env, $context, "@WebProfiler/Icon/symfony.svg");
        echo " Symfony <span>Profiler</span></h1>

        <div class=\"search\">
            <form method=\"get\" action=\"https://symfony.com/search\" target=\"_blank\">
                <div class=\"form-row\">
                    <input name=\"q\" id=\"search-id\" type=\"search\" placeholder=\"search on symfony.com\">
                    <button type=\"submit\" class=\"btn\">Search</button>
                </div>
           </form>
        </div>
    </div>
</div>
";
        
        $__internal_82812f86c6b3fa2e1c2eacf8b0904d81697d017fddeba92694dc46dec0a32e63->leave($__internal_82812f86c6b3fa2e1c2eacf8b0904d81697d017fddeba92694dc46dec0a32e63_prof);

        
        $__internal_b0a562b4d3bf5a81a3dd6f32de06de118a212f03c89275cd47bd3fa3d40c158d->leave($__internal_b0a562b4d3bf5a81a3dd6f32de06de118a212f03c89275cd47bd3fa3d40c158d_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:header.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  29 => 3,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div id=\"header\">
    <div class=\"container\">
        <h1>{{ include('@WebProfiler/Icon/symfony.svg') }} Symfony <span>Profiler</span></h1>

        <div class=\"search\">
            <form method=\"get\" action=\"https://symfony.com/search\" target=\"_blank\">
                <div class=\"form-row\">
                    <input name=\"q\" id=\"search-id\" type=\"search\" placeholder=\"search on symfony.com\">
                    <button type=\"submit\" class=\"btn\">Search</button>
                </div>
           </form>
        </div>
    </div>
</div>
", "WebProfilerBundle:Profiler:header.html.twig", "E:\\xampp_new\\htdocs\\Symfony\\store\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle/Resources/views/Profiler/header.html.twig");
    }
}

<?php

/* TwigBundle:Exception:error.xml.twig */
class __TwigTemplate_9d5ce6b71acb3f19cc479ca358c3b1229d9d0145fe73e5b87b1aecb87011a7f0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b7bc2b4fde81d7264545f174b9f2f1545d27ae1ead08464ba52b18ca8a2de915 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b7bc2b4fde81d7264545f174b9f2f1545d27ae1ead08464ba52b18ca8a2de915->enter($__internal_b7bc2b4fde81d7264545f174b9f2f1545d27ae1ead08464ba52b18ca8a2de915_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.xml.twig"));

        $__internal_a2bef1fb41dc18c9223ad76bf03cc82e2bfd0632edc7f638a7b2815c68627f12 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a2bef1fb41dc18c9223ad76bf03cc82e2bfd0632edc7f638a7b2815c68627f12->enter($__internal_a2bef1fb41dc18c9223ad76bf03cc82e2bfd0632edc7f638a7b2815c68627f12_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.xml.twig"));

        // line 1
        echo "<?xml version=\"1.0\" encoding=\"";
        echo twig_escape_filter($this->env, $this->env->getCharset(), "html", null, true);
        echo "\" ?>

<error code=\"";
        // line 3
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo "\" message=\"";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo "\" />
";
        
        $__internal_b7bc2b4fde81d7264545f174b9f2f1545d27ae1ead08464ba52b18ca8a2de915->leave($__internal_b7bc2b4fde81d7264545f174b9f2f1545d27ae1ead08464ba52b18ca8a2de915_prof);

        
        $__internal_a2bef1fb41dc18c9223ad76bf03cc82e2bfd0632edc7f638a7b2815c68627f12->leave($__internal_a2bef1fb41dc18c9223ad76bf03cc82e2bfd0632edc7f638a7b2815c68627f12_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.xml.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  31 => 3,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?xml version=\"1.0\" encoding=\"{{ _charset }}\" ?>

<error code=\"{{ status_code }}\" message=\"{{ status_text }}\" />
", "TwigBundle:Exception:error.xml.twig", "E:\\xampp_new\\htdocs\\Symfony\\store\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle/Resources/views/Exception/error.xml.twig");
    }
}

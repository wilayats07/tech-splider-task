<?php

/* @Framework/Form/textarea_widget.html.php */
class __TwigTemplate_2a212e5ed31a1b3cc9815006cc6157bfbfa8741c4f386eb219b775c8de20091d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d1d7e5aa96e4900a98bda4896a74bfd9bbd1fd0779b5f17918fe5ccfc0bbcf20 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d1d7e5aa96e4900a98bda4896a74bfd9bbd1fd0779b5f17918fe5ccfc0bbcf20->enter($__internal_d1d7e5aa96e4900a98bda4896a74bfd9bbd1fd0779b5f17918fe5ccfc0bbcf20_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/textarea_widget.html.php"));

        $__internal_39157e07d3a943fd61a6c613afa71b024575d5a36fd894ac2e90ca534efe1460 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_39157e07d3a943fd61a6c613afa71b024575d5a36fd894ac2e90ca534efe1460->enter($__internal_39157e07d3a943fd61a6c613afa71b024575d5a36fd894ac2e90ca534efe1460_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/textarea_widget.html.php"));

        // line 1
        echo "<textarea <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>><?php echo \$view->escape(\$value) ?></textarea>
";
        
        $__internal_d1d7e5aa96e4900a98bda4896a74bfd9bbd1fd0779b5f17918fe5ccfc0bbcf20->leave($__internal_d1d7e5aa96e4900a98bda4896a74bfd9bbd1fd0779b5f17918fe5ccfc0bbcf20_prof);

        
        $__internal_39157e07d3a943fd61a6c613afa71b024575d5a36fd894ac2e90ca534efe1460->leave($__internal_39157e07d3a943fd61a6c613afa71b024575d5a36fd894ac2e90ca534efe1460_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/textarea_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<textarea <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>><?php echo \$view->escape(\$value) ?></textarea>
", "@Framework/Form/textarea_widget.html.php", "E:\\xampp_new\\htdocs\\Symfony\\store\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\textarea_widget.html.php");
    }
}

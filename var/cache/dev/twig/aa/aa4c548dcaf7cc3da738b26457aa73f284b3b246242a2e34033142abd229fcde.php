<?php

/* WebProfilerBundle:Profiler:ajax_layout.html.twig */
class __TwigTemplate_26cf9bf30cb6908e94b72420d69b92dc184a271e6a2627c73312e1113d40db5f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6676a9d438501a224d7dacf4012780375d8c7ee85f6f4fca8c9b3f425ba64abf = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6676a9d438501a224d7dacf4012780375d8c7ee85f6f4fca8c9b3f425ba64abf->enter($__internal_6676a9d438501a224d7dacf4012780375d8c7ee85f6f4fca8c9b3f425ba64abf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:ajax_layout.html.twig"));

        $__internal_b4ac732e0d47a96ec63039c42ce62fd959b99f9767657cdcb2856cb19d0b3e49 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b4ac732e0d47a96ec63039c42ce62fd959b99f9767657cdcb2856cb19d0b3e49->enter($__internal_b4ac732e0d47a96ec63039c42ce62fd959b99f9767657cdcb2856cb19d0b3e49_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:ajax_layout.html.twig"));

        // line 1
        $this->displayBlock('panel', $context, $blocks);
        
        $__internal_6676a9d438501a224d7dacf4012780375d8c7ee85f6f4fca8c9b3f425ba64abf->leave($__internal_6676a9d438501a224d7dacf4012780375d8c7ee85f6f4fca8c9b3f425ba64abf_prof);

        
        $__internal_b4ac732e0d47a96ec63039c42ce62fd959b99f9767657cdcb2856cb19d0b3e49->leave($__internal_b4ac732e0d47a96ec63039c42ce62fd959b99f9767657cdcb2856cb19d0b3e49_prof);

    }

    public function block_panel($context, array $blocks = array())
    {
        $__internal_3a8f5792cc2a6cadcf8a0bb61701d418a7b7d4c986c7f4cbad52ad3ba66d5732 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3a8f5792cc2a6cadcf8a0bb61701d418a7b7d4c986c7f4cbad52ad3ba66d5732->enter($__internal_3a8f5792cc2a6cadcf8a0bb61701d418a7b7d4c986c7f4cbad52ad3ba66d5732_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_408a2e30263081d66dd07e220814a2bd17f649225c48e9304b4c2ac1b825fbc9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_408a2e30263081d66dd07e220814a2bd17f649225c48e9304b4c2ac1b825fbc9->enter($__internal_408a2e30263081d66dd07e220814a2bd17f649225c48e9304b4c2ac1b825fbc9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        echo "";
        
        $__internal_408a2e30263081d66dd07e220814a2bd17f649225c48e9304b4c2ac1b825fbc9->leave($__internal_408a2e30263081d66dd07e220814a2bd17f649225c48e9304b4c2ac1b825fbc9_prof);

        
        $__internal_3a8f5792cc2a6cadcf8a0bb61701d418a7b7d4c986c7f4cbad52ad3ba66d5732->leave($__internal_3a8f5792cc2a6cadcf8a0bb61701d418a7b7d4c986c7f4cbad52ad3ba66d5732_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:ajax_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  26 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% block panel '' %}
", "WebProfilerBundle:Profiler:ajax_layout.html.twig", "E:\\xampp_new\\htdocs\\Symfony\\store\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle/Resources/views/Profiler/ajax_layout.html.twig");
    }
}

<?php

/* @Framework/Form/url_widget.html.php */
class __TwigTemplate_84664b445ccd4bc884d673233ec7c6cff71a7547a841ef729247df96b12213ed extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_28bc98a78024b3798264a31306cb34f53c593d407442668a2f4258a2899106e2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_28bc98a78024b3798264a31306cb34f53c593d407442668a2f4258a2899106e2->enter($__internal_28bc98a78024b3798264a31306cb34f53c593d407442668a2f4258a2899106e2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/url_widget.html.php"));

        $__internal_3088b89fe8621b511abba198c2ac6a438f1490c564fad864710c77f282376fe4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3088b89fe8621b511abba198c2ac6a438f1490c564fad864710c77f282376fe4->enter($__internal_3088b89fe8621b511abba198c2ac6a438f1490c564fad864710c77f282376fe4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/url_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'url')) ?>
";
        
        $__internal_28bc98a78024b3798264a31306cb34f53c593d407442668a2f4258a2899106e2->leave($__internal_28bc98a78024b3798264a31306cb34f53c593d407442668a2f4258a2899106e2_prof);

        
        $__internal_3088b89fe8621b511abba198c2ac6a438f1490c564fad864710c77f282376fe4->leave($__internal_3088b89fe8621b511abba198c2ac6a438f1490c564fad864710c77f282376fe4_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/url_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'url')) ?>
", "@Framework/Form/url_widget.html.php", "E:\\xampp_new\\htdocs\\Symfony\\store\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\url_widget.html.php");
    }
}

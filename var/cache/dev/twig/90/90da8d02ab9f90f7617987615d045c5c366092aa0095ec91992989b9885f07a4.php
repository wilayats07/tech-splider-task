<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_0fd258d5cd90e6569d1ab61f7a8b5064101977573920a7e0c68993aed9a371c4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e166d3f0f0489335e2888329706837053ff82599f1923a148007b983366a3f5d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e166d3f0f0489335e2888329706837053ff82599f1923a148007b983366a3f5d->enter($__internal_e166d3f0f0489335e2888329706837053ff82599f1923a148007b983366a3f5d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $__internal_647689726675d974e23f2556dd7fdd37f5bea8be3ad1740afcb86652c73b1b02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_647689726675d974e23f2556dd7fdd37f5bea8be3ad1740afcb86652c73b1b02->enter($__internal_647689726675d974e23f2556dd7fdd37f5bea8be3ad1740afcb86652c73b1b02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_e166d3f0f0489335e2888329706837053ff82599f1923a148007b983366a3f5d->leave($__internal_e166d3f0f0489335e2888329706837053ff82599f1923a148007b983366a3f5d_prof);

        
        $__internal_647689726675d974e23f2556dd7fdd37f5bea8be3ad1740afcb86652c73b1b02->leave($__internal_647689726675d974e23f2556dd7fdd37f5bea8be3ad1740afcb86652c73b1b02_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_584041693909df117d26a8f804212a5d30668fa32e20fe3d16bf34795028ed8e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_584041693909df117d26a8f804212a5d30668fa32e20fe3d16bf34795028ed8e->enter($__internal_584041693909df117d26a8f804212a5d30668fa32e20fe3d16bf34795028ed8e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_c6b8d9165a5690c778138fe16374607ae55595323c81235406976a970ed93055 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c6b8d9165a5690c778138fe16374607ae55595323c81235406976a970ed93055->enter($__internal_c6b8d9165a5690c778138fe16374607ae55595323c81235406976a970ed93055_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_c6b8d9165a5690c778138fe16374607ae55595323c81235406976a970ed93055->leave($__internal_c6b8d9165a5690c778138fe16374607ae55595323c81235406976a970ed93055_prof);

        
        $__internal_584041693909df117d26a8f804212a5d30668fa32e20fe3d16bf34795028ed8e->leave($__internal_584041693909df117d26a8f804212a5d30668fa32e20fe3d16bf34795028ed8e_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_4aa710e9c53494d40f66ccbb57b913290bc73f4a00454680449fc5655dcde6ab = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4aa710e9c53494d40f66ccbb57b913290bc73f4a00454680449fc5655dcde6ab->enter($__internal_4aa710e9c53494d40f66ccbb57b913290bc73f4a00454680449fc5655dcde6ab_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_97cbb799ec54f52c2570a8e2dbe92fc355a44358c096a0f65260e3660cf250df = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_97cbb799ec54f52c2570a8e2dbe92fc355a44358c096a0f65260e3660cf250df->enter($__internal_97cbb799ec54f52c2570a8e2dbe92fc355a44358c096a0f65260e3660cf250df_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_97cbb799ec54f52c2570a8e2dbe92fc355a44358c096a0f65260e3660cf250df->leave($__internal_97cbb799ec54f52c2570a8e2dbe92fc355a44358c096a0f65260e3660cf250df_prof);

        
        $__internal_4aa710e9c53494d40f66ccbb57b913290bc73f4a00454680449fc5655dcde6ab->leave($__internal_4aa710e9c53494d40f66ccbb57b913290bc73f4a00454680449fc5655dcde6ab_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_87d0c14f7aa50bbb1d1c6dbea094ece2b2656b25502df0698e80301639e8c19b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_87d0c14f7aa50bbb1d1c6dbea094ece2b2656b25502df0698e80301639e8c19b->enter($__internal_87d0c14f7aa50bbb1d1c6dbea094ece2b2656b25502df0698e80301639e8c19b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_08610ab7fac0223c34c1d25c36dc0229f7e454e08020feaa6cb5143bc034dd82 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_08610ab7fac0223c34c1d25c36dc0229f7e454e08020feaa6cb5143bc034dd82->enter($__internal_08610ab7fac0223c34c1d25c36dc0229f7e454e08020feaa6cb5143bc034dd82_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_router", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_08610ab7fac0223c34c1d25c36dc0229f7e454e08020feaa6cb5143bc034dd82->leave($__internal_08610ab7fac0223c34c1d25c36dc0229f7e454e08020feaa6cb5143bc034dd82_prof);

        
        $__internal_87d0c14f7aa50bbb1d1c6dbea094ece2b2656b25502df0698e80301639e8c19b->leave($__internal_87d0c14f7aa50bbb1d1c6dbea094ece2b2656b25502df0698e80301639e8c19b_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 13,  85 => 12,  71 => 7,  68 => 6,  59 => 5,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}{% endblock %}

{% block menu %}
<span class=\"label\">
    <span class=\"icon\">{{ include('@WebProfiler/Icon/router.svg') }}</span>
    <strong>Routing</strong>
</span>
{% endblock %}

{% block panel %}
    {{ render(path('_profiler_router', { token: token })) }}
{% endblock %}
", "@WebProfiler/Collector/router.html.twig", "E:\\xampp_new\\htdocs\\Symfony\\store\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle\\Resources\\views\\Collector\\router.html.twig");
    }
}

<?php

/* TwigBundle:Exception:error.json.twig */
class __TwigTemplate_9ee94395d57bd775afdb5d78dd46fdaa650e54a29ca756c0211b2c734117b8a0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_96a38f267c9b653268d9579dafc5642c56b2da7cbfada8039fe21f5d5fcb3b5e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_96a38f267c9b653268d9579dafc5642c56b2da7cbfada8039fe21f5d5fcb3b5e->enter($__internal_96a38f267c9b653268d9579dafc5642c56b2da7cbfada8039fe21f5d5fcb3b5e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.json.twig"));

        $__internal_2785ff8d9b4272967f719e836e2554c368135b6ecca420593c2107a31dac46bc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2785ff8d9b4272967f719e836e2554c368135b6ecca420593c2107a31dac46bc->enter($__internal_2785ff8d9b4272967f719e836e2554c368135b6ecca420593c2107a31dac46bc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.json.twig"));

        // line 1
        echo twig_jsonencode_filter(array("error" => array("code" => (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "message" => (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")))));
        echo "
";
        
        $__internal_96a38f267c9b653268d9579dafc5642c56b2da7cbfada8039fe21f5d5fcb3b5e->leave($__internal_96a38f267c9b653268d9579dafc5642c56b2da7cbfada8039fe21f5d5fcb3b5e_prof);

        
        $__internal_2785ff8d9b4272967f719e836e2554c368135b6ecca420593c2107a31dac46bc->leave($__internal_2785ff8d9b4272967f719e836e2554c368135b6ecca420593c2107a31dac46bc_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.json.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ { 'error': { 'code': status_code, 'message': status_text } }|json_encode|raw }}
", "TwigBundle:Exception:error.json.twig", "E:\\xampp_new\\htdocs\\Symfony\\store\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle/Resources/views/Exception/error.json.twig");
    }
}

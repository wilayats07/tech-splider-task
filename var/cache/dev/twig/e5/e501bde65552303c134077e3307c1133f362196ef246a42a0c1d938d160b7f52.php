<?php

/* TwigBundle:Exception:exception.rdf.twig */
class __TwigTemplate_f0dbcbe243af1ac361b1565700d55943209a3eb5f9cedb9d61e3b26f27280039 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b8fe984668b310966269602b2f583ddd043315066c3d0b61bd13513516761741 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b8fe984668b310966269602b2f583ddd043315066c3d0b61bd13513516761741->enter($__internal_b8fe984668b310966269602b2f583ddd043315066c3d0b61bd13513516761741_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.rdf.twig"));

        $__internal_be640b571fffc815ea0a16011439f493e2b24af16abd66b16ebbaecbce55dae0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_be640b571fffc815ea0a16011439f493e2b24af16abd66b16ebbaecbce55dae0->enter($__internal_be640b571fffc815ea0a16011439f493e2b24af16abd66b16ebbaecbce55dae0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.rdf.twig"));

        // line 1
        $this->loadTemplate("@Twig/Exception/exception.xml.twig", "TwigBundle:Exception:exception.rdf.twig", 1)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        
        $__internal_b8fe984668b310966269602b2f583ddd043315066c3d0b61bd13513516761741->leave($__internal_b8fe984668b310966269602b2f583ddd043315066c3d0b61bd13513516761741_prof);

        
        $__internal_be640b571fffc815ea0a16011439f493e2b24af16abd66b16ebbaecbce55dae0->leave($__internal_be640b571fffc815ea0a16011439f493e2b24af16abd66b16ebbaecbce55dae0_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.rdf.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% include '@Twig/Exception/exception.xml.twig' with { 'exception': exception } %}
", "TwigBundle:Exception:exception.rdf.twig", "E:\\xampp_new\\htdocs\\Symfony\\store\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle/Resources/views/Exception/exception.rdf.twig");
    }
}

<?php

/* @Framework/Form/percent_widget.html.php */
class __TwigTemplate_bede0af796a1486980ba5e86053a1ccc012ec265647dee3874742536b88ef0a0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d239f201f9d666f8c79a8386643f96038a84dd4da15e0f591f3e566ef0c1335d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d239f201f9d666f8c79a8386643f96038a84dd4da15e0f591f3e566ef0c1335d->enter($__internal_d239f201f9d666f8c79a8386643f96038a84dd4da15e0f591f3e566ef0c1335d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/percent_widget.html.php"));

        $__internal_e1b7da21c1a1d233dbdb9d447a51205518bce508baf7d7b672f90a5989499529 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e1b7da21c1a1d233dbdb9d447a51205518bce508baf7d7b672f90a5989499529->enter($__internal_e1b7da21c1a1d233dbdb9d447a51205518bce508baf7d7b672f90a5989499529_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/percent_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'text')) ?> %
";
        
        $__internal_d239f201f9d666f8c79a8386643f96038a84dd4da15e0f591f3e566ef0c1335d->leave($__internal_d239f201f9d666f8c79a8386643f96038a84dd4da15e0f591f3e566ef0c1335d_prof);

        
        $__internal_e1b7da21c1a1d233dbdb9d447a51205518bce508baf7d7b672f90a5989499529->leave($__internal_e1b7da21c1a1d233dbdb9d447a51205518bce508baf7d7b672f90a5989499529_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/percent_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'text')) ?> %
", "@Framework/Form/percent_widget.html.php", "E:\\xampp_new\\htdocs\\Symfony\\store\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\percent_widget.html.php");
    }
}

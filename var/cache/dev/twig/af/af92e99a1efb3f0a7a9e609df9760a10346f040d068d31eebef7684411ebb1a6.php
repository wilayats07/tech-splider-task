<?php

/* TwigBundle:Exception:exception_full.html.twig */
class __TwigTemplate_91cfe39bf96e55acb08e6a7bc08963c05241befb51acccf23313894d9fb6bc42 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "TwigBundle:Exception:exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1982c4bb65e457a279cb9381da58e799fc84c4c8348bf34782148306e2a879fa = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1982c4bb65e457a279cb9381da58e799fc84c4c8348bf34782148306e2a879fa->enter($__internal_1982c4bb65e457a279cb9381da58e799fc84c4c8348bf34782148306e2a879fa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception_full.html.twig"));

        $__internal_5dedaada4b707cf63546ba75632009df1b228268d4478b2ac2ca9cbae714b6b4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5dedaada4b707cf63546ba75632009df1b228268d4478b2ac2ca9cbae714b6b4->enter($__internal_5dedaada4b707cf63546ba75632009df1b228268d4478b2ac2ca9cbae714b6b4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_1982c4bb65e457a279cb9381da58e799fc84c4c8348bf34782148306e2a879fa->leave($__internal_1982c4bb65e457a279cb9381da58e799fc84c4c8348bf34782148306e2a879fa_prof);

        
        $__internal_5dedaada4b707cf63546ba75632009df1b228268d4478b2ac2ca9cbae714b6b4->leave($__internal_5dedaada4b707cf63546ba75632009df1b228268d4478b2ac2ca9cbae714b6b4_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_7c9a6d474dbda901e923cdfb8702288ad67b04d93a0d39c3c5fc06fef0ed6226 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7c9a6d474dbda901e923cdfb8702288ad67b04d93a0d39c3c5fc06fef0ed6226->enter($__internal_7c9a6d474dbda901e923cdfb8702288ad67b04d93a0d39c3c5fc06fef0ed6226_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_3e3b90fb94b4736f25a38a35e5205f3d2e5b8930d8c00041715896c309d97359 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3e3b90fb94b4736f25a38a35e5205f3d2e5b8930d8c00041715896c309d97359->enter($__internal_3e3b90fb94b4736f25a38a35e5205f3d2e5b8930d8c00041715896c309d97359_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
";
        
        $__internal_3e3b90fb94b4736f25a38a35e5205f3d2e5b8930d8c00041715896c309d97359->leave($__internal_3e3b90fb94b4736f25a38a35e5205f3d2e5b8930d8c00041715896c309d97359_prof);

        
        $__internal_7c9a6d474dbda901e923cdfb8702288ad67b04d93a0d39c3c5fc06fef0ed6226->leave($__internal_7c9a6d474dbda901e923cdfb8702288ad67b04d93a0d39c3c5fc06fef0ed6226_prof);

    }

    // line 136
    public function block_title($context, array $blocks = array())
    {
        $__internal_71400f761641f483e744d575b7068bdb47a425b8e5f2e495da1fa280ab679aeb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_71400f761641f483e744d575b7068bdb47a425b8e5f2e495da1fa280ab679aeb->enter($__internal_71400f761641f483e744d575b7068bdb47a425b8e5f2e495da1fa280ab679aeb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_0507e4fabe3f9e239b67a82c1d28e8b879f711fcb86e20dd693849cc036f8d82 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0507e4fabe3f9e239b67a82c1d28e8b879f711fcb86e20dd693849cc036f8d82->enter($__internal_0507e4fabe3f9e239b67a82c1d28e8b879f711fcb86e20dd693849cc036f8d82_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 137
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_0507e4fabe3f9e239b67a82c1d28e8b879f711fcb86e20dd693849cc036f8d82->leave($__internal_0507e4fabe3f9e239b67a82c1d28e8b879f711fcb86e20dd693849cc036f8d82_prof);

        
        $__internal_71400f761641f483e744d575b7068bdb47a425b8e5f2e495da1fa280ab679aeb->leave($__internal_71400f761641f483e744d575b7068bdb47a425b8e5f2e495da1fa280ab679aeb_prof);

    }

    // line 140
    public function block_body($context, array $blocks = array())
    {
        $__internal_8d91e74faec2f42b2675234a5563a60f2a5f6e611ed8da7a21d9d89267862b9f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8d91e74faec2f42b2675234a5563a60f2a5f6e611ed8da7a21d9d89267862b9f->enter($__internal_8d91e74faec2f42b2675234a5563a60f2a5f6e611ed8da7a21d9d89267862b9f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_2483864162edcf10a96bf2b12800396d9be94774d4c1f169972b23c63f259b7b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2483864162edcf10a96bf2b12800396d9be94774d4c1f169972b23c63f259b7b->enter($__internal_2483864162edcf10a96bf2b12800396d9be94774d4c1f169972b23c63f259b7b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 141
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "TwigBundle:Exception:exception_full.html.twig", 141)->display($context);
        
        $__internal_2483864162edcf10a96bf2b12800396d9be94774d4c1f169972b23c63f259b7b->leave($__internal_2483864162edcf10a96bf2b12800396d9be94774d4c1f169972b23c63f259b7b_prof);

        
        $__internal_8d91e74faec2f42b2675234a5563a60f2a5f6e611ed8da7a21d9d89267862b9f->leave($__internal_8d91e74faec2f42b2675234a5563a60f2a5f6e611ed8da7a21d9d89267862b9f_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  226 => 141,  217 => 140,  200 => 137,  191 => 136,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@Twig/layout.html.twig' %}

{% block head %}
    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
{% endblock %}

{% block title %}
    {{ exception.message }} ({{ status_code }} {{ status_text }})
{% endblock %}

{% block body %}
    {% include '@Twig/Exception/exception.html.twig' %}
{% endblock %}
", "TwigBundle:Exception:exception_full.html.twig", "E:\\xampp_new\\htdocs\\Symfony\\store\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle/Resources/views/Exception/exception_full.html.twig");
    }
}

<?php

/* @Framework/Form/container_attributes.html.php */
class __TwigTemplate_130701d4b750a3dc8e72cf64fdc23086b6aa456bb05f282846a1074e5ac9e72a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6748fd57560dae23cac66964b7b8d29e0a8bc15ff4d67c9e2758fe5de127640d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6748fd57560dae23cac66964b7b8d29e0a8bc15ff4d67c9e2758fe5de127640d->enter($__internal_6748fd57560dae23cac66964b7b8d29e0a8bc15ff4d67c9e2758fe5de127640d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/container_attributes.html.php"));

        $__internal_b2f5690c7cdb442c7d7c9a936579efa38bb0b61dd6784e24d4bfdc6ff6baecd7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b2f5690c7cdb442c7d7c9a936579efa38bb0b61dd6784e24d4bfdc6ff6baecd7->enter($__internal_b2f5690c7cdb442c7d7c9a936579efa38bb0b61dd6784e24d4bfdc6ff6baecd7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/container_attributes.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>
";
        
        $__internal_6748fd57560dae23cac66964b7b8d29e0a8bc15ff4d67c9e2758fe5de127640d->leave($__internal_6748fd57560dae23cac66964b7b8d29e0a8bc15ff4d67c9e2758fe5de127640d_prof);

        
        $__internal_b2f5690c7cdb442c7d7c9a936579efa38bb0b61dd6784e24d4bfdc6ff6baecd7->leave($__internal_b2f5690c7cdb442c7d7c9a936579efa38bb0b61dd6784e24d4bfdc6ff6baecd7_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/container_attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>
", "@Framework/Form/container_attributes.html.php", "E:\\xampp_new\\htdocs\\Symfony\\store\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\container_attributes.html.php");
    }
}
